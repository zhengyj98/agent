import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.util.EntityUtils;

import cn.cellcom.agent.online.message.BizMessage;
import cn.cellcom.agent.online.message.MessageConstant;
import cn.cellcom.agent.pojo.TSession;
import cn.cellcom.agent.token.TokenTester;

public class TesterClient {

	private static String pid = "jianyuekefu57714";
	private static String channel = "10001";
	public static final String LOGIN = "http://www.jianyuekefu.com:8080/visitor/VisitorDpAct.do?method=logon&pid=" + pid + "&channel=" + channel
			+ "&api=java";
	public static String PROCESSOR = "http://www.jianyuekefu.com:8080/visitor/VisitorDpAct.do?method=processor";
	public static String GET_GROUP = "http://www.jianyuekefu.com:8080/visitor/VisitorDpAct.do?method=processor&event=GET_GROUP&pid=" + pid;

	public static void main(String[] args) {
		try {
			Map<String, String> headers = new HashMap<String, String>();
			HttpResponse result = TokenTester.sendRequest(LOGIN, headers);
			int statusCode = result.getStatusLine().getStatusCode();

			if (statusCode == HttpStatus.SC_OK) {
				String string = EntityUtils.toString(result.getEntity(), "UTF-8");
				Map map = (Map) JSONObject.toBean(JSONObject.fromObject(string), Map.class);
				String cid = (String) map.get("id");
				PROCESSOR = PROCESSOR + "&cid=" + cid;
				while (true) {
					System.out.println("--------------------");
					result = TokenTester.sendRequest(PROCESSOR, headers);
					String string2 = EntityUtils.toString(result.getEntity(), "UTF-8");
					List map2 = (List) JSONArray.fromObject(string2);
					for (int i = 0; i < map2.size(); i++) {
						Object obj = map2.get(i);
						Map<String, Class> classMap = new HashMap<String, Class>();
						classMap.put("object", TSession.class);
						BizMessage biz = (BizMessage) JSONObject.toBean((JSONObject) obj, BizMessage.class, classMap);
						TSession session = (TSession) biz.getObject();
						if (session.getStep().equals("LOGIN")) {
							HttpResponse result3 = TokenTester.sendRequest(GET_GROUP + "&cid=" + cid, headers);
							String string3 = EntityUtils.toString(result3.getEntity(), "UTF-8");
							System.out.println(string3);
							
						}
					}
					Thread.sleep(3000);
				}
			} else {
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
