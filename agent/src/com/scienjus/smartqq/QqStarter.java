package com.scienjus.smartqq;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.scienjus.smartqq.callback.MessageCallback;
import com.scienjus.smartqq.client.SmartQQClient;
import com.scienjus.smartqq.model.Category;
import com.scienjus.smartqq.model.Discuss;
import com.scienjus.smartqq.model.DiscussMessage;
import com.scienjus.smartqq.model.Friend;
import com.scienjus.smartqq.model.Group;
import com.scienjus.smartqq.model.GroupMessage;
import com.scienjus.smartqq.model.Message;
import com.scienjus.smartqq.model.UserInfo;

import cn.cellcom.jar.file.FileProcessor;
import cn.cellcom.jar.file.IFileProcessor;
import cn.cellcom.jar.file.IFileProcessor.MODE;
import cn.cellcom.jar.util.MyException;

/**
 * @author ScienJus
 * @date 2015/12/18.
 */
public class QqStarter {

	public static void main(String[] args) {
		// 创建一个新对象时需要扫描二维码登录，并且传一个处理接收到消息的回调，如果你不需要接收消息，可以传null
		SmartQQClient client = new SmartQQClient(new MessageCallback() {
			@Override
			public void onMessage(Message message) {
				System.out.println(message.getUserId() + "--->" + message.getContent());
			}

			@Override
			public void onGroupMessage(GroupMessage message) {
				System.out.println(message.getUserId() + "--->" + message.getContent());
			}

			@Override
			public void onDiscussMessage(DiscussMessage message) {
				System.out.println(message.getUserId() + "--->" + message.getContent());
			}

			@Override
			public void onQrCode(byte[] bytes) {
				try {
					// 本地存储二维码图片
					String filePath;
					try {
						filePath = new File("qrcode.png").getCanonicalPath();
					} catch (IOException e) {
						throw new IllegalStateException("二维码保存失败");
					}
					IFileProcessor ifp = new FileProcessor();
					ifp.write(filePath, bytes, MODE.WRITE_CREATE_MODE);
				} catch (MyException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void loginSuccess(UserInfo userInfo) {

			}

			@Override
			public void loginCanceled(UserInfo userInfo) {
				// TODO Auto-generated method stub
				
			}

		}, "tester", "tester");
		// 登录成功后便可以编写你自己的业务逻辑了
		List<Category> categories = client.getFriendListWithCategory();
		for (Category category : categories) {
			System.out.println(category.getName());
			for (Friend friend : category.getFriends()) {
				System.out.println(friend.getUserId() + "————" + friend.getNickname() + "----" + friend.getMarkname());
			}
		}
		List<Discuss> discussList = client.getDiscussList();
		for (int i = 0; i < discussList.size(); i++) {
			Discuss one = discussList.get(i);
			System.out.println(one.getId() + "---" + one.getName());
		}
		List<Group> groupList = client.getGroupList();
		for (int i = 0; i < groupList.size(); i++) {
			Group one = groupList.get(i);
			System.out.println(one.getId() + "---" + one.getName());
		}
		boolean b = true;
		while (b) {
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
			}
			long l = 4250764825L;
			client.sendMessageToFriend(l, "测试" + System.currentTimeMillis());
		}
		// 使用后调用close方法关闭，你也可以使用try-with-resource创建该对象并自动关闭
		try {
			client.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
