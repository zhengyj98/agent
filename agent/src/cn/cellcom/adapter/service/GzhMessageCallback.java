package cn.cellcom.adapter.service;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.cellcom.agent.common.AgentEnv;
import cn.cellcom.rpc.RpcFramework;
import cn.cellcom.rpc.entity.ChatMessage;
import cn.cellcom.rpc.entity.KfzhMessage;
import cn.cellcom.rpc.service.RpcService;

public class GzhMessageCallback {
	private static Logger log = LoggerFactory.getLogger(GzhMessageCallback.class);

	private ExecutorService pool = Executors.newFixedThreadPool(100);

	private static RpcService service;

	private static GzhMessageCallback instance;

	public static synchronized GzhMessageCallback getInstance() {
		if (instance == null) {
			instance = new GzhMessageCallback();
			try {
				service = RpcFramework.refer(RpcService.class, AgentEnv.WECHAT_RPC_REMOTE_HOST, AgentEnv.WECHAT_RPC_REMOTE_PORT);
			} catch (Exception e) {
				log.error("", e);
			}
		}
		return instance;
	}

	public synchronized void kfzh(final KfzhMessage kfzh, final String appid) {
		pool.execute(new Runnable() {
			@Override
			public void run() {
				try {
					service.kfzh(kfzh, appid);
				} catch (Exception e) {
					log.error("", e);
				}
			}
		});
	}

	public synchronized void remoteChat(final ChatMessage message, final String appid) {
		pool.execute(new Runnable() {
			@Override
			public void run() {
				try {
					service.chat(message, appid);
				} catch (Exception e) {
					log.error("", e);
				}
			}
		});
	}

}
