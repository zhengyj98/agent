package cn.cellcom.adapter.service;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.cellcom.agent.biz.TChannelBiz;
import cn.zhouyafeng.itchat4j.beans.BaseMsg;
import cn.zhouyafeng.itchat4j.core.Core;
import cn.zhouyafeng.itchat4j.face.IMsgHandlerFace;
import cn.zhouyafeng.itchat4j.service.ILoginService;
import cn.zhouyafeng.itchat4j.thread.CheckLoginStatusThread;
import cn.zhouyafeng.itchat4j.utils.tools.CommonTools;

public class WechatMessageCallback implements IMsgHandlerFace {
	private Logger log = LoggerFactory.getLogger(this.getClass());
	private TChannelBiz biz;
	private String org;
	private String pid;

	public WechatMessageCallback(TChannelBiz biz, String org, String pid) {
		this.biz = biz;
		this.org = org;
		this.pid = pid;
	}

	public enum RESULT {
		SUCCESS, ERROR, WAITING
	}

	private byte[] qrCode = new byte[0];

	public byte[] getQrCode() {
		return qrCode;
	}

	@Override
	public void onQrCode(byte[] bytes) {
		this.qrCode = bytes;
	}

	@Override
	public void loginSuccess(Core core, ILoginService loginService) {
		RESULT result = null;
		try {
			if (biz.getChannelByQq(core.getUserSelf().getString("")) == null) {
				//biz.createQqChannel(org, pid, userInfo.getAccount(), userInfo.getNick(), userInfo.getLnick());
			}
			result = RESULT.SUCCESS;
		} catch (Exception e) {
		}

		log.info("5. 登陆成功，微信初始化");
		if (!loginService.webWxInit()) {
			log.info("6. 微信初始化异常");
			System.exit(0);
		}

		log.info("6. 开启微信状态通知");
		loginService.wxStatusNotify();

		log.info("7. 清除。。。。");
		CommonTools.clearScreen();
		log.info(String.format("欢迎回来， %s", core.getNickName()));

		log.info("8. 开始接收消息");
		loginService.startReceiving();

		log.info("9. 获取联系人信息");
		loginService.webWxGetContact();

		log.info("10. 获取群好友及群好友列表");
		loginService.WebWxBatchGetContact();

		log.info("11. 缓存本次登陆好友相关消息");
		// WechatTools.setUserInfo(); // 登陆成功后缓存本次登陆好友相关消息（NickName, UserName）

		log.info("12.开启微信状态检测线程");
		new Thread(new CheckLoginStatusThread(core)).start();
	}

	@Override
	public String textMsgHandle(BaseMsg msg) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String picMsgHandle(BaseMsg msg) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String voiceMsgHandle(BaseMsg msg) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String viedoMsgHandle(BaseMsg msg) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String nameCardMsgHandle(BaseMsg msg) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void sysMsgHandle(BaseMsg msg) {
		// TODO Auto-generated method stub

	}

	@Override
	public String verifyAddFriendMsgHandle(BaseMsg msg) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String mediaMsgHandle(BaseMsg msg) {
		// TODO Auto-generated method stub
		return null;
	}

}
