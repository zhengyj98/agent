package cn.cellcom.agent.online.handler;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import cn.cellcom.agent.biz.TQuestionBiz;
import cn.cellcom.agent.common.AgentConstant;
import cn.cellcom.agent.common.AgentConstant.SELF_RESPONSE;
import cn.cellcom.agent.entity.TSettingEntity;
import cn.cellcom.agent.online.client.VisitorClient;
import cn.cellcom.agent.online.message.QuestionListMessage;
import cn.cellcom.agent.online.wrapper.SessionWrapper;
import cn.cellcom.agent.pojo.TQuestion;
import cn.cellcom.jar.util.AU;
import cn.cellcom.jar.util.LogUtil;
import cn.cellcom.jar.util.MyException;
import cn.zhouyafeng.itchat4j.tlUtil.TulingClient;

public class SelfChatHandler {

	private TulingClient tuling = new TulingClient();
	private VisitorClient client;
	private TQuestionBiz qbiz;

	public SelfChatHandler(VisitorClient client, TQuestionBiz qbiz) {
		this.client = client;
		this.qbiz = qbiz;
	}

	/**
	 * 自助服务处理
	 * 
	 * @param session
	 * @param pid
	 * @param text
	 * @param questionId
	 */
	public SELF_RESPONSE handleSelfResponse(SessionWrapper session, String text, String questionId) {
		// long start = System.currentTimeMillis();
		try {
			TSettingEntity setting = session.getSetting();
			if (setting.getRobotType() == AgentConstant.ROBOT_TYPE.TULING_ROBOT.ordinal() && StringUtils.isBlank(questionId)) {
				String answer = tuling.textMsgHandle(setting.getTulingKey(), text, client.getId());
				if(StringUtils.isBlank(answer) || answer.equals(text)) {
					session.getSystem().sysSendText(setting.getSelfUnknowTip());
					return SELF_RESPONSE.ZERO;
				} else {
					session.getSystem().sysSendText(answer);
					return SELF_RESPONSE.ONE;
				}
			}
			if (StringUtils.isNotBlank(questionId)) {// 点击问题提问
				TQuestion question = null;
				try {
					question = qbiz.getQuestion(questionId);
					// 更新次数
					question.setTimes(question.getTimes() + 1);
				} catch (MyException e) {
					LogUtil.e(this.getClass(), "[" + session.getId() + "]get question[" + questionId + "] error", e);
				}

				try {
					qbiz.modify(question);
				} catch (MyException e) {
					LogUtil.e(this.getClass(), "update question [" + question.getId() + "] times fiail.", e);
				}
				if (question == null) {
					session.getSystem().sysSendText(setting.getSelfUnknowTip());
					return SELF_RESPONSE.ZERO;
				} else {
					session.getSystem().sysSendText(question.getAnswer());
					return SELF_RESPONSE.ONE;
				}
			} else {// 直接发消息问答
				List<TQuestion> questionList = null;
				try {
					questionList = qbiz.getQuestionList(session.getPid(), text);
				} catch (MyException e) {
					LogUtil.e(this.getClass(), "[" + session.getId() + "]get question[" + questionId + "] error", e);
				}
				if (AU.isBlank(questionList)) {// 没有查找到相关模糊问题
					session.getSystem().sysSendText(setting.getSelfUnknowTip());
					return SELF_RESPONSE.ZERO;
				} else if (questionList.size() == 1) {// 只有一条则直接回复
					TQuestion question = questionList.get(0);
					session.getSystem().sysSendText(question.getAnswer());
					// 更新次数
					question.setTimes(question.getTimes() + 1);
					try {
						qbiz.modify(question);
					} catch (MyException e) {
						LogUtil.e(this.getClass(), "update question [" + question.getId() + "] times fiail.", e);
					}
					return SELF_RESPONSE.ONE;
				} else {// 多条则被选择
					QuestionListMessage m = new QuestionListMessage(session, questionList);
					client.sendBizMessage(m);
					return SELF_RESPONSE.MORE;
				}
			}
		} finally {
			// System.out.println(System.currentTimeMillis() - start);
		}
	}
}
