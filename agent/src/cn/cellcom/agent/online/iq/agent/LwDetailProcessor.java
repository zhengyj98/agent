package cn.cellcom.agent.online.iq.agent;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.cellcom.agent.biz.TLwBiz;
import cn.cellcom.agent.online.client.Client;
import cn.cellcom.agent.online.client.VisitorClient;
import cn.cellcom.agent.online.iq.AbstractChainedProcessor;
import cn.cellcom.agent.online.iq.Processor;
import cn.cellcom.agent.online.iq.ProcessorResult;
import cn.cellcom.agent.pojo.TLw;
import cn.cellcom.agent.struts.form.VisitorForm;
import cn.cellcom.jar.biz.AbstractBiz;
import cn.cellcom.jar.util.LogUtil;
import cn.cellcom.jar.util.MyException;

public class LwDetailProcessor extends AbstractChainedProcessor {

	public static final String NAME = "lwDetail";
	private Logger log = LoggerFactory.getLogger(this.getClass());

	public LwDetailProcessor(Processor next) {
		super(next);
	}

	@Override
	public ProcessorResult doProcess(Map<String, AbstractBiz> bizs, Client client2, VisitorForm cf) {
		TLw lw = null;
		try {
			TLwBiz lbiz = (TLwBiz) bizs.get("lbiz");
			lw = (TLw) lbiz.getDao().myGet(TLw.class, cf.getId());
		} catch (MyException e) {
			LogUtil.e(this.getClass(), "", e);
		}
		if(lw == null) {
			return new ProcessorResult(false, this, null);
		} else {
			if(client2 instanceof VisitorClient) {
				if(!client2.getId().equals(lw.getCrm())) {
					log.error("[{}] get other's leaveword[{}]", client2.getId(), cf.getId());
					return new ProcessorResult(false, this, null);
				}
			}
		}
		return new ProcessorResult(true, this, lw);
	}

	@Override
	public boolean isAcceptable(VisitorForm cf) {
		return NAME.equals(cf.getEvent());
	}
}
