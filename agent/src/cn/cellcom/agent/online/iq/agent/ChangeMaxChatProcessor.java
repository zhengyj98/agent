package cn.cellcom.agent.online.iq.agent;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.cellcom.agent.biz.TSettingBiz;
import cn.cellcom.agent.common.AgentConstant;
import cn.cellcom.agent.entity.TSettingEntity;
import cn.cellcom.agent.online.client.AgentClient;
import cn.cellcom.agent.online.client.Client;
import cn.cellcom.agent.online.iq.AbstractChainedProcessor;
import cn.cellcom.agent.online.iq.Processor;
import cn.cellcom.agent.online.iq.ProcessorResult;
import cn.cellcom.agent.online.message.MessageConstant;
import cn.cellcom.agent.pojo.TSetting;
import cn.cellcom.agent.struts.form.VisitorForm;
import cn.cellcom.jar.biz.AbstractBiz;
import cn.cellcom.jar.util.AU;
import cn.cellcom.jar.util.CU;
import cn.cellcom.jar.util.LogUtil;
import cn.cellcom.jar.util.MyException;

public class ChangeMaxChatProcessor extends AbstractChainedProcessor {

	private static final String NAME = MessageConstant.MESSAGE_EVENT.AG_CHANGE_MAXCHAT.name();

	private Logger log = LoggerFactory.getLogger(this.getClass());

	public ChangeMaxChatProcessor(Processor next) {
		super(next);
	}

	@Override
	public ProcessorResult doProcess(Map<String, AbstractBiz> bizs, Client client, VisitorForm cf) {
		AgentClient ac = (AgentClient) client;
		// 设置然后再次通知其他坐席
		ac.getAgentConfig().setMaxChat(CU.toi(cf.getValue()));
		ac.notifyAgentInfo(false);
		//同时修改数据库的内容
		try {
			TSettingBiz sbiz = (TSettingBiz) bizs.get("sbiz");
			TSetting one = new TSetting();
			one.setOrg(ac.getOrg());
			one.setPid(ac.getAgent().getPid());
			one.setK(TSettingEntity.CHAT_MAX_LIMIT);
			one.setV(cf.getValue());
			one.setSubject(client.getId());
			one.setLevel(AgentConstant.SETTING_LEVEL.AGENT.ordinal());
			List<TSetting> settingList = AU.getNewList(one);
			sbiz.modifyOrgSetting(ac.getAgent().getPid(), settingList);
		} catch (MyException e) {
			LogUtil.e(this.getClass(), "", e);
			return new ProcessorResult(false, this, null);
		}
		log.info("[{}] set the maxchat to [{}]", ac.getId(), cf.getValue(), cf.getEvent());
		return new ProcessorResult(true, this, null);
	}

	@Override
	public boolean isAcceptable(VisitorForm message) {
		return NAME.equals(message.getEvent());
	}
}
