package cn.cellcom.agent.online.iq.agent;

import cn.cellcom.agent.online.iq.GetMessageProessor;
import cn.cellcom.agent.online.iq.Processor;

public class AgentProcessors {
	static Processor processor = null;

	static {
		GetMessageProessor unknown = new GetMessageProessor(null);
		AgentInfoProcessor agentInfo = new AgentInfoProcessor(unknown);
		SettingProcessor setting = new SettingProcessor(agentInfo);
		CrmLogProcessor crmLog = new CrmLogProcessor(setting);
		GroupsAndMemberProcessor gam = new GroupsAndMemberProcessor(crmLog);
		SessionListProcessor sessionList = new SessionListProcessor(gam);
		AgentActionProcessor agentMessage = new AgentActionProcessor(sessionList);
		ChangeMaxChatProcessor changeMaxChat = new ChangeMaxChatProcessor(agentMessage);
		CountMessageProcessor countMessage = new CountMessageProcessor(changeMaxChat);
		LogoutProcessor logout = new LogoutProcessor(countMessage);
		CrmFieldProcessor crmInfo = new CrmFieldProcessor(logout);
		CrmInfoProcessor crmField = new CrmInfoProcessor(crmInfo);
		ModifyCrmProcessor modifyCrm = new ModifyCrmProcessor(crmField);
		CrmHistoryProcessor crmHistory = new CrmHistoryProcessor(modifyCrm);
		CrmTraceProcessor crmTrace = new CrmTraceProcessor(crmHistory);
		CrmSessionProcessor crmSession = new CrmSessionProcessor(crmTrace);
		LwListProcessor lwList = new LwListProcessor(crmSession);
		LwDetailProcessor lwDetail = new LwDetailProcessor(lwList);
		AgentInviteProcessor invite = new AgentInviteProcessor(lwDetail);
		processor = invite;
	}

	private AgentProcessors() {
	}

	public static synchronized Processor createProcessors() {
		return processor;
	}
}
