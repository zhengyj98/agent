package cn.cellcom.agent.online.iq.agent;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.cellcom.agent.biz.TCrmBiz;
import cn.cellcom.agent.online.client.Client;
import cn.cellcom.agent.online.iq.AbstractChainedProcessor;
import cn.cellcom.agent.online.iq.Processor;
import cn.cellcom.agent.online.iq.ProcessorResult;
import cn.cellcom.agent.pojo.TCrmField;
import cn.cellcom.agent.struts.form.VisitorForm;
import cn.cellcom.jar.biz.AbstractBiz;
import cn.cellcom.jar.util.MyException;

public class CrmFieldProcessor extends AbstractChainedProcessor {

	public static final String NAME = "crmField";

	private Logger log = LoggerFactory.getLogger(this.getClass());

	public CrmFieldProcessor(Processor next) {
		super(next);
	}

	@Override
	public ProcessorResult doProcess(Map<String, AbstractBiz> bizs, Client client, VisitorForm cf) {
		TCrmBiz cbiz = (TCrmBiz) bizs.get("cbiz");
		List<TCrmField> fields = null;
		try {
			fields = cbiz.getEnableCrmField(client.getOrg());
		} catch (MyException e) {
			log.error("[{}] get crmifo[{}]", client.getId(), cf.getCid());
			return new ProcessorResult(false, this, "");
		}
		return new ProcessorResult(true, this, fields);
	}

	@Override
	public boolean isAcceptable(VisitorForm message) {
		return NAME.equals(message.getEvent());
	}
}
