package cn.cellcom.agent.online.iq.agent;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.cellcom.agent.common.AgentConstant;
import cn.cellcom.agent.online.client.AgentClient;
import cn.cellcom.agent.online.client.Client;
import cn.cellcom.agent.online.handler.LogHandler;
import cn.cellcom.agent.online.iq.AbstractChainedProcessor;
import cn.cellcom.agent.online.iq.Processor;
import cn.cellcom.agent.online.iq.ProcessorResult;
import cn.cellcom.agent.struts.form.VisitorForm;
import cn.cellcom.jar.biz.AbstractBiz;

public class CrmLogProcessor extends AbstractChainedProcessor {

	public static final String NAME = "crmLog";

	private Logger log = LoggerFactory.getLogger(this.getClass());

	public CrmLogProcessor(Processor next) {
		super(next);
	}

	@Override
	public ProcessorResult doProcess(Map<String, AbstractBiz> bizs, Client client, VisitorForm cf) {
		AgentClient ac = (AgentClient) client;
		String cid = cf.getCid();
		String sid = cf.getSid();
		int start = cf.getStart();
		int count = cf.getCount();
		List list = LogHandler.getLog(ac.getAgent().getPid(), cid, sid, start, count, AgentConstant.USER_TYPE_AGENT);
		return new ProcessorResult(true, this, list);
	}

	@Override
	public boolean isAcceptable(VisitorForm message) {
		return NAME.equals(message.getEvent());
	}
}
