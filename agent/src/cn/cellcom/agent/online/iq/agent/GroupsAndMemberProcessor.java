package cn.cellcom.agent.online.iq.agent;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.cellcom.agent.biz.TGroupBiz;
import cn.cellcom.agent.biz.TUserBiz;
import cn.cellcom.agent.online.client.AgentClient;
import cn.cellcom.agent.online.client.Client;
import cn.cellcom.agent.online.iq.AbstractChainedProcessor;
import cn.cellcom.agent.online.iq.Processor;
import cn.cellcom.agent.online.iq.ProcessorResult;
import cn.cellcom.agent.pojo.TGroup;
import cn.cellcom.agent.struts.form.VisitorForm;
import cn.cellcom.jar.biz.AbstractBiz;
import cn.cellcom.jar.util.LogUtil;
import cn.cellcom.jar.util.MyException;

public class GroupsAndMemberProcessor extends AbstractChainedProcessor {

	public static final String NAME = "groupsAndMember";

	private Logger log = LoggerFactory.getLogger(this.getClass());

	public GroupsAndMemberProcessor(Processor next) {
		super(next);
	}

	@Override
	public ProcessorResult doProcess(Map<String, AbstractBiz> bizs, Client client, VisitorForm cf) {
		AgentClient ac = (AgentClient) client;
		List list = new ArrayList();
		try {
			TUserBiz ubiz = (TUserBiz) bizs.get("ubiz");
			TGroupBiz gbiz = (TGroupBiz) bizs.get("gbiz");

			List<TGroup> groups = ubiz.getGroups(client.getId());
			list = gbiz.getGroupsAndMembers(ac.getAgent().getPid(), groups);
		} catch (MyException e) {
			LogUtil.e(this.getClass(), "", e);
		}
		return new ProcessorResult(true, this, list);
	}

	@Override
	public boolean isAcceptable(VisitorForm message) {
		return NAME.equals(message.getEvent());
	}
}
