package cn.cellcom.agent.online.dispatcher;

import java.util.Comparator;

import cn.cellcom.agent.online.client.AgentClient;
import cn.cellcom.agent.online.wrapper.SessionWrapper;

public class CurrentFreeAgentSelector extends AbstractAgentSelector {

	@Override
	public AgentClient bestAgentFrom(SessionWrapper session) {
		return super.bestAgentFrom(session, new AgentClientComparator());
	}

	private class AgentClientComparator implements Comparator<AgentClient> {
		@Override
		public int compare(AgentClient o1, AgentClient o2) {
			Float f1 = (float)(o1.getAgentConfig().getMaxChat() - o1.getCurrentChat()) / o1.getAgentConfig().getMaxChat();
			Float f2 = (float)(o2.getAgentConfig().getMaxChat() - o2.getCurrentChat()) / o2.getAgentConfig().getMaxChat();
			return f1.compareTo(f2);
		}
	}

}
