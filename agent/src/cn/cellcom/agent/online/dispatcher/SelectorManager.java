package cn.cellcom.agent.online.dispatcher;

import java.util.HashMap;
import java.util.Map;

import cn.cellcom.agent.common.AgentConstant.SESSION_DISPATCH_POLICY;

/**
 * 选择器管理
 * 
 * @author zhengyj
 * 
 */
public class SelectorManager {

	static Map<Integer, IAgentSelector> map = new HashMap<Integer, IAgentSelector>();
	static {
		map.put(SESSION_DISPATCH_POLICY.RANDOM.ordinal(), new RandomAgentSelector());
		map.put(SESSION_DISPATCH_POLICY.CURRENT_MIN.ordinal(), new CurrentMinAgentSelector());
		map.put(SESSION_DISPATCH_POLICY.CURRENT_FREE.ordinal(), new CurrentFreeAgentSelector());
		map.put(SESSION_DISPATCH_POLICY.LEVEL.ordinal(), new LevelAgentSelector());
		map.put(SESSION_DISPATCH_POLICY.LAST_AGENT.ordinal(), new LastAgentSelector());
	}

	public static IAgentSelector getSelector(Integer policy) {
		return map.get(policy);
	}
}
