package cn.cellcom.agent.online.client;

import java.net.URLEncoder;

import org.apache.commons.lang.StringUtils;

import cn.cellcom.jar.util.IDGenerator;
import cn.cellcom.jar.util.MD5;

public class IDManager {

	public static String getCid() {
		return "CRM_" + IDGenerator.getDefaultUUID();
	}

	public static String getMid() {
		return "M_" + IDGenerator.getDefaultUUID();
	}

	public static String getSid() {
		return "SSN_" + IDGenerator.getDefaultUUID();
	}

	public static String getAgSid() {
		return "AGSSN_" + IDGenerator.getDefaultUUID();
	}

	/**
	 * 如果企业没有配置accesskey，那么就认为验证通过
	 * @param tid
	 * @param timestamp
	 * @param accesskey
	 * @param token
	 * @return
	 */
	public static boolean validate(String tid, String timestamp, String accesskey, String token) {
		if(StringUtils.isBlank(accesskey)) {
			return true;
		}
		return MD5.toMd5(tid + timestamp + accesskey).equals(token);
	}

	public static String getToken(String tid, String timestamp, String accesskey) {
		return MD5.toMd5(tid + timestamp + accesskey);
	}
	
	public static void main(String[] args) {
		System.out.println(MD5.toMd5("XXXX01" + "JIANYUE"));
	}

	public static String getCfid() {
		return "CF_" + IDGenerator.getDefaultUUID();
	}

	public static String getGid() {
		return "G_" + IDGenerator.getDefaultUUID();
	}

	public static String getGUid() {
		return "GU_" + IDGenerator.getDefaultUUID();
	}

	public static String getQid() {
		return "QST_" + IDGenerator.getDefaultUUID();
	}

	public static String getLid() {
		return "LW_" + IDGenerator.getDefaultUUID();
	}	
}
