package cn.cellcom.agent.online.client;

import java.util.HashMap;
import java.util.Map;

import cn.cellcom.agent.common.AgentConstant.MEMBER_STATUS;
import cn.cellcom.agent.online.message.MessageConstant;
import cn.cellcom.agent.online.message.MessageConstant.MESSAGE_NOTIFY;

public class AgentWechatNotify {

	private static Map<String, String> map = new HashMap<String, String>();
	static {
		//map.put(MessageConstant.MESSAGE_NOTIFY.END_SESSION.toString(), "会话已经结束，期待您下次来访");
		//map.put(MessageConstant.MESSAGE_NOTIFY.ENTER_SELF.toString(), "您已进入人机交互");
		map.put(MessageConstant.MESSAGE_NOTIFY.MEMBER_STATUS.toString() + "-" + MEMBER_STATUS.ONLINE, "坐席正在为您服务");
		//map.put(MessageConstant.MESSAGE_NOTIFY.MEMBER_STATUS.toString() + "-" + MEMBER_STATUS.OFFLINE, "坐席暂时离开");
		map.put(MessageConstant.MESSAGE_NOTIFY.QUEUE_POSITION.toString(), "您正排队,请稍等");
	}

	/**
	 * 通知转为文字
	 * 
	 * @param notify
	 * @param arg
	 * @return
	 */
	public static String get(MESSAGE_NOTIFY notify, Object arg) {
		if (notify.equals(MessageConstant.MESSAGE_NOTIFY.MEMBER_STATUS)) {
			Map map2 = (Map) arg;
			return map.get(notify.toString() + "-" + map2.get("status"));
		}
		return map.get(notify.toString());
	}
}
