package cn.cellcom.agent.online.client;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.cellcom.agent.common.AgentConstant;
import cn.cellcom.agent.common.AgentConstant.RESULT;
import cn.cellcom.agent.online.message.BizMessage;
import cn.cellcom.agent.online.message.MessageConstant;
import cn.cellcom.agent.pojo.TChannel;
import cn.cellcom.jar.util.LogUtil;

public abstract class Client {
	private Logger log = LoggerFactory.getLogger(this.getClass());

	private long lastActiveGet = 0;

	private ArrayBlockingQueue<BizMessage> blockingQueue;

	public Client() {
		blockingQueue = new ArrayBlockingQueue<BizMessage>(500);
	}

	public abstract Map<String, String> getIdMap();

	public abstract String getHttpSessionId();

	public abstract String getId();

	public abstract String getOrg();

	public abstract String getSender(TChannel tChannel);

	public abstract String getNickname();

	/**
	 * 读取消息队列中的x条
	 * 
	 * @param x
	 * @return
	 */
	public List<BizMessage> getXMessage(int x) {
		List<BizMessage> mlist = new ArrayList<BizMessage>();
		int start = 1;
		while (start++ < x && blockingQueue.size() > 0) {
			BizMessage m = null;
			try {
				m = blockingQueue.take();
				mlist.add(m);
			} catch (InterruptedException e) {
				LogUtil.e(this.getClass(), m.tojson(), e);
			}
		}
		return mlist;
	}

	/**
	 * 当前的消息数量
	 * 
	 * @return
	 */
	public int getMessageCount() {
		return blockingQueue.size();
	}

	/**
	 * 接收消息
	 * 
	 * @param m
	 */
	protected boolean receiveMessage(BizMessage m) {
		try {
			blockingQueue.put(m);
		} catch (InterruptedException e) {
			LogUtil.e(this.getClass(), m.tojson(), e);
			return false;
		}
		return true;
	}

	/*
	 * public boolean erroReceipt(String id) { BizMessage m = new BizMessage();
	 * m.setId(id); m.setCode(AgentConstant.RESULT.ERROR.ordinal());
	 * m.setEvent(MESSAGE_EVENT.RECEIPT); try { blockingQueue.put(m); } catch
	 * (InterruptedException e) { LogUtil.e(this.getClass(), m.tojson(), e); return
	 * false; } return true; }
	 */

	/**
	 * 默认的系统消息字段内容
	 * 
	 * @param m
	 * @return
	 */
	public RESULT sendBizMessage(BizMessage m) {
		if (m.getMultiType() == null) {
			m.setMultiType(MessageConstant.MULIT_TYPE.TEXT);
		}
		if (this instanceof SystemClient) {
			m.setSenderRole(AgentConstant.USER_TYPE_SYSTEM);
		} else if (this instanceof AgentClient) {
			m.setSenderRole(AgentConstant.USER_TYPE_AGENT);
		} else if (this instanceof VisitorClient) {
			m.setSenderRole(AgentConstant.USER_TYPE_VISITOR);
		}
		Client client = ClientManager.getInstance().getClient(m.getReceiver());
		if (client != null && client instanceof VisitorClient) {// 此情况是为了获取坐席xxx@xxx的格式给微信显示
			m.setSender(this.getSender(((VisitorClient) client).getChannelObject()));
		} else {
			m.setSender(this.getSender(null));
		}
		m.setSenderNickname(this.getNickname());
		m.setOrg(this.getOrg());
		return sendMessage(m);
	}

	/**
	 * 发送消息并归档
	 * 
	 * @param m
	 * @return
	 */
	private RESULT sendMessage(BizMessage m) {
		if (StringUtils.isBlank(m.getOrg()) || StringUtils.isBlank(m.getSender()) || StringUtils.isBlank(m.getReceiver()) || m.getEvent() == null) {
			LogUtil.e(this.getClass(), "Message is half-baked : " + m);
			return RESULT.ARG_ERROR;
		}
		if (m.getId() == null) {
			m.setId(IDManager.getMid());
		}
		AgentConstant.RESULT result = AgentConstant.RESULT.SUCCESS;
		Client receiver = ClientManager.getInstance().getClient(m.getReceiver());
		if (receiver == null) {
			m.setResult(AgentConstant.RESULT.NO_RECEIVER);
			LogUtil.i(this.getClass(), "The receiver " + m.getReceiver() + " is offline?");
			return RESULT.SUCCESS;
		}
		result = receiver.receiveMessage(m) ? RESULT.SUCCESS : RESULT.ERROR;
		m.setResult(result);

		return result;
	}

	public long getLastActiveGet() {
		return lastActiveGet;
	}

	public void setLastActiveGet() {
		this.lastActiveGet = System.currentTimeMillis();
	}

}
