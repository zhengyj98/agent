package cn.cellcom.agent.online.client;

import org.apache.commons.lang.StringUtils;

import com.scienjus.smartqq.client.SmartQQClient;

import cn.cellcom.agent.biz.TChannelBiz;
import cn.cellcom.agent.biz.TGroupBiz;
import cn.cellcom.agent.biz.TQuestionBiz;
import cn.cellcom.agent.biz.TSessionBiz;
import cn.cellcom.agent.online.message.BizMessage;
import cn.cellcom.agent.online.message.MessageConstant;
import cn.cellcom.agent.pojo.TCrm;

public class QqClient extends VisitorClient {

	private SmartQQClient agentQQClient = null;

	public QqClient(TChannelBiz chbiz, TSessionBiz ssbiz, TGroupBiz gbiz, TQuestionBiz qbiz, String channel, TCrm crm, String httpSessionId) {
		super(chbiz, ssbiz, gbiz, qbiz, channel, crm, httpSessionId);
	}

	public void setAgentQQClient(SmartQQClient agentQQClient) {
		this.agentQQClient = agentQQClient;
	}

	@Override
	protected boolean receiveMessage(BizMessage m) {
		if (StringUtils.isNotEmpty(m.getText()) || m.getEvent().equals(MessageConstant.MESSAGE_EVENT.NOTIFY)) {
			if (m.getEvent().equals(MessageConstant.MESSAGE_EVENT.NOTIFY)) {
				m.setText(AgentWechatNotify.get(m.getNotify(), m.getObject()));
			}
			if (StringUtils.isNotBlank(m.getText())) {
				agentQQClient.sendMessageToFriend(Long.parseLong(this.getId()), m.getText());
			}
		}
		return true;
	}
}
