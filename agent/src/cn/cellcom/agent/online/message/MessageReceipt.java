package cn.cellcom.agent.online.message;

import cn.cellcom.agent.common.AgentConstant;
import cn.cellcom.agent.online.message.MessageConstant.MESSAGE_EVENT;
import cn.cellcom.jar.util.DT;

/**
 * @author zhengyj
 * 
 */
public class MessageReceipt extends BizMessage {

	public MessageReceipt(String id, boolean success) {
		super.setId(id);
		if (success) {
			super.setCode(AgentConstant.RESULT.SUCCESS.ordinal());
		} else {
			super.setCode(AgentConstant.RESULT.ERROR.ordinal());
		}
		super.setTime(DT.getYYYYMMDDHHMMSS());
		super.setEvent(MESSAGE_EVENT.RECEIPT);
	}
}
