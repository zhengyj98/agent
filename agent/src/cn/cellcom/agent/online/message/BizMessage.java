package cn.cellcom.agent.online.message;

import cn.cellcom.agent.common.AgentConstant;
import cn.cellcom.agent.online.message.MessageConstant.MULIT_TYPE;
import cn.cellcom.jar.util.DT;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class BizMessage {
	/**
	 * 消息的结果，默认当然是0了
	 */
	private int code = 0;

	/**
	 * 消息的ID
	 */
	private String id;

	/**
	 * 企业ID
	 */
	private String org = "";

	/**
	 * 
	 */
	private String pid;

	/**
	 * 消息类型
	 */
	private MULIT_TYPE multiType = null;

	/**
	 * 消息事件
	 */
	private MessageConstant.MESSAGE_EVENT event = null;

	/**
	 * 消息事件为通知时，这个变量有效
	 */
	private MessageConstant.MESSAGE_NOTIFY notify = null;

	/**
	 * 管理员/坐席/访客
	 */
	private String senderRole = "";

	/**
	 * 发送者的ID
	 */
	private String sender = "";

	/**
	 * 发送者的昵称
	 */
	private String senderNickname = "";

	/**
	 * 接收者的ID
	 */
	private String receiver = "";

	/**
	 * 消息事件，采用系统事件
	 */
	private String time = DT.getYYYYMMDDHHMMSS();

	/**
	 * 发送的文本内容
	 */
	private String text = null;

	/**
	 * 发送的对象
	 */
	private Object object;

	/**
	 * 显示的策略： 0表示多方可以查看，1表示仅坐席，2表示仅访客，3表示仅管理，默认是所有
	 */
	private MessageConstant.ARCHIVE_SHOW_POLICY showPolicy = MessageConstant.ARCHIVE_SHOW_POLICY.ALL;

	/**
	 * 发送结果
	 */
	private AgentConstant.RESULT result = AgentConstant.RESULT.SUCCESS;

	/**
	 * 会话的ID
	 */
	private String sid;

	/**
	 * 访客ID
	 */
	private String cid;

	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public AgentConstant.RESULT getResult() {
		return result;
	}

	public void setResult(AgentConstant.RESULT result) {
		this.result = result;
	}

	public MessageConstant.ARCHIVE_SHOW_POLICY getShowPolicy() {
		return showPolicy;
	}

	public void setShowPolicy(MessageConstant.ARCHIVE_SHOW_POLICY showPolicy) {
		this.showPolicy = showPolicy;
	}

	public MessageConstant.MESSAGE_NOTIFY getNotify() {
		return notify;
	}

	public void setNotify(MessageConstant.MESSAGE_NOTIFY notify) {
		this.notify = notify;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getSenderNickname() {
		return senderNickname;
	}

	public void setSenderNickname(String senderNickname) {
		this.senderNickname = senderNickname;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getOrg() {
		return org;
	}

	public void setOrg(String org) {
		this.org = org;
	}

	public MULIT_TYPE getMultiType() {
		return multiType;
	}

	public void setMultiType(MULIT_TYPE multiType) {
		this.multiType = multiType;
	}

	public String getSenderRole() {
		return senderRole;
	}

	public void setSenderRole(String senderRole) {
		this.senderRole = senderRole;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String tojson() {
		return JSONArray.fromObject(this).toString();
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public MessageConstant.MESSAGE_EVENT getEvent() {
		return event;
	}

	public void setEvent(MessageConstant.MESSAGE_EVENT event) {
		this.event = event;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String toString() {
		return "BizMessage [code=" + code + ", id=" + id + ", org=" + org + ", senderRole=" + senderRole + ", sender=" + sender + ", receiver="
				+ receiver + ", senderNickname=" + senderNickname + ", time=" + time + ", text=" + text + ", notify=" + notify + ", event=" + event
				+ ", object=" + object + "]";
	}

	public static BizMessage toMessage(String str) {
		JSONObject sfObject = JSONObject.fromObject(str);
		BizMessage msg = (BizMessage) JSONObject.toBean(sfObject, BizMessage.class);
		return msg;
	}
}
