package cn.cellcom.agent.online.message;

public class ChatMessage extends BizMessage {

	public ChatMessage(String receiver, String text, String multiType) {
		super.setEvent(MessageConstant.MESSAGE_EVENT.CHAT);
		super.setMultiType(MessageConstant.MULIT_TYPE.valueOf(multiType));
		super.setReceiver(receiver);
		super.setText(text);		
	}
}
