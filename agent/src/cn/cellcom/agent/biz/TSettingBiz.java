package cn.cellcom.agent.biz;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;

import cn.cellcom.agent.common.AgentConstant;
import cn.cellcom.agent.entity.TSettingEntity;
import cn.cellcom.agent.pojo.TSetting;
import cn.cellcom.jar.biz.AbstractBiz;
import cn.cellcom.jar.reflect.JavaBase;
import cn.cellcom.jar.reflect.JavaBase.GetterOrSetter;
import cn.cellcom.jar.reflect.MethodUtil;
import cn.cellcom.jar.util.AU;
import cn.cellcom.jar.util.CU;
import cn.cellcom.jar.util.IDGenerator;
import cn.cellcom.jar.util.MyException;

public class TSettingBiz extends AbstractBiz {

	@Override
	public List delete(HttpServletRequest req, ActionForm form, Object obj) {
		return null;
	}

	@Override
	public Object add(HttpServletRequest req, ActionForm form, Object obj) {
		return null;
	}

	@Override
	public Object modify(HttpServletRequest req, ActionForm form, Object obj) {
		return null;
	}
	
	/**
	 * 数据库记录转换为Setting对象
	 * @param list
	 * @return
	 * @throws MyException
	 */
	private TSettingEntity list2SettingEntity(List<TSetting> list) throws MyException {
		MethodUtil mu = new MethodUtil();
		//将配置列表转换为一个定义好的对象
		TSettingEntity setting = new TSettingEntity();
		for (int i = 0; i < list.size(); i++) {
			TSetting one = list.get(i);
			String key = one.getK();
			String value = one.getV(); 			
			/*if(key.startsWith("tuling")) {
				System.out.println(value);
			}*/
			//看看实体中是否已经有值
			String getter = JavaBase.toGetterOrSetterName(key, GetterOrSetter.GETTER);
			Object had = mu.invoke(setting, new MethodUtil().newAMethod(getter, new Object[] {}));
			if (had == null) {//没有值则进行设置
				String setter = JavaBase.toGetterOrSetterName(key, GetterOrSetter.SETTER);
				//如果是Integer类型就转换，这个办法不是太好
				Object value2 = null;
				if(CU.isNumber(value)) {// todo 这个判断有问题，如果string输入数字会错误
					value2 = Integer.parseInt(value);
				} else {
					value2 = value;
				}
				mu.invoke(setting, new MethodUtil().newAMethod(setter, new Object[] { value2 }));
			}
		}
		return setting;
	}

	/**
	 * 查询某个企业应用的配置
	 * 
	 * @param org
	 * @param app
	 * @param levels
	 * @return
	 * @throws MyException 
	 */
	public TSettingEntity getOrgSetting(String org, String pid) throws MyException {
		//查询app已经配置的参数，包括app的配置
		String hql = "from " + TSetting.class.getSimpleName() + " where (org = ? and pid = ? and level=?) or level=0 order by level desc";
		Object[] values = new Object[]{org, pid, AgentConstant.SETTING_LEVEL.ORG.ordinal()};
		
		List<TSetting> list = this.getDao().myList(hql, values);
		return list2SettingEntity(list);
	}

	/**
	 * 获取渠道的配置
	 * @param org
	 * @param subject 渠道对象
	 * @param onlyMyLevel 是否仅查询本基本的配置，不往上级别查询,如果为false则表示:当渠道不存在特殊配置则往上级别查询
	 * @return
	 * @throws MyException
	 */
	public TSettingEntity getChannelSetting(String pid, String subject, boolean onlyMyLevel) throws MyException {
		MethodUtil mu = new MethodUtil();
		//查询app已经配置的参数，包括app的配置
		String hql = "from " + TSetting.class.getSimpleName() + " where level=0 or (pid = ? and level=?) or (pid = ? and subject=? and level=?) order by level desc";
		Object[] values = new Object[]{pid, AgentConstant.SETTING_LEVEL.ORG.ordinal(), pid, subject, AgentConstant.SETTING_LEVEL.CHANNEL.ordinal()};
		if(onlyMyLevel) {
			hql = "from " + TSetting.class.getSimpleName() + " where pid = ? and subject=? and level=?";
			values = new Object[]{pid, subject, AgentConstant.SETTING_LEVEL.CHANNEL.ordinal()};
		}
		
		List<TSetting> list = this.getDao().myList(hql, values);
		return list2SettingEntity(list);
	}

	/**
	 * 获取坐席的配置
	 * @param org
	 * @param subject
	 * @param onlyMyLevel
	 * @return
	 * @throws MyException
	 */
	public TSettingEntity getAgentSetting(String pid, String subject, boolean onlyMyLevel) throws MyException {
		MethodUtil mu = new MethodUtil();
		//查询app已经配置的参数，包括app的配置
		String hql = "from " + TSetting.class.getSimpleName() + " where level=0 or (pid =? and level=?) or (subject=? and level=?) order by level desc";
		Object[] values = new Object[]{pid, AgentConstant.SETTING_LEVEL.ORG.ordinal(),subject, AgentConstant.SETTING_LEVEL.AGENT.ordinal()};
		if(onlyMyLevel) {
			hql = "from " + TSetting.class.getSimpleName() + " where pid = ? and subject=? and level=?";
			values = new Object[]{pid, subject, AgentConstant.SETTING_LEVEL.AGENT.ordinal()};
		}
		
		List<TSetting> list = this.getDao().myList(hql, values);
		return list2SettingEntity(list);
	}

	/**
	 * 修改企业的配置
	 * @param org
	 * @param tag
	 * @param settingList
	 * @return
	 * @throws MyException 
	 */
	public TSettingEntity modifyOrgSetting(String pid, List<TSetting> settingList) throws MyException {
		for (int i = 0; i < settingList.size(); i++) {
			TSetting setting = settingList.get(i);
			//if exist the setting of the key for org
			String hql = "from " + TSetting.class.getSimpleName() + " where pid = ? and k=? and level = ?";
			Object[] values = new Object[]{pid, setting.getK(), setting.getLevel()};
			if(StringUtils.isNotBlank(setting.getSubject())) {
				hql = "from " + TSetting.class.getSimpleName() + " where pid = ? and k=? and level = ? and subject=?";
				values = new Object[]{pid, setting.getK(), setting.getLevel(), setting.getSubject()};
			}
			List<TSetting> list = this.getDao().myList(hql, values);
			//not exis in db
			if(AU.isBlank(list)) {
				setting.setId(IDGenerator.getDefaultUUID());
				this.getDao().mySave(setting);
			} else {//exist in db
				TSetting exist = list.get(0);
				exist.setV(setting.getV());
				this.getDao().myUpdate(exist);
			}
		}
		return null;
	}

	/**
	 * 查询某个企业应用的配置
	 * 
	 * @param org
	 * @param app
	 * @param levels
	 * @return
	 */
	public List<TSettingEntity> getChannelSettingList(String org, int channel) {
		return null;
	}

	/**
	 * 查询某个坐席的配置
	 * 
	 * @param org
	 * @param app
	 * @param levels
	 * @return
	 */
	public List<TSettingEntity> getChannelSettingList(String org, String agent) {
		return null;
	}

}
