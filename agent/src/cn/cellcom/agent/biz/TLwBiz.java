package cn.cellcom.agent.biz;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;

import cn.cellcom.agent.common.AgentConstant;
import cn.cellcom.agent.pojo.TCrm;
import cn.cellcom.agent.pojo.TLw;
import cn.cellcom.agent.pojo.TUser;
import cn.cellcom.agent.struts.form.TLwForm;
import cn.cellcom.jar.biz.AbstractBiz;
import cn.cellcom.jar.logon.LogonSession;
import cn.cellcom.jar.reflect.ToString;
import cn.cellcom.jar.util.CopyUtil;
import cn.cellcom.jar.util.DT;
import cn.cellcom.jar.util.LogUtil;
import cn.cellcom.jar.util.MyException;

public class TLwBiz extends AbstractBiz {

	@Override
	public List delete(HttpServletRequest req, ActionForm form, Object obj) {

		return null;
	}

	@Override
	public Object add(HttpServletRequest req, ActionForm form, Object obj) {
		return null;
	}

	@Override
	public Object modify(HttpServletRequest req, ActionForm form, Object obj) {
		TLwForm fm = (TLwForm) form;
		LogonSession ls = new LogonSession(req);
		TUser user = (TUser) ls.getLogonObject();
		try {
			log.info("modify log：" + ls.getAttribute("attribute.id") + "[" + req.getRemoteAddr() + "]");
			log.info(ToString.toString(fm, false));
		} catch (MyException e) {
		}
		try {
			java.lang.String key = fm.getId();
			TLw pojo = (TLw) this.getDao().myGet(TLw.class, key);
			if (pojo == null) {
				return "修改的记录不存在";
			}
			log.info("被修改的pojo：" + ToString.toString(TLw.class, pojo, false));

			// 以下为pojo的必填字段
			CopyUtil.copy(req, pojo, null, false);
			pojo.setHandleUser(user.getName());
			pojo.setHandleTime(System.currentTimeMillis());
			this.getDao().myUpdate(pojo);

			return pojo;
		} catch (MyException e) {
			return LogUtil.e(this.getClass(), "修改数据出现错误", e);
		}

	}

	public List getNoHandled(String pid) throws MyException {
		String hql = "from " + TLw.class.getSimpleName() + " as a, " + TCrm.class.getSimpleName()
				+ " as b where a.crm=b.id and a.status = ? and a.pid=? order by a.createTime desc";
		return this.getDao().myList(hql, new String[] { AgentConstant.NO, pid });
	}

	public List getHandled(String pid) throws MyException {
		String hql = "from " + TLw.class.getSimpleName() + " as a, " + TCrm.class.getSimpleName()
				+ " as b where a.crm=b.id and a.status = ? and a.pid=? AND a.createTime>=? order by a.createTime desc limit 50";
		return this.getDao().myList(hql, new Object[] { AgentConstant.YES, pid, System.currentTimeMillis() - DT.ADAY * 10 });
	}
}
