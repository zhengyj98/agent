package cn.cellcom.agent.util;

import java.util.ArrayList;
import java.util.List;

public class ProvinceAndCity {

	final static List<String> provinceList = new ArrayList<String>();

	static {
		provinceList.add("内蒙古");
		provinceList.add("西藏");
		provinceList.add("宁夏");
		provinceList.add("新疆");
		provinceList.add("广西");

		provinceList.add("香港");
		provinceList.add("澳门");
		provinceList.add("台湾");

		provinceList.add("江西");
		provinceList.add("河北");
		provinceList.add("山西");
		provinceList.add("辽宁");
		provinceList.add("吉林");
		provinceList.add("黑龙江");
		provinceList.add("江西");
		provinceList.add("江苏");
		provinceList.add("浙江");
		provinceList.add("安徽");
		provinceList.add("福建");
		provinceList.add("山东");
		provinceList.add("河南");
		provinceList.add("湖北");
		provinceList.add("湖南");
		provinceList.add("广东");
		provinceList.add("海南");
		provinceList.add("四川");
		provinceList.add("贵州");
		provinceList.add("云南");
		provinceList.add("陕西");
		provinceList.add("甘肃");
		provinceList.add("江苏");
		provinceList.add("青海");
	}

	public static void main(String[] args) {
		/*
		 * System.out.println(diparseProvince("江西省南昌市"));
		 * System.out.println(diparseProvince("江西南昌市"));
		 * System.out.println(diparseProvince("江西省南昌"));
		 * System.out.println(diparseProvince("江西南昌"));
		 */
		System.out.println(diparseProvince("澳门特别行政区阿道夫"));
		/*
		 * System.out.println(diparseProvince("澳门2"));
		 * System.out.println(diparseProvince("台湾23"));
		 */
	}

	public static Belong diparseProvince(String province) {
		String proValue = "";
		String cityValue = "";
		int proIndex = province.indexOf("省");
		int cityIndex = province.indexOf("市");
		if (proIndex != -1 && cityIndex != -1) {// 有省有市
			proValue = province.substring(0, proIndex);
			cityValue = province.substring(proIndex + 1, cityIndex);
			return new Belong(proValue, cityValue);
		} else if (proIndex != -1 && cityIndex == -1) {// 有省无市
			proValue = province.substring(0, proIndex);
			cityValue = province.substring(proIndex + 1);
			return new Belong(proValue, cityValue);
		} else if (proIndex == -1) {// 没有省字
			int i = 0;
			while (i < provinceList.size()) {
				if (province.equals(provinceList.get(i)) && provinceList.get(i).equals("香港")) {// 香港
					return new Belong(provinceList.get(i), "");
				}
				if (province.equals(provinceList.get(i)) && provinceList.get(i).equals("澳门")) {// 澳门
					return new Belong(provinceList.get(i), "");
				}
				if (province.equals(provinceList.get(i)) && provinceList.get(i).equals("台湾")) {// 台湾
					return new Belong(provinceList.get(i), "");
				}
				if (province.startsWith(provinceList.get(i))) {
					proValue = provinceList.get(i);
					if (province.indexOf("市") == -1) {
						cityValue = province.substring(provinceList.get(i).length());
					} else {
						cityValue = province.substring(provinceList.get(i).length(), province.indexOf("市"));
					}
				}
				i++;
			}
			if (proValue != "" && cityValue != "") {
				return new Belong(proValue, cityValue);
			}
		}

		if (province.startsWith("北京") || province.startsWith("上海") || province.startsWith("重庆") || province.startsWith("天津")) {// 市即为省
			if (cityIndex != -1) {
				proValue = province.substring(0, cityIndex);
			} else {
				proValue = province;
			}
			return new Belong(proValue, "");
		} else {// 非中国城市
			return new Belong(province, "");
		}
	}

}
