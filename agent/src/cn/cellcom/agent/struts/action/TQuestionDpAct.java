package cn.cellcom.agent.struts.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import cn.cellcom.agent.hql.TQuestionMql;
import cn.cellcom.agent.pojo.TQuestion;
import cn.cellcom.agent.struts.form.TQuestionForm;
import cn.cellcom.jar.biz.AbstractBiz;
import cn.cellcom.jar.env.Env;
import cn.cellcom.jar.pagination.AbstractPaginationBiz;
import cn.cellcom.jar.pagination.IPaginationBiz;
import cn.cellcom.jar.util.BeanUtil;
import cn.cellcom.jar.util.LogUtil;
import cn.cellcom.jar.util.MyException;

public class TQuestionDpAct extends DispatchAction {

	private IPaginationBiz pbiz;
	private IPaginationBiz mpbiz;

	private AbstractBiz cbiz;

	private Log log = LogFactory.getLog(this.getClass());

	/**
	 * 查询记录
	 * 
	 * @param mapping
	 * @param fm
	 * @param req
	 * @param rep
	 * @return
	 */
	public ActionForward list(ActionMapping mapping, ActionForm form, HttpServletRequest req, HttpServletResponse rep) {
		form = (ActionForm) AbstractPaginationBiz.getPaginationActionForm(req, form);
		List list = null;
		try {
			list = pbiz.pagination(mapping, form, req, new TQuestionMql()).getDataResult();
			req.setAttribute(Env.DATA_NAME, list);
		} catch (MyException e) {
			return LogUtil.e(this.clazz, "操作出现错误", e, mapping, req);
		}
		// 存在读取数据库的配置

		return mapping.findForward("list");
	}

	public ActionForward list2(ActionMapping mapping, ActionForm form, HttpServletRequest req, HttpServletResponse rep) {
		form = (ActionForm) AbstractPaginationBiz.getPaginationActionForm(req, form);
		List list = null;
		try {
			mpbiz.setPageSize(6);
			list = mpbiz.pagination(mapping, form, req, new TQuestionMql()).getDataResult();
			req.setAttribute(Env.DATA_NAME, list);
			
			req.setAttribute("typelist", BeanUtil.startWith("TQuestion-type"));
		} catch (MyException e) {
			return LogUtil.e(this.clazz, "操作出现错误", e, mapping, req);
		}
		// 存在读取数据库的配置

		return mapping.findForward("list2");
	}
	/**
	 * 准备记录添加
	 * 
	 * @param mapping
	 * @param fm
	 * @param req
	 * @param rep
	 * @return
	 */
	public ActionForward add(ActionMapping mapping, ActionForm form, HttpServletRequest req, HttpServletResponse rep) {

		req.setAttribute("typelist", BeanUtil.startWith("TQuestion-type"));
		return mapping.findForward("add");
	}

	/**
	 * 执行记录添加
	 * 
	 * @param mapping
	 * @param fm
	 * @param req
	 * @param rep
	 * @return
	 */
	public ActionForward doAdd(ActionMapping mapping, ActionForm form, HttpServletRequest req, HttpServletResponse rep) {
		TQuestionForm fm = (TQuestionForm) form;
		Object obj = null;
		try {
			obj = cbiz.add(req, fm, null);
		} catch (MyException e) {
			LogUtil.e(this.clazz, "新增数据出现异常", e);
		}
		log.info("新增记录结果：" + obj);
		String message;
		if (obj == null || !(obj instanceof TQuestion)) {
			req.setAttribute(Env.DATA_NAME, "操作失败，" + obj);
			return mapping.findForward("error");
		} else {
			message = "记录添加成功";
		}

		add(mapping, fm, req, rep);
		// req.setAttribute("success", fm);
		req.setAttribute(Env.DATA_NAME, message);
		return mapping.findForward("add");
	}

	/**
	 * 准备修改记录
	 * 
	 * @param mapping
	 * @param fm
	 * @param req
	 * @param rep
	 * @return
	 */
	public ActionForward modify(ActionMapping mapping, ActionForm form, HttpServletRequest req, HttpServletResponse rep) {
		TQuestionForm fm = (TQuestionForm) form;
		try {
			java.lang.String key = fm.getId();
			TQuestion rf = (TQuestion) cbiz.getDao().myGet(TQuestion.class, key);
			if (rf == null) {
				req.setAttribute(Env.DATA_NAME, "读取数据错误");
				return mapping.findForward("error");
			}
			req.setAttribute(Env.DATA_NAME, rf);
			req.setAttribute("typelist", BeanUtil.startWith("TQuestion-type"));
		} catch (MyException e) {
			return LogUtil.e(this.clazz, "读取数据错误", e, mapping, req);
		}
		return mapping.findForward("modify");
	}

	/**
	 * 执行记录修改
	 * 
	 * @param mapping
	 * @param fm
	 * @param req
	 * @param rep
	 * @return
	 */
	public ActionForward doModify(ActionMapping mapping, ActionForm form, HttpServletRequest req, HttpServletResponse rep) {
		TQuestionForm fm = (TQuestionForm) form;
		Object obj = null;
		try {
			obj = cbiz.modify(req, fm, null);
		} catch (MyException e) {
			LogUtil.e(this.clazz, "修改数据出现异常", e);
		}
		log.info("修改记录结果：" + obj);
		if (obj == null || !(obj instanceof TQuestion)) {
			req.setAttribute(Env.DATA_NAME, "操作失败，" + obj);
			return mapping.findForward("error");
		}
		req.setAttribute(Env.DATA_NAME, "记录修改成功");
		return mapping.findForward("success");
	}

	/**
	 * 执行删除操作
	 * 
	 * @param mapping
	 * @param fm
	 * @param req
	 * @param rep
	 * @return
	 */
	public ActionForward doDelete(ActionMapping mapping, ActionForm form, HttpServletRequest req, HttpServletResponse rep) {
		TQuestionForm fm = (TQuestionForm) form;
		List res = null;
		try {
			res = cbiz.delete(req, fm, null);
		} catch (MyException e) {
			LogUtil.e(this.clazz, "删除数据出现异常", e);
		}
		if (res == null || !(res instanceof List)) {
			return LogUtil.e(this.clazz, "操作失败，执行删除记录失败", null, mapping, req);
		} else {
			req.setAttribute(Env.DATA_NAME, "操作完成，删除记录数量：" + res.size());
			return mapping.findForward("success");
		}
	}

	public void setCbiz(AbstractBiz cbiz) {
		this.cbiz = cbiz;
	}

	public void setPbiz(IPaginationBiz pbiz) {
		this.pbiz = pbiz;
	}

	public void setMpbiz(IPaginationBiz mpbiz) {
		this.mpbiz = mpbiz;
	}
}
