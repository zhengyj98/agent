package cn.cellcom.agent.struts.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import cn.cellcom.agent.hql.TTagHql;
import cn.cellcom.agent.struts.form.TTagForm;
import cn.cellcom.jar.biz.AbstractBiz;
import cn.cellcom.jar.env.Env;
import cn.cellcom.jar.pagination.AbstractPaginationBiz;
import cn.cellcom.jar.pagination.IPaginationBiz;
import cn.cellcom.jar.util.LogUtil;
import cn.cellcom.jar.util.MyException;

public class TTagDpAct extends DispatchAction {

	private IPaginationBiz pbiz;

	private AbstractBiz cbiz;

	private Log log = LogFactory.getLog(this.getClass());

	/**
	 * 查询记录
	 * 
	 * @param mapping
	 * @param fm
	 * @param req
	 * @param rep
	 * @return
	 */
	public ActionForward list(ActionMapping mapping, ActionForm form,
			HttpServletRequest req, HttpServletResponse rep) {
		form = (ActionForm) AbstractPaginationBiz.getPaginationActionForm(req,form);
		List list = null;
		try {
			log.info("查询的语句：TTagHql()");
			list = pbiz.pagination(mapping, form, req, new TTagHql()).getDataResult();

						req.setAttribute(Env.DATA_NAME, list);
		} catch (MyException e) {
			return LogUtil.e(this.clazz, "操作出现错误", e, mapping, req);
		}
						//存在读取数据库的配置
						
					return mapping.findForward("list");		
			}

		
		
		
		/**
	 * 执行删除操作
	 * @param mapping
	 * @param fm
	 * @param req
	 * @param rep
	 * @return
	 */
	public ActionForward doDelete(ActionMapping mapping, ActionForm form,
			HttpServletRequest req, HttpServletResponse rep) {
		TTagForm fm = (TTagForm) form;
		List res = null;
		try {
			res = cbiz.delete(req, fm, null);
		} catch (MyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(res == null || !(res instanceof List)) {
			return LogUtil.e(this.clazz, "操作失败，执行删除记录失败", null, mapping, req);
		} else {
			req.setAttribute(Env.DATA_NAME, "操作完成，删除记录数量：" + res.size());
			return mapping.findForward("success");
		}
	}
		
	
	
		
	
	public void setCbiz(AbstractBiz cbiz) {
		this.cbiz = cbiz;
	}
		
	public void setPbiz(IPaginationBiz pbiz) {
		this.pbiz = pbiz;
	}
}

