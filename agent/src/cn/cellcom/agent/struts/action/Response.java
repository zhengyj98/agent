package cn.cellcom.agent.struts.action;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * 向流输出字符串
 * 
 * @author zhengyj
 * 
 */
public class Response {

	private static Log log = LogFactory.getLog(Response.class);

	public static void err(HttpServletResponse rep, String msg) {
		try {
			rep.setCharacterEncoding("UTF-8");
			String out = "{\"code\":\"1\", \"msg\":\"" + msg + "\"}";
			rep.getOutputStream().write(out.getBytes());
		} catch (IOException e) {
			log.error(e);
		}
	}

	public static void succ(HttpServletResponse rep, String msg) {
		try {
			rep.setCharacterEncoding("UTF-8");
			String out = "{\"code\":\"0\", \"msg\":\"" + msg + "\"}";
			rep.getOutputStream().write(out.getBytes());
		} catch (IOException e) {
			log.error(e);
		}
	}

	public static void succ(HttpServletResponse rep, List mlist) {
		try {
			rep.setCharacterEncoding("UTF-8");
			rep.getOutputStream().write(JSONArray.fromObject(mlist).toString().getBytes());
		} catch (IOException e) {
			log.error(e);
		}
	}

	public static void succ(HttpServletResponse rep, Collection collection) {
		try {
			rep.setCharacterEncoding("UTF-8");
			rep.getOutputStream().write(JSONArray.fromObject(collection).toString().getBytes());
		} catch (IOException e) {
			log.error(e);
		}
	}

	public static void succObject(HttpServletResponse rep, Object obj) {
		try {
			rep.setCharacterEncoding("UTF-8");
			rep.getOutputStream().write(JSONObject.fromObject(obj).toString().getBytes());
		} catch (IOException e) {
			log.error(e);
		}
	}

	public static void succ(HttpServletResponse rep, byte[] arr) {
		try {
			rep.getOutputStream().write(arr);
		} catch (IOException e) {
			log.error(e);
		}
	}
}
