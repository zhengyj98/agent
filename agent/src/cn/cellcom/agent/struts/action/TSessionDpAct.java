package cn.cellcom.agent.struts.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

import cn.cellcom.agent.common.AgentEnv;
import cn.cellcom.agent.hql.TSessionHql;
import cn.cellcom.agent.pojo.TCrm;
import cn.cellcom.agent.pojo.TGroup;
import cn.cellcom.agent.pojo.TSession;
import cn.cellcom.agent.struts.form.TSessionForm;
import cn.cellcom.jar.biz.AbstractBiz;
import cn.cellcom.jar.dao.IBaseDao;
import cn.cellcom.jar.env.Env;
import cn.cellcom.jar.file.FileLoad;
import cn.cellcom.jar.file.FileProcessor;
import cn.cellcom.jar.file.IFileLoad;
import cn.cellcom.jar.file.IFileProcessor;
import cn.cellcom.jar.pagination.AbstractPaginationBiz;
import cn.cellcom.jar.pagination.IPaginationBiz;
import cn.cellcom.jar.port.ExportTemplate;
import cn.cellcom.jar.port.IDataPort;
import cn.cellcom.jar.port.JxlPort;
import cn.cellcom.jar.util.ABean;
import cn.cellcom.jar.util.BeanUtil;
import cn.cellcom.jar.util.CU;
import cn.cellcom.jar.util.DT;
import cn.cellcom.jar.util.LogUtil;
import cn.cellcom.jar.util.MyException;

public class TSessionDpAct extends DispatchAction {

	private IPaginationBiz pbiz;

	private AbstractBiz cbiz;

	private Log log = LogFactory.getLog(this.getClass());

	/**
	 * 查询记录
	 * 
	 * @param mapping
	 * @param fm
	 * @param req
	 * @param rep
	 * @return
	 */
	public ActionForward list(ActionMapping mapping, ActionForm form, HttpServletRequest req, HttpServletResponse rep) {
		form = (ActionForm) AbstractPaginationBiz.getPaginationActionForm(req, form);
		List list = null;
		try {
			log.info("查询的语句：TSessionHql()");
			list = pbiz.pagination(mapping, form, req, new TSessionHql()).getDataResult();

			req.setAttribute(Env.DATA_NAME, list);
		} catch (MyException e) {
			return LogUtil.e(this.clazz, "操作出现错误", e, mapping, req);
		}
		req.setAttribute("endTypelist", BeanUtil.startWith("TSession-endType"));
		req.setAttribute("steplist", BeanUtil.startWith("TSession-step"));

		return mapping.findForward("list");
	}

	/**
	 * 查询记录详细信息
	 * 
	 * @param mapping
	 * @param fm
	 * @param req
	 * @param rep
	 * @return
	 */
	public ActionForward detail(ActionMapping mapping, ActionForm form, HttpServletRequest req, HttpServletResponse rep) {

		TSessionForm fm = (TSessionForm) form;
		IBaseDao dao = cbiz.getDao();
		try {
			String hql = "from TSession as s, TCrm as c, TGroup g where s.crm=c.id and s.requestGroup=g.id and s.id=?";
			Object pojo = dao.myGet(hql, new String[] { fm.getId() });
			if (pojo == null) {
				req.setAttribute(Env.DATA_NAME, "查看的信息不存，有疑问请联系管理员");
				return mapping.findForward("error");
			}
			req.setAttribute(Env.DATA_NAME, pojo);
		} catch (MyException e) {
			return LogUtil.e(this.clazz, "操作失败，未能读取相关信息的详细资料", e, mapping, req);
		}
		req.setAttribute("endTypelist", BeanUtil.startWith("TSession-endType"));
		req.setAttribute("steplist", BeanUtil.startWith("TSession-step"));

		return mapping.findForward("detail");
	}

	/**
	 * 导出数据
	 * 
	 * @param mapping
	 * @param form
	 * @param req
	 * @param rep
	 * @return
	 */
	public ActionForward doExport(ActionMapping mapping, ActionForm form, HttpServletRequest req, HttpServletResponse rep) {

		final List<ABean> endTypeList = BeanUtil.startWith("TSession-endType");
		final List<ABean> stepList = BeanUtil.startWith("TSession-step");
		try {
			TSessionHql hql0 = new TSessionHql();
			List list = this.cbiz.getDao().myList(hql0.getHql(req, form), hql0.getParaValue(req, form));
			if (list == null || list.size() < 1) {
				req.setAttribute(Env.DATA_NAME, "本次查询没有数据，所以不做导出");
				return mapping.findForward("success");
			}

			ExportTemplate template = new ExportTemplate() {
				private static final long serialVersionUID = 1L;

				@Override
				public void setRow(DataRow row, Object obj) {
					Object[] arr = (Object[]) obj;
					TSession pojo = (TSession) arr[0];
					TCrm crm = (TCrm) arr[1];
					TGroup group = (TGroup) arr[2];
					row.add(CU.valueOf(pojo.getId()));
					row.add(CU.valueOf(pojo.getChannel()));
					row.add(CU.valueOf(crm.getName()));
					row.add(CU.valueOf(crm.getPhone()));
					row.add(CU.valueOf(DT.getYMDHMS(pojo.getRequestTime())));
					row.add(CU.valueOf((DT.getYMDHMS(pojo.getRobotTime()))));
					row.add(CU.valueOf((DT.getYMDHMS(pojo.getQueueTime()))));
					row.add(CU.valueOf((DT.getYMDHMS(pojo.getAgentTime()))));
					row.add(CU.valueOf((DT.getYMDHMS(pojo.getEndTime()))));
					row.add(CU.valueOf(pojo.getAgent()));
					row.add(CU.valueOf(pojo.getAgentName()));
					row.add(CU.valueOf(String.valueOf(BeanUtil.getObjectFromValue(pojo.getEndType(), endTypeList))));
					row.add(CU.valueOf(DT.getYMDHMS(pojo.getFirstAgentResponseTime())));
					row.add(CU.valueOf(group.getName()));
					row.add(CU.valueOf(pojo.getMessageCount()));
					row.add(CU.valueOf(pojo.getMessageCountWhenAgent()));
					row.add(CU.valueOf(pojo.getAgentMessageCount()));
					row.add(CU.valueOf(pojo.getSystemMessageCount()));
					row.add(CU.valueOf(pojo.getSatisfyScore()));
					row.add(CU.valueOf(pojo.getSatisfyText()));
					row.add(CU.valueOf(String.valueOf(BeanUtil.getObjectFromValue(pojo.getStep(), stepList))));
					row.add(CU.valueOf(pojo.getIp()));
					row.add(CU.valueOf(pojo.getProvince()));
					row.add(CU.valueOf(pojo.getCity()));

				}
			};
			String root3path3file = AgentEnv.EXPORT_PATH + CU.getSerial(".xls");
			template.setRoot3path3file(root3path3file);
			template.setContentTitle(new String[] { "id", "渠道", "访客", "访客电话", "请求时间", "自助聊天时间", "排队时间", "坐席接入时间", "访客结束时间", "坐席账号", "坐席昵称", "结束原因",
					"首次响应时间", "请求组", "访客消息数", "与坐席消息数", "坐席消息数", "系统消息数", "满意度", "满意度评论", "最后步骤", "ip", "省份", "城市" });

			IDataPort port = new JxlPort();
			template.setDataList(list);
			port.export(template);

			IFileLoad fl = new FileLoad();
			fl.download(req, rep, this.servlet.getServletConfig(), root3path3file);
			IFileProcessor fp = new FileProcessor();
			fp.delete(root3path3file);
		} catch (MyException e) {
			return LogUtil.e(this.clazz, "操作出现错误", e, mapping, req);
		}

		LogUtil.e(this.clazz, "数据已经成功导出", null, mapping, req);
		return null;
	}

	public void setCbiz(AbstractBiz cbiz) {
		this.cbiz = cbiz;
	}

	public void setPbiz(IPaginationBiz pbiz) {
		this.pbiz = pbiz;
	}
}
