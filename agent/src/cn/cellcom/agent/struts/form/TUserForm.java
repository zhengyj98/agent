package cn.cellcom.agent.struts.form;

import org.apache.struts.action.ActionForm;

public class TUserForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// ids字段为默认增加，是为了配合删除功能的需要
	private String[] ids;

	public String[] getIds() {
		return this.ids;
	}

	public void setIds(String[] ids) {
		this.ids = ids;
	}

	private java.lang.String app;

	private java.lang.String status;

	private java.lang.String no;

	private java.lang.String org;

	private java.lang.String type;

	private java.lang.String password;

	private java.lang.String id;

	private java.lang.String username;

	// 日期类型，结合strutsform的需要增加了两个字段
	private java.lang.String insertTimeStart;

	private java.lang.String insertTimeEnd;

	private java.lang.String insertTime;

	private java.lang.String phone;

	private java.lang.String nickName;

	private java.lang.String email;

	private java.lang.String name;
	
	private java.lang.Integer level;
	
	public java.lang.Integer getLevel() {
		return level;
	}

	public void setLevel(java.lang.Integer level) {
		this.level = level;
	}

	public java.lang.String getApp() {
		return this.app;
	}

	public void setApp(java.lang.String app) {
		this.app = app;
	}

	public java.lang.String getStatus() {
		return this.status;
	}

	public void setStatus(java.lang.String status) {
		this.status = status;
	}

	public java.lang.String getNo() {
		return this.no;
	}

	public void setNo(java.lang.String no) {
		this.no = no;
	}

	public java.lang.String getOrg() {
		return this.org;
	}

	public void setOrg(java.lang.String org) {
		this.org = org;
	}

	public java.lang.String getType() {
		return this.type;
	}

	public void setType(java.lang.String type) {
		this.type = type;
	}

	public java.lang.String getPassword() {
		return this.password;
	}

	public void setPassword(java.lang.String password) {
		this.password = password;
	}

	public java.lang.String getId() {
		return this.id;
	}

	public void setId(java.lang.String id) {
		this.id = id;
	}

	public java.lang.String getUsername() {
		return this.username;
	}

	public void setUsername(java.lang.String username) {
		this.username = username;
	}

	public java.lang.String getInsertTimeStart() {
		return this.insertTimeStart;
	}

	public void setInsertTimeStart(java.lang.String insertTimeStart) {
		this.insertTimeStart = insertTimeStart;
	}

	public java.lang.String getInsertTimeEnd() {
		return this.insertTimeEnd;
	}

	public void setInsertTimeEnd(java.lang.String insertTimeEnd) {
		this.insertTimeEnd = insertTimeEnd;
	}

	public java.lang.String getInsertTime() {
		return this.insertTime;
	}

	public void setInsertTime(java.lang.String insertTime) {
		this.insertTime = insertTime;
	}

	public java.lang.String getPhone() {
		return this.phone;
	}

	public void setPhone(java.lang.String phone) {
		this.phone = phone;
	}

	public java.lang.String getNickName() {
		return this.nickName;
	}

	public void setNickName(java.lang.String nickName) {
		this.nickName = nickName;
	}

	public java.lang.String getEmail() {
		return this.email;
	}

	public void setEmail(java.lang.String email) {
		this.email = email;
	}

	public java.lang.String getName() {
		return this.name;
	}

	public void setName(java.lang.String name) {
		this.name = name;
	}

}
