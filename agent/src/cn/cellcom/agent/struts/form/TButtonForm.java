package cn.cellcom.agent.struts.form;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

import cn.cellcom.agent.common.AgentConstant;

public class TButtonForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// ids字段为默认增加，是为了配合删除功能的需要
	private String[] ids;

	public String[] getIds() {
		return this.ids;
	}

	public void setIds(String[] ids) {
		this.ids = ids;
	}

	private java.lang.String id;

	private java.lang.String pid;

	private java.lang.String org;

	private java.lang.String title;

	private java.lang.String memo;

	private java.lang.String name;

	private java.lang.String img;

	private FormFile imgFile;

	private java.lang.Integer seq;

	private java.lang.String href;

	private String subject = AgentConstant.DEFAULT_ORG;

	private int level = 1;
	
	private boolean ajax;
	
	public boolean isAjax() {
		return ajax;
	}

	public void setAjax(boolean ajax) {
		this.ajax = ajax;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public FormFile getImgFile() {
		return imgFile;
	}

	public void setImgFile(FormFile imgFile) {
		this.imgFile = imgFile;
	}

	public java.lang.String getId() {
		return this.id;
	}

	public void setId(java.lang.String id) {
		this.id = id;
	}

	public java.lang.String getPid() {
		return this.pid;
	}

	public void setPid(java.lang.String pid) {
		this.pid = pid;
	}

	public java.lang.String getOrg() {
		return this.org;
	}

	public void setOrg(java.lang.String org) {
		this.org = org;
	}

	public java.lang.String getTitle() {
		return this.title;
	}

	public void setTitle(java.lang.String title) {
		this.title = title;
	}

	public java.lang.String getMemo() {
		return this.memo;
	}

	public void setMemo(java.lang.String memo) {
		this.memo = memo;
	}

	public java.lang.String getName() {
		return this.name;
	}

	public void setName(java.lang.String name) {
		this.name = name;
	}

	public java.lang.String getImg() {
		return this.img;
	}

	public void setImg(java.lang.String img) {
		this.img = img;
	}

	public java.lang.Integer getSeq() {
		return this.seq;
	}

	public void setSeq(java.lang.Integer seq) {
		this.seq = seq;
	}

	public java.lang.String getHref() {
		return this.href;
	}

	public void setHref(java.lang.String href) {
		this.href = href;
	}

}
