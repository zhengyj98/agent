package cn.cellcom.agent.struts.form;

import org.apache.struts.action.ActionForm;

public class TGroupForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// ids字段为默认增加，是为了配合删除功能的需要
	private String[] ids;

	public String[] getIds() {
		return this.ids;
	}

	public void setIds(String[] ids) {
		this.ids = ids;
	}

	private java.lang.String id;

	private java.lang.String app;

	private java.lang.String no;

	private java.lang.String name;

	private java.lang.String org;

	private java.lang.String memo;

	public java.lang.String getId() {
		return this.id;
	}

	public void setId(java.lang.String id) {
		this.id = id;
	}

	public java.lang.String getApp() {
		return this.app;
	}

	public void setApp(java.lang.String app) {
		this.app = app;
	}

	public java.lang.String getNo() {
		return this.no;
	}

	public void setNo(java.lang.String no) {
		this.no = no;
	}

	public java.lang.String getName() {
		return this.name;
	}

	public void setName(java.lang.String name) {
		this.name = name;
	}

	public java.lang.String getOrg() {
		return this.org;
	}

	public void setOrg(java.lang.String org) {
		this.org = org;
	}

	public java.lang.String getMemo() {
		return this.memo;
	}

	public void setMemo(java.lang.String memo) {
		this.memo = memo;
	}

}
