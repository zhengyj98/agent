package cn.cellcom.agent.struts.form;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;

import cn.cellcom.agent.online.message.MessageConstant;

public class VisitorForm extends ActionForm {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7496248731734939541L;

	/**
	 * 普通id
	 */
	private String id;

	/**
	 * 
	 */
	private String org;

	/**
	 * 
	 */
	private String channel;

	/**
	 * 商户id
	 */
	private String pid = null;

	/**
	 * 对接方的crmid
	 */
	private String tid;

	/**
	 * CRM id
	 */
	private String cid;

	/**
	 * 会话id
	 */
	private String sid;

	/**
	 * 访客操作
	 */
	private String event;

	/**
	 * 普通的参数请求，不需要每个动作都定义一个对应的变量名
	 */
	private String value;

	/**
	 * 普通的参数请求，不需要每个动作都定义一个对应的变量名
	 */
	private String value2;

	/**
	 * 消息的媒体类型
	 */
	private String multiType = MessageConstant.MULIT_TYPE.TEXT.name();

	/**
	 * 分页记录的开始，和每页的数量
	 */
	private int start;

	private int count;

	/**
	 * 第三方传递tid的话必须同时带上这个参数做校验 MD5(tid + accesskey)
	 */
	private String ttoken;

	/**
	 * 请求的时间戳，用于加密
	 */
	private String timestamp;

	/**
	 * 普通的对象参数
	 */
	private Object object;
	
	/**
	 * 来源
	 */
	private String api;
	
	/**
	 * 每次访问的序号会变
	 */
	private Long sequence;
	
	public Long getSequence() {
		return sequence;
	}

	public void setSequence(Long sequence) {
		this.sequence = sequence;
	}

	public String getApi() {
		return api;
	}

	public void setApi(String api) {
		this.api = api;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getTtoken() {
		return ttoken;
	}

	public void setTtoken(String ttoken) {
		this.ttoken = ttoken;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public void setMultiType(String multiType) {
		this.multiType = multiType;
	}

	public String getMultiType() {
		return multiType;
	}

	public String getValue2() {
		return value2;
	}

	public void setValue2(String value2) {
		this.value2 = value2;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getPid() {
		if (StringUtils.isBlank(pid)) {
			return org;
		}
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrg() {
		return org;
	}

	public void setOrg(String org) {
		this.org = org;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getTid() {
		return tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}

	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public String toString() {
		return "VisitorForm [org=" + org + ", channel=" + channel + ", multiType=" + multiType + ", cid=" + tid + ", id=" + id
				+ ", crmId=" + cid + ", event=" + event + ", tcrm=" + tcrm + ",value=" + value + "]";
	}

	private String tcrm;

	public String getTcrm() {
		return tcrm;
	}

	public void setTcrm(String tcrm) {
		this.tcrm = tcrm;
	}
}
