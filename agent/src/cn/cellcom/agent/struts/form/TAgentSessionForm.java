package cn.cellcom.agent.struts.form;

import org.apache.struts.action.ActionForm;

public class TAgentSessionForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//ids字段为默认增加，是为了配合删除功能的需要
	private String[] ids;
	
	public String[] getIds() {
		return this.ids;
	}
	
	public void setIds(String[] ids) {
		this.ids = ids;
	}

		
			
				
			private java.lang.String org;	
					
				
			private java.lang.String agent;	
					
				
			private java.lang.String id;	
					
				
			private java.lang.String session;	
					
				
			private java.lang.Long joinTime;	
					
				
			private java.lang.String groupid;	
					
				
			private java.lang.String agentNickname;	
					
				
			private java.lang.String joinType;	
					
				
			private java.lang.String channel;	
					
				
			private java.lang.Long firstResponseTime;	
					
				
			private java.lang.String pid;	
					
				
			private java.lang.String endType;	
					
				
			private java.lang.Long endTime;	
					
				
			private java.lang.Integer messageCount;	
				
						public java.lang.String getOrg() {
				return this.org;
			}
			
			public void setOrg(java.lang.String org) {
				this.org = org;
			}	
								public java.lang.String getAgent() {
				return this.agent;
			}
			
			public void setAgent(java.lang.String agent) {
				this.agent = agent;
			}	
								public java.lang.String getId() {
				return this.id;
			}
			
			public void setId(java.lang.String id) {
				this.id = id;
			}	
								public java.lang.String getSession() {
				return this.session;
			}
			
			public void setSession(java.lang.String session) {
				this.session = session;
			}	
								public java.lang.Long getJoinTime() {
				return this.joinTime;
			}
			
			public void setJoinTime(java.lang.Long joinTime) {
				this.joinTime = joinTime;
			}	
								public java.lang.String getGroupid() {
				return this.groupid;
			}
			
			public void setGroupid(java.lang.String groupid) {
				this.groupid = groupid;
			}	
								public java.lang.String getAgentNickname() {
				return this.agentNickname;
			}
			
			public void setAgentNickname(java.lang.String agentNickname) {
				this.agentNickname = agentNickname;
			}	
								public java.lang.String getJoinType() {
				return this.joinType;
			}
			
			public void setJoinType(java.lang.String joinType) {
				this.joinType = joinType;
			}	
								public java.lang.String getChannel() {
				return this.channel;
			}
			
			public void setChannel(java.lang.String channel) {
				this.channel = channel;
			}	
								public java.lang.Long getFirstResponseTime() {
				return this.firstResponseTime;
			}
			
			public void setFirstResponseTime(java.lang.Long firstResponseTime) {
				this.firstResponseTime = firstResponseTime;
			}	
								public java.lang.String getPid() {
				return this.pid;
			}
			
			public void setPid(java.lang.String pid) {
				this.pid = pid;
			}	
								public java.lang.String getEndType() {
				return this.endType;
			}
			
			public void setEndType(java.lang.String endType) {
				this.endType = endType;
			}	
								public java.lang.Long getEndTime() {
				return this.endTime;
			}
			
			public void setEndTime(java.lang.Long endTime) {
				this.endTime = endTime;
			}	
								public java.lang.Integer getMessageCount() {
				return this.messageCount;
			}
			
			public void setMessageCount(java.lang.Integer messageCount) {
				this.messageCount = messageCount;
			}	
				
}

