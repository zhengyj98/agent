package cn.cellcom.agent.struts.form;

import org.apache.struts.action.ActionForm;

public class TCrmForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// ids字段为默认增加，是为了配合删除功能的需要
	private String[] ids;

	public String[] getIds() {
		return this.ids;
	}

	public void setIds(String[] ids) {
		this.ids = ids;
	}

	private java.lang.String org;

	private java.lang.String id;

	private java.lang.String userId;

	private java.lang.String sex;

	private java.lang.String phone;

	private java.lang.String memo;

	private java.lang.String extend10;

	private java.lang.Integer type;

	private java.lang.String city;

	private java.lang.Integer level;

	private java.lang.String name;

	private java.lang.String province;

	private java.lang.String qq;

	private java.lang.Integer status;

	private java.lang.String extend1;

	private java.lang.String extend3;

	private java.lang.String extend2;

	private java.lang.String extend5;

	private java.lang.String extend4;

	private java.lang.String extend7;

	private java.lang.String tid;

	private java.lang.String extend6;

	private java.lang.String extend9;

	private java.lang.String extend8;

	private java.lang.String email;

	private java.lang.String company;

	public java.lang.String getOrg() {
		return this.org;
	}

	public void setOrg(java.lang.String org) {
		this.org = org;
	}

	public java.lang.String getId() {
		return this.id;
	}

	public void setId(java.lang.String id) {
		this.id = id;
	}

	public java.lang.String getUserId() {
		return this.userId;
	}

	public void setUserId(java.lang.String userId) {
		this.userId = userId;
	}

	public java.lang.String getSex() {
		return this.sex;
	}

	public void setSex(java.lang.String sex) {
		this.sex = sex;
	}

	public java.lang.String getPhone() {
		return this.phone;
	}

	public void setPhone(java.lang.String phone) {
		this.phone = phone;
	}

	public java.lang.String getMemo() {
		return this.memo;
	}

	public void setMemo(java.lang.String memo) {
		this.memo = memo;
	}

	public java.lang.String getExtend10() {
		return this.extend10;
	}

	public void setExtend10(java.lang.String extend10) {
		this.extend10 = extend10;
	}

	public java.lang.Integer getType() {
		return this.type;
	}

	public void setType(java.lang.Integer type) {
		this.type = type;
	}

	public java.lang.String getCity() {
		return this.city;
	}

	public void setCity(java.lang.String city) {
		this.city = city;
	}

	public java.lang.Integer getLevel() {
		return this.level;
	}

	public void setLevel(java.lang.Integer level) {
		this.level = level;
	}

	public java.lang.String getName() {
		return this.name;
	}

	public void setName(java.lang.String name) {
		this.name = name;
	}

	public java.lang.String getProvince() {
		return this.province;
	}

	public void setProvince(java.lang.String province) {
		this.province = province;
	}

	public java.lang.String getQq() {
		return this.qq;
	}

	public void setQq(java.lang.String qq) {
		this.qq = qq;
	}

	public java.lang.Integer getStatus() {
		return this.status;
	}

	public void setStatus(java.lang.Integer status) {
		this.status = status;
	}

	public java.lang.String getExtend1() {
		return this.extend1;
	}

	public void setExtend1(java.lang.String extend1) {
		this.extend1 = extend1;
	}

	public java.lang.String getExtend3() {
		return this.extend3;
	}

	public void setExtend3(java.lang.String extend3) {
		this.extend3 = extend3;
	}

	public java.lang.String getExtend2() {
		return this.extend2;
	}

	public void setExtend2(java.lang.String extend2) {
		this.extend2 = extend2;
	}

	public java.lang.String getExtend5() {
		return this.extend5;
	}

	public void setExtend5(java.lang.String extend5) {
		this.extend5 = extend5;
	}

	public java.lang.String getExtend4() {
		return this.extend4;
	}

	public void setExtend4(java.lang.String extend4) {
		this.extend4 = extend4;
	}

	public java.lang.String getExtend7() {
		return this.extend7;
	}

	public void setExtend7(java.lang.String extend7) {
		this.extend7 = extend7;
	}

	public java.lang.String getTid() {
		return this.tid;
	}

	public void setTid(java.lang.String tid) {
		this.tid = tid;
	}

	public java.lang.String getExtend6() {
		return this.extend6;
	}

	public void setExtend6(java.lang.String extend6) {
		this.extend6 = extend6;
	}

	public java.lang.String getExtend9() {
		return this.extend9;
	}

	public void setExtend9(java.lang.String extend9) {
		this.extend9 = extend9;
	}

	public java.lang.String getExtend8() {
		return this.extend8;
	}

	public void setExtend8(java.lang.String extend8) {
		this.extend8 = extend8;
	}

	public java.lang.String getEmail() {
		return this.email;
	}

	public void setEmail(java.lang.String email) {
		this.email = email;
	}

	public java.lang.String getCompany() {
		return this.company;
	}

	public void setCompany(java.lang.String company) {
		this.company = company;
	}

}
