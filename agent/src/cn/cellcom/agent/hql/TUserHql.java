package cn.cellcom.agent.hql;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;

import cn.cellcom.agent.pojo.TUser;
import cn.cellcom.jar.dao.IHql;
import cn.cellcom.jar.logon.LogonSession;
import cn.cellcom.jar.util.AU;
import cn.cellcom.jar.util.CU;
import cn.cellcom.jar.util.MyException;
import cn.cellcom.jar.util.form.FormUtil;
import cn.cellcom.jar.util.form.FormUtil.SpecialField;
import cn.cellcom.jar.util.form.FormUtil.SpecialValue;

public class TUserHql implements IHql {

	private Log log = LogFactory.getLog(this.getClass());

	public String getHql(HttpServletRequest req, Object fm) {
		FormUtil fu = new FormUtil();
		try {
			if ("select2".equals(req.getParameter("from"))) {
				LogonSession ls = new LogonSession(req);
				TUser user = (TUser) ls.getLogonObject();

				String term = CU.valueOf(req.getParameter("term"));
				StringBuffer sb = new StringBuffer("from TUser where pid='").append(user.getPid()).append("' and (username like '%").append(term)
						.append("%' or name like '%").append(term).append("%' or phone like '%").append(term).append("%')");

				String group = req.getParameter("group");
				if (StringUtils.isNotBlank(group)) {// 某组下
					sb = new StringBuffer("select u from TUser as u, TGroupUser as gu where u.username=gu.user and gu.groupId='").append(group).append(
							"' and u.pid='").append(user.getPid()).append("' and (u.username like '%").append(term).append("%' or u.name like '%")
							.append(term).append("%' or u.phone like '%").append(term).append("%')");
				}
				return sb.toString();
			}

			StringBuffer sb = new StringBuffer("from TUser where 1=1 ");
			SpecialField sf = null;
			sf = fu.newSpecialField("no", "no", "like");
			sf.add("username", "username", "like");
			sf.add("nickName", "nickName", "like");
			sf.add("name", "name", "like");
			sf.add("phone", "phone", "like");

			sb.append(fu.fieldToString(fu.getInput((ActionForm) fm, new String[] { "ids" }), null, null, sf));
			return sb.append(" and pid=?").toString();
		} catch (MyException e) {
			log.error("分析获取hql语句失败", e.getException());
			return null;
		}
	}

	public Object[] getParaValue(HttpServletRequest req, Object fm) {
		if ("select2".equals(req.getParameter("from"))) {
			return new Object[] {};
		}
		LogonSession ls = new LogonSession(req);
		FormUtil fu = new FormUtil();
		try {
			SpecialValue sv = null;
			sv = fu.newSpecialValue("no", "%", "%", null);
			sv.add("username", "%", "%", null);
			sv.add("nickName", "%", "%", null);
			sv.add("name", "%", "%", null);
			sv.add("phone", "%", "%", null);

			Object[] obj = fu.valueToList(fu.getInput((ActionForm) fm, new String[] { "ids" }), sv).toArray();
			TUser user = (TUser) ls.getLogonObject();
			return AU.append(obj, new String[] { user.getPid() });
		} catch (MyException e) {
			log.error("分析获取hql语句失败", e.getException());
			return null;
		}
	}
}
