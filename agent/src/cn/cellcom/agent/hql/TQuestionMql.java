package cn.cellcom.agent.hql;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;

import cn.cellcom.agent.pojo.TQuestion;
import cn.cellcom.agent.pojo.TUser;
import cn.cellcom.agent.struts.form.TQuestionForm;
import cn.cellcom.jar.dao.IHql;
import cn.cellcom.jar.logon.LogonSession;
import cn.cellcom.jar.util.AU;

public class TQuestionMql implements IHql {

	public String getHql(HttpServletRequest req, Object fm) {
		StringBuffer sb = new StringBuffer("from " + TQuestion.class + " where (pid = ?)");// and (no in ?)
		TQuestionForm qf = (TQuestionForm) fm;
		if (StringUtils.isNotBlank(qf.getType())) {
			sb.append(" and (type = ?)");
		}
		if (StringUtils.isNotBlank(qf.getNo())) {
			sb.append(" and (no = ?)");
		}
		if (StringUtils.isNotBlank(qf.getQuestion())) {
			sb.append(" and (question like ?)");
		}
		if (StringUtils.isNotBlank(qf.getAnswer())) {
			sb.append(" and (answer like ?)");
		}
		sb.append(" order by times desc");
		return sb.toString();
	}

	public Object[] getParaValue(HttpServletRequest req, Object fm) {
		LogonSession ls = new LogonSession(req);
		TUser user = (TUser) ls.getLogonObject();
		
		Object[] obj = new Object[] {};
		obj = AU.append(obj, user.getPid());
		//Object a = new Object[]{"3", "5"};
		//obj = AU.append(obj, a);
		
		TQuestionForm qf = (TQuestionForm) fm;
		if (StringUtils.isNotBlank(qf.getType())) {
			obj = AU.append(obj, qf.getType());
		}
		if (StringUtils.isNotBlank(qf.getNo())) {
			obj = AU.append(obj, qf.getNo());
		}
		if (StringUtils.isNotBlank(qf.getQuestion())) {
			obj = AU.append(obj, qf.getQuestion());
		}
		if (StringUtils.isNotBlank(qf.getAnswer())) {
			obj = AU.append(obj, qf.getAnswer());
		}
		return obj;
	}
}
