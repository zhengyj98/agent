package cn.cellcom.agent.hql;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;

import cn.cellcom.agent.pojo.TUser;
import cn.cellcom.agent.struts.form.TExtendForm;
import cn.cellcom.jar.dao.IHql;
import cn.cellcom.jar.logon.LogonSession;
import cn.cellcom.jar.util.AU;
import cn.cellcom.jar.util.MyException;
import cn.cellcom.jar.util.form.FormUtil;
import cn.cellcom.jar.util.form.FormUtil.SpecialField;
import cn.cellcom.jar.util.form.FormUtil.SpecialValue;

public class TExtendHql implements IHql {

	private Log log = LogFactory.getLog(this.getClass());

	public String getHql(HttpServletRequest req, Object fm) {
		FormUtil fu = new FormUtil();
		try {
			StringBuffer sb = new StringBuffer("from TExtend where 1=1 ");

			SpecialField sf = null;

			sb.append(fu.fieldToString(fu.getInput((ActionForm) fm, new String[] { "ids" }), null, null, sf));
			return sb.append(" and pid = ?").toString();
		} catch (MyException e) {
			log.error("分析获取hql语句失败", e.getException());
			return null;
		}
	}

	public Object[] getParaValue(HttpServletRequest req, Object fm) {
		LogonSession ls = new LogonSession(req);
		FormUtil fu = new FormUtil();
		TExtendForm form = (TExtendForm) fm;
		try {
			SpecialValue sv = null;

			Object[] obj = new Object[] {};
			obj = AU.append(obj, fu.valueToList(fu.getInput((ActionForm) fm, new String[] { "ids" }), sv).toArray());
			TUser user = (TUser) ls.getLogonObject();
			return AU.append(obj, new String[] { user.getPid() });
		} catch (MyException e) {
			log.error("分析获取hql语句失败", e.getException());
			return null;
		}
	}
}
