package cn.cellcom.agent.hql;

import javax.servlet.http.HttpServletRequest;

import cn.cellcom.agent.common.AgentConstant;
import cn.cellcom.agent.pojo.TUser;
import cn.cellcom.agent.struts.form.ReporterForm;
import cn.cellcom.jar.dao.IHql;
import cn.cellcom.jar.logon.LogonSession;
import cn.cellcom.jar.util.DT;

public class RptAgentsHql implements IHql {

	@Override
	public String getHql(HttpServletRequest req, Object fm) {
		ReporterForm rf = (ReporterForm) fm;
		LogonSession ls = new LogonSession(req);
		TUser user = (TUser) ls.getLogonObject();
		long startTime = 0;
		long endTime = 0;
		if (rf.getDuration() == AgentConstant.DURATION.TODAY.value()) {// 今天
			startTime = DT.getToday().getTime();
		} else if (rf.getDuration() == AgentConstant.DURATION.YESTERDAY.value()) {// 昨天
			startTime = DT.getYesterday().getTime();
			endTime = DT.getToday().getTime();
		} else if (rf.getDuration() == AgentConstant.DURATION.WEEK.value()) {// 近一周
			startTime = DT.getNearlyWeek().getTime();
			endTime = DT.getNextDay(DT.getToday(), 1).getTime();
		} else if (rf.getDuration() == AgentConstant.DURATION.MONTH.value()) {//
			startTime = DT.getNearlyMonth().getTime();
			endTime = DT.getNextDay(DT.getToday(), 1).getTime();
		} else {
			startTime = DT.getToday().getTime();
		}
		String hql = "SELECT agent, COUNT(1) as sessionCount ,SUM(message_count) as messageCount, SUM(first_response_time-join_time)/1000 as sumFirstResponseTime, "
				+ "MAX(first_response_time-join_time)/1000 as maxFirstResponseTime, SUM(end_time-join_time)/1000 as sumSessionTime, MAX(end_time-join_time)/1000 as maxSessionTime"
				+ " FROM t_agent_session where join_time >= '" + startTime + "' and end_time is not null";
		if (endTime > 0) {
			hql = hql + " and end_time < '" + endTime + "'";
		}
		hql = hql + " and pid='" + user.getPid() + "' group by agent order by agent";
		return hql;
	}

	@Override
	public Object[] getParaValue(HttpServletRequest req, Object fm) {
		return new Object[] {};
	}

}
