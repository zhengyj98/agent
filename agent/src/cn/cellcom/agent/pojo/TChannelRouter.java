package cn.cellcom.agent.pojo;

/**
 * TChannelRouter entity.
 * 
 * @author MyEclipse Persistence Tools
 */

public class TChannelRouter implements java.io.Serializable {

	// Fields

	private String id;
	private String org;
	private String pid;
	private String status;
	private String channel;
	private String routeGroup;
	private String routeAgent;
	private Long createTime;
	private String inSupportGroup;

	// Constructors

	/** default constructor */
	public TChannelRouter() {
	}

	/** minimal constructor */
	public TChannelRouter(String id, String org, String pid, String status, String channel, String routeGroup, Long createTime) {
		this.id = id;
		this.org = org;
		this.pid = pid;
		this.status = status;
		this.channel = channel;
		this.routeGroup = routeGroup;
		this.createTime = createTime;
	}

	/** full constructor */
	public TChannelRouter(String id, String org, String pid, String status, String channel, String routeGroup, String routeAgent, Long createTime,
			String inSupportGroup) {
		this.id = id;
		this.org = org;
		this.pid = pid;
		this.status = status;
		this.channel = channel;
		this.routeGroup = routeGroup;
		this.routeAgent = routeAgent;
		this.createTime = createTime;
		this.inSupportGroup = inSupportGroup;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrg() {
		return this.org;
	}

	public void setOrg(String org) {
		this.org = org;
	}

	public String getPid() {
		return this.pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getChannel() {
		return this.channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getRouteGroup() {
		return this.routeGroup;
	}

	public void setRouteGroup(String routeGroup) {
		this.routeGroup = routeGroup;
	}

	public String getRouteAgent() {
		return this.routeAgent;
	}

	public void setRouteAgent(String routeAgent) {
		this.routeAgent = routeAgent;
	}

	public Long getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}

	public String getInSupportGroup() {
		return this.inSupportGroup;
	}

	public void setInSupportGroup(String inSupportGroup) {
		this.inSupportGroup = inSupportGroup;
	}

}
