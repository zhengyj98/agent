package cn.cellcom.agent.pojo;

/**
 * TCrmFieldOption entity.
 * 
 * @author MyEclipse Persistence Tools
 */

public class TCrmFieldOption implements java.io.Serializable {

	// Fields

	private String id;
	private String org;
	private String fieldFname;
	private String optionText;
	private String optionValue;
	private Long createTime;

	// Constructors

	/** default constructor */
	public TCrmFieldOption() {
	}

	/** minimal constructor */
	public TCrmFieldOption(String id) {
		this.id = id;
	}

	/** full constructor */
	public TCrmFieldOption(String id, String org, String fieldFname, String optionText, String optionValue, Long createTime) {
		this.id = id;
		this.org = org;
		this.fieldFname = fieldFname;
		this.optionText = optionText;
		this.optionValue = optionValue;
		this.createTime = createTime;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrg() {
		return this.org;
	}

	public void setOrg(String org) {
		this.org = org;
	}

	public String getFieldFname() {
		return this.fieldFname;
	}

	public void setFieldFname(String fieldFname) {
		this.fieldFname = fieldFname;
	}

	public String getOptionText() {
		return this.optionText;
	}

	public void setOptionText(String optionText) {
		this.optionText = optionText;
	}

	public String getOptionValue() {
		return this.optionValue;
	}

	public void setOptionValue(String optionValue) {
		this.optionValue = optionValue;
	}

	public Long getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}

}
