package cn.cellcom.agent.pojo;

import java.util.Date;

/**
 * TOrg entity.
 * 
 * @author MyEclipse Persistence Tools
 */

public class TOrg implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1300669439651898318L;
	// Fields

	private String pid;
	private String org;
	private String name;
	private String phone;
	private Date insertTime;
	private Date firstLogon;
	private String ip;

	// Constructors

	/** default constructor */
	public TOrg() {
	}

	/** minimal constructor */
	public TOrg(String pid, String org, String name, String phone, Date insertTime, String ip) {
		this.pid = pid;
		this.org = org;
		this.name = name;
		this.phone = phone;
		this.insertTime = insertTime;
		this.ip = ip;
	}

	/** full constructor */
	public TOrg(String pid, String org, String name, String phone, Date insertTime, Date firstLogon, String ip) {
		this.pid = pid;
		this.org = org;
		this.name = name;
		this.phone = phone;
		this.insertTime = insertTime;
		this.firstLogon = firstLogon;
		this.ip = ip;
	}

	// Property accessors

	public String getPid() {
		return this.pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getOrg() {
		return this.org;
	}

	public void setOrg(String org) {
		this.org = org;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getInsertTime() {
		return this.insertTime;
	}

	public void setInsertTime(Date insertTime) {
		this.insertTime = insertTime;
	}

	public Date getFirstLogon() {
		return this.firstLogon;
	}

	public void setFirstLogon(Date firstLogon) {
		this.firstLogon = firstLogon;
	}

	public String getIp() {
		return this.ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

}