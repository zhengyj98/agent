package cn.cellcom.agent.pojo;

import java.util.ArrayList;
import java.util.List;

/**
 * TCrmField entity.
 * 
 * @author MyEclipse Persistence Tools
 */

public class TCrmField implements java.io.Serializable {

	// Fields

	private String id;
	private String org;
	private String fname;
	private String fshowName;
	private String ftype;
	private Integer fsystem;
	private Integer fsearch;
	private Integer fenable;
	private Integer frequied;
	private Integer fmodify;
	private Integer flist;
	private Long createTime;
	private Integer ind;
	
	private List<TCrmFieldOption> options = new ArrayList<TCrmFieldOption>();

	public List<TCrmFieldOption> getOptions() {
		return options;
	}
	// Constructors


	/** default constructor */
	public TCrmField() {
	}

	/** minimal constructor */
	public TCrmField(String id, String org, String fname, String fshowName, Long createTime) {
		this.id = id;
		this.org = org;
		this.fname = fname;
		this.fshowName = fshowName;
		this.createTime = createTime;
	}

	/** full constructor */
	public TCrmField(String id, String org, String fname, String fshowName, String ftype, Integer fsystem, Integer fsearch, Integer fenable,
			Integer frequied, Integer fmodify, Integer flist, Long createTime, Integer ind) {
		this.id = id;
		this.org = org;
		this.fname = fname;
		this.fshowName = fshowName;
		this.ftype = ftype;
		this.fsystem = fsystem;
		this.fsearch = fsearch;
		this.fenable = fenable;
		this.frequied = frequied;
		this.fmodify = fmodify;
		this.flist = flist;
		this.createTime = createTime;
		this.ind = ind;
	}

	// Property accessors

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrg() {
		return this.org;
	}

	public void setOrg(String org) {
		this.org = org;
	}

	public String getFname() {
		return this.fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getFshowName() {
		return this.fshowName;
	}

	public void setFshowName(String fshowName) {
		this.fshowName = fshowName;
	}

	public String getFtype() {
		return this.ftype;
	}

	public void setFtype(String ftype) {
		this.ftype = ftype;
	}

	public Integer getFsystem() {
		return this.fsystem;
	}

	public void setFsystem(Integer fsystem) {
		this.fsystem = fsystem;
	}

	public Integer getFsearch() {
		return this.fsearch;
	}

	public void setFsearch(Integer fsearch) {
		this.fsearch = fsearch;
	}

	public Integer getFenable() {
		return this.fenable;
	}

	public void setFenable(Integer fenable) {
		this.fenable = fenable;
	}

	public Integer getFrequied() {
		return this.frequied;
	}

	public void setFrequied(Integer frequied) {
		this.frequied = frequied;
	}

	public Integer getFmodify() {
		return this.fmodify;
	}

	public void setFmodify(Integer fmodify) {
		this.fmodify = fmodify;
	}

	public Integer getFlist() {
		return this.flist;
	}

	public void setFlist(Integer flist) {
		this.flist = flist;
	}

	public Long getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}

	public Integer getInd() {
		return this.ind;
	}

	public void setInd(Integer ind) {
		this.ind = ind;
	}

}
