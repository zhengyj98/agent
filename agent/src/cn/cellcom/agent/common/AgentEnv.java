package cn.cellcom.agent.common;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cn.cellcom.jar.biz.IEnv;
import cn.cellcom.jar.file.FU;
import cn.cellcom.jar.util.CU;
import cn.cellcom.jar.util.MyProperties;

public class AgentEnv implements IEnv {

	// 静态方法定义必须早与static块，否则配置文件的配置将被这个定义所覆盖
	private static String redisHost = null;
	private static int redisPort = 0;
	public static String SETTING_FILE_PATH = "";
	public static String CHAT_FILE_PATH = "";
	public static String WECHAT_TEMP_PATH = "";
	public static String QUESTION_FILE_PATH = "";
	public static String EXPORT_PATH = "";
	public static String DEFAULT_IMG = "agent.png,test.png,visitor.png,";

	public static String WECHAT_RPC_REMOTE_HOST = null;

	public static int WECHAT_RPC_REMOTE_PORT = 0;

	public static int RPC_SERVER_PORT = 90;

	private Log log = LogFactory.getLog(AgentConstant.class);
	static {
		AgentEnv env = new AgentEnv();
		env.load();
	}

	public static String getRedisHost() {
		return redisHost;
	}

	public static int getRedisPort() {
		return redisPort;
	}

	public void load() {
		MyProperties pro = null;
		try {
			pro = new MyProperties(FU.getClassesRoot3path3name("/conf/common/env.properties"));
		} catch (Exception e) {
			log.error("载入系统初始化配置文件[env.properties]出错", e);
		}
		if (null != pro) {
			if (pro.get("redisHost") != null) {
				redisHost = String.valueOf(pro.get("redisHost"));
				redisPort = CU.toi(String.valueOf(pro.get("redisPort")));
				SETTING_FILE_PATH = String.valueOf(pro.get("SETTING_FILE_PATH"));
				FU.mkDir(SETTING_FILE_PATH);
				CHAT_FILE_PATH = String.valueOf(pro.get("CHAT_FILE_PATH"));
				FU.mkDir(CHAT_FILE_PATH);
				WECHAT_TEMP_PATH = String.valueOf(pro.get("WECHAT_TEMP_PATH"));
				FU.mkDir(WECHAT_TEMP_PATH);
				QUESTION_FILE_PATH = String.valueOf(pro.get("QUESTION_FILE_PATH"));
				FU.mkDir(QUESTION_FILE_PATH);
				EXPORT_PATH = String.valueOf(pro.get("EXPORT_PATH"));
				FU.mkDir(EXPORT_PATH);
				DEFAULT_IMG = String.valueOf(pro.get("DEFAULT_IMG"));
				RPC_SERVER_PORT = CU.toi(String.valueOf(pro.get("RPC_SERVER_PORT")));
				WECHAT_RPC_REMOTE_HOST = String.valueOf(pro.get("WECHAT_RPC_REMOTE_HOST"));
				WECHAT_RPC_REMOTE_PORT = CU.toi(String.valueOf(pro.get("WECHAT_RPC_REMOTE_PORT")));
			}
		}
	}
}
