package cn.cellcom.agent.common;

public class AgentConstant {
	/**
	 * 针对多选框的勾选配置value
	 */
	public static String on = "on";

	/**
	 * 会话内成员的状态
	 * 
	 * @author zhengyj
	 * 
	 */
	public enum MEMBER_STATUS {
		ONLINE, OFFLINE, END
	}

	public enum WECHAT_STEP {
		INIT, GET_GROUP, IN_GROUP
	}

	/**
	 * 操作结果
	 * 
	 * @author zhengyj
	 * 
	 */
	public enum RESULT {
		SUCCESS, ERROR, ARG_ERROR, NO_RECEIVER
	}

	/**
	 * 进入技能组的结局
	 * 
	 * @author zhengyj
	 * 
	 */
	public enum ENTER_GROUP_RESULT {
		ERROR, SELF, QUEUE, NO_AGENT_ONLINE, AGENT
	}

	/**
	 * 用户类型：系统
	 */
	public static final String USER_TYPE_SYSTEM = "s";

	/**
	 * 用户类型：坐席
	 */
	public static final String USER_TYPE_AGENT = "a";

	/**
	 * 用户类型：管理员
	 */
	public static final String USER_TYPE_MANAGER = "m";

	/**
	 * 用户类型：访客端
	 */
	public static final String USER_TYPE_VISITOR = "v";

	/**
	 * 用户状态：激活
	 */
	public static final String USER_STATUS_YES = "Y";

	/**
	 * 注册账号的默认等级
	 */
	public static final Integer USER_DEFAULT_LEVEL = 1;

	public static final String DEFAULT_ORG = "defaultorg";

	public static final String YES = "Y";
	public static final String NO = "N";

	/**
	 * 渠道模式
	 */
	public static enum VISITOR_STATUS {
		YES, NO
	};

	/**
	 * 渠道模式
	 */
	public static enum WINDOW_OPEN_MODE {
		I, P
	};

	/**
	 * 窗口模式
	 */
	public static enum WINDOW_SHOW_MODE {
		S, B
	};

	/**
	 * 渠道模式
	 */
	public static enum CHANNEL_TYPE {
		WEB, WECHAT, QQ, API
	};

	/**
	 * 引导的模式
	 * 
	 * @author zhengyj
	 * 
	 */
	public static enum GUIDE_POLICY {
		BUTTON, ALERT, SHOW_BOX, HIDE_BUTTON
	};

	/**
	 * 聊天模式
	 * 
	 * @author zhengyj
	 * 
	 */
	public static enum SELF_POLICY {
		NO_SELF, AFTER_GROUP, DIRECT
	};

	/**
	 * 默认号
	 */
	public static final String DEFAULT_NO = "10001";

	public static final String WIN_BIG = "big";

	public static final String WIN_SMALL = "small";

	/**
	 * 代表仅登陆坐席
	 */
	public static final Object AGENT_LOGIN = "agent";

	public static final String CRM_ID_IN_SESSION = "CRM_ID_IN_SESSION";

	/**
	 * 机器人类型
	 * 
	 * @author zhengyj
	 *
	 */
	public static enum ROBOT_TYPE {
		SELF_ROBOT, TULING_ROBOT
	};

	/**
	 * 问题的类型 F表示FAQ，M表示标准接待，A表示自定义
	 * 
	 * @author zhengyj
	 * 
	 */
	public static enum QUESTION_TYPE {
		F, B, A
	};

	/**
	 * S为单聊,G为群聊
	 * 
	 * @author zhengyj
	 *
	 */
	public static enum SESSION_TYPE {
		S, G
	};

	/**
	 * 创建源
	 * 
	 * @author zhengyj
	 *
	 */
	public static enum CREATE_SOURCE {
		IM, IMP, ADD
	};

	/**
	 * 扩展模块所在的位置
	 * 
	 * @author zhengyj
	 * 
	 */
	public static enum EXTEND {
		MENU, ONLINE
	};

	/**
	 * 排队策略
	 * 
	 * @author zhengyj
	 * 
	 */
	public static enum VISITOR_QUEUE_POLICY {
		BY_TIME, BY_LEVEL
	};

	/**
	 * 坐席退出的策略
	 * 
	 * @author zhengyj
	 * 
	 */
	public static enum AGENT_LOGOU_POLICY {
		NOTHING, END_SESSION
	};

	/**
	 * 访客的来源渠道
	 * 
	 * @author zhengyj
	 * 
	 */
	public static enum CRM_ADD_TYPE {
		NICK, INTERFACE, REGISTER, SYSTEM
	};

	/**
	 * 定义错误码
	 * 
	 * @author zhengyj
	 * 
	 */
	public static enum ERROR_CODE {
		ARG_ERROR, SYS_ERROR, GET_GROUP_ERROR, LIST_GROUP_ERROR
	};

	/**
	 * 坐席进入会话的方式
	 * 
	 * @author zhengyj
	 * 
	 */
	public static enum AGENT_JOIN_TYPE {
		RECEIVE_QUEUE, AGENT_INVITE, AGENT_TRANSFER, AGENT_MONITOR
	};

	/**
	 * 无坐席在线是否可以排队
	 * 
	 * @author zhengyj
	 * 
	 */
	public static enum NO_ONLINE_AGENT_POLICY {
		ENABLE, DISABLE
	};

	/**
	 * 排队时的发言机制
	 * 
	 * @author zhengyj
	 * 
	 */
	public static enum QUEUE_TALKING_POLICY {
		DISABLE, ENABLE, ENABLE_TIP, ENABLE_SELF
	};

	/**
	 * 坐席聊天时候的机制
	 * 
	 * @author zhengyj
	 * 
	 */
	public static enum AGENT_TALKING_POLICY {
		DISABLE_SELF, ENABLE_SELF, BUSY_TO_SELF
	};

	/**
	 * 自助回复的结果
	 * 
	 * @author zhengyj
	 * 
	 */
	public static enum SELF_RESPONSE {
		ZERO, ONE, MORE
	};

	/**
	 * 会话的会话步骤
	 * 
	 * @author zhengyj
	 * 
	 */
	public static enum SESSION_STEP {
		LOGIN, SELF, QUEUE, AGENT
	};

	/**
	 * 关闭原因<br>
	 * 排队结束的原因有：BY_VISITOR[访客主动] BY_REBOOT[系统重启] QUEUE_OVER_TIME[排队超过了等待时间]<br>
	 * 坐席会话结束的原因有：BY_VISITOR[访客主动] BY_AGENT[坐席主动] BY_OFFLINE[访客离线]
	 * BY_SILENT[访客X分钟无消息]<br>
	 * 自助聊天结束的原因有：BY_VISITOR[访客主动] BY_OFFLINE[访客离线]
	 * 
	 * @author zhengyj
	 * 
	 */
	public static enum SESSION_END_TYPE {
		BY_VISITOR, BY_AGENT, BY_SILENT, BY_QUEUE_OVER_TIME, BY_REBOOT, BY_OFFLINE
	};

	/**
	 * 发送消息的角色 {SYSTEM:"s", AGENT:"a", MANAGER:"m", VISITOR:"v"};
	 * 
	 * @author Administrator
	 *
	 */
	public static enum SENDER_ROLE {
		s, a, m, v
	};

	/**
	 * 坐席会话结束的原因
	 * 
	 * @author zhengyj
	 * 
	 */
	public static enum AG_SESSION_END_TYPE {
		BY_REPEAT, BY_AGENT, BY_AG_LOGOUT, AG_TRANSFER2_SUCC
	};

	/**
	 * 配置信息的等级
	 */
	public static enum SETTING_LEVEL {
		DEFAULT, ORG, CHANNEL, WORKGROUP, AGENT
	};

	/**
	 * 回调事件
	 * 
	 * @author zhengyj
	 *
	 */
	public static enum ThirdCallback {
		crmGet, endSession, satisfy, orderList, viewList
	};

	/**
	 * 查询的范围
	 */
	public static enum DURATION {
		TODAY(0), YESTERDAY(1), WEEK(7), MONTH(30);
		private int value = 0;

		DURATION(int value) {
			this.value = value;
		}

		public static DURATION valueOf(int value) {
			switch (value) {
			case 0:
				return TODAY;
			case 1:
				return YESTERDAY;
			case 7:
				return WEEK;
			case 30:
				return MONTH;
			default:
				return null;
			}
		}

		public int value() {
			return this.value;
		}
	};

	/**
	 * 
	 * @author zhengyj
	 * 
	 */
	public static enum AGENT_STATUS {
		ONLINE, GO_AWAY, OFFLINE
	};

	/**
	 * TSetting-sessionDispatchPolicy-0,0,随机分配<br>
	 * TSetting-sessionDispatchPolicy-1,1,当日总量最少优先<br>
	 * TSetting-sessionDispatchPolicy-2,2,当前接待最少优先<br>
	 * TSetting-sessionDispatchPolicy-3,3,坐席级别最高优先<br>
	 * 
	 * @author zhengyj
	 * 
	 */
	public static enum SESSION_DISPATCH_POLICY {
		RANDOM, CURRENT_MIN, CURRENT_FREE, LEVEL, LAST_AGENT
	};

}
