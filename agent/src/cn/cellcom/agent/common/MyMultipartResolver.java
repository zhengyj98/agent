package cn.cellcom.agent.common;

import java.util.List;

import org.apache.commons.fileupload.FileUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

/**
 * 
 */
public class MyMultipartResolver extends CommonsMultipartResolver {

	private static Logger logger = LoggerFactory.getLogger(MyMultipartResolver.class);

	@Override
	protected MultipartParsingResult parseFileItems(List arg0, String arg1) {
		return super.parseFileItems(arg0, arg1);
	}

	@Override
	protected FileUpload prepareFileUpload(String encoding) {
		return super.prepareFileUpload(encoding);
	}
}
