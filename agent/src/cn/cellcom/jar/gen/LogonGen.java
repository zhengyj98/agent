package cn.cellcom.jar.gen;

import cn.cellcom.jar.gen.common.CommonDpActConfig;
import cn.cellcom.jar.gen.logon.LogonVelocity;

/**
 * 该类将读取logongen.properties进行生成登陆的代码。 基本上这是项目的第一步
 * 
 * @author Administrator
 * 
 */
public class LogonGen {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		CommonDpActConfig.config();
		LogonVelocity.gen();
	}

}
