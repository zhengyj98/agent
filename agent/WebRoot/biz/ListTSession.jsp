<%@ page contentType="text/html; charset=UTF-8" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>访客会话管理</title>
	<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />    
	
	<!--请在下面增加js-->
   	<script type="text/javascript" src="../js/mycommon.jquery.js"></script>
	<script type="text/javascript" src='../plugins/jquery-x.x.js'> </script>	
	<script type="text/javascript" src="../plugins/layer/layer.js"></script>
	<script type="text/javascript" src='../js/online/agent-common.js'> </script>

</head>

<body>
	<html:form action="/biz/TSessionDpAct?method=list" styleId="TSessionForm">
		<div id="main">
			<div id="tab-top">
				<div id="lift"></div>
				<div id="pt">访客会话管理</div>
				<div id="right"></div>
			</div>
			
			<div id="table">
				<div id="ptk"> <div id="tabtop-l"> </div>
				<div id="tabtop-z">输入查询条件</div>
				<div id="tabtop-r1"></div></div>
			</div>
		
		
			<div id="main-tab">
				<div id="info-4">           
					<li>渠道：<html:text property='channel' styleClass='s-input-2' /></li>
					<li>访客电话：<html:text property='phone' styleClass='s-input-2' /></li>
					<li>结束原因：<myjsp:select name='endType' width='100' id='endType' listname='endTypelist' first='----' value='${TSessionForm.endType}' readonly='' showonly=''/></li>
					<li>最后步骤：<myjsp:select name='step' width='100' id='step' listname='steplist' first='----' value='${TSessionForm.step}' readonly='' showonly=''/></li>
					<li style="width:80px"><html:submit styleClass="search-2" value="查询" /></li>	
					<li style="width:80px"><input class='search-2' type='button' value='导出数据' onClick='return exportList("../biz/TSessionDpAct.do?method=doExport")'></li>			
				</div>          
			</div>
		
			<div id="table">
				<div id="ptk"> 
					<div id="tabtop-l"> </div>
					<div id="tabtop-z">当前查询结果</div>
					<li></li>
					<li></li>
					<li></li>					
				</div>					
			</div>
		
			<div id="fh-flie-2"> </div>
			<div id="main-tablist">
				<table width='100%' border='0' cellpadding='0' cellspacing='0' id='showData'>
					<tr id='minpt-tab'>
						<td>渠道</td>
						<td>请求组</td>
						<td>访客</td>
						<td>请求时间</td>
						<td>自助聊天</td>
						<td>排队</td>
						<td>访客结束</td>
						<td>坐席</td>
						<td>结束原因</td>
						<td>满意度</td>
						<td>最后步骤</td>
						<td>操作</td>
					</tr>

					<c:forEach var="aRecord" items="${result}">
						<tr class='out' onMouseOver='this.className="over"' onMouseOut='this.className="out"'>
							<td class='zw-txt'>${aRecord[0].channel}</td>
							<td class='zw-txt'>${aRecord[2].name}</td>
							<td class='zw-txt'>${aRecord[1].name}<br/>[电话:${aRecord[1].phone}]</td>
							<td class='zw-txt'><myjsp:date value='${aRecord[0].requestTime}' format='yyyy-MM-dd HH:mm:ss' /></td>
							<td class='zw-txt'><myjsp:date value='${aRecord[0].robotTime}' format='yyyy-MM-dd HH:mm:ss' /></td>
							<td class='zw-txt'><myjsp:date value='${aRecord[0].queueTime}' format='yyyy-MM-dd HH:mm:ss' /></td>
							<td class='zw-txt'><myjsp:date value='${aRecord[0].endTime}' format='yyyy-MM-dd HH:mm:ss' /></td>
							<td class='zw-txt'>${aRecord[0].agent}<br/>${aRecord[0].agentName}<br/><myjsp:date value='${aRecord[0].agentTime}' format='yyyy-MM-dd HH:mm:ss' /></td>
							<td class='zw-txt'><myjsp:select name='endType' width='0' id='endType' listname='endTypelist' first='----' value='${aRecord[0].endType}' readonly='' showonly='true'/></td>
							<td class='zw-txt'>${aRecord[0].satisfyScore}</td>
							<td class='zw-txt'><myjsp:select name='step' width='0' id='step' listname='steplist' first='----' value='${aRecord[0].step}' readonly='' showonly='true'/></td>
							<td class='zw-txt'>
							&nbsp;<a href='../biz/TSessionDpAct.do?method=detail&id=${aRecord[0].id}'>详细</a>
							<br/><a href='javascript:showLogs("${aRecord[0].id}")'>聊天历史</a>
							</td>
						</tr>
					</c:forEach>
				</table>
				
				<div id="info-pz"> 
					${link}
				</div>
			</div>
		</div>
	</html:form>
</body>
</html>
