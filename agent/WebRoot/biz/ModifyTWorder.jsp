<%@ page contentType="text/html; charset=UTF-8" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>修改工单</title>
	<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />    
	<link rel="stylesheet" href="../plugins/select2/select2.min.css" type="text/css" title="styles1" />    
	
	<!--请在下面增加js-->
   	<script type="text/javascript" src="../js/mycommon.js"></script>
	<script type="text/javascript" src='../plugins/jquery-x.x.js'> </script>
	<script type="text/javascript" src='../plugins/tinymce/tinymce.min.js'> </script>
	<script type="text/javascript" src='../plugins/tinymce/jquery.tinymce.min.js'> </script>
	<script type="text/javascript" src='../plugins/tinymce/tinymce-extend.js'> </script>
	<script type="text/javascript" src="../js/jquery.validate.js"></script>
	<script type="text/javascript" src="../js/jquery.mine.js"></script>	
	<script type="text/javascript" src="../js/TWorder.js"></script>
	<script type="text/javascript" src="../js/TWorder2.js"></script>
	<script type="text/javascript" src="../plugins/layer/layer.js"></script>
	<script type="text/javascript" src="../plugins/select2/select2.js"></script>
	<style type="">
		.addCrm {
		    width: 15px !important;
		    height: 15px !important;
		    color: #fff;
		    background-color: #039be5;
		    display: inline-block;
		    border-radius: 50% !important;
		    text-align: center;
		    line-height: 13px;
		    padding: 0;
		    margin-right: 6px;
		    cursor:pointer;
		}
		li {
		    float: inherit;
		    text-indent: 0px;
		}
	</style>
</head>

<body>
<div id="main">
  	<div id="tab-top">
  		<div id="ptk1">
	  		<div id="lift"></div>
	  		<div id="pt">修改工单</div>
  		    <div id="right"></div>
  		</div>
 	</div>
	<div id="table">
	    <div id="ptk">
	   		<div id="tabtop-l"></div>
	    	<div id="tabtop-z">输入信息</div>
	    	<div id="tabtop-r1"></div>
      </div>
	</div>
	<div id="main-tab">
		<html:form action="/biz/TWorderDpAct?method=doModify" styleId="TWorderForm" >
			<html:hidden property='id' name='result' />
			<table width="911" align="center" class="table-slyle-hs" >
				<tr>
					<td>工单号：</td>
					<td>${result.sn}</td>
					<td>创建于：</td>
					<td><myjsp:date value='${result.createTime}' format='yyyy-MM-dd HH:mm:ss' /></td>
				</tr>
				<tr>
					<td>创建源：</td>
					<td><myjsp:select name='createSource' added="class='s-select'" id='createSource' listname='createSourcelist' value='${result.createSource}' readonly='' showonly='true'/></td>
					<td>创建对象：</td>
					<td>${result.createObject}</td>
				</tr>	
				<tr>
					<td>状态：</td>
					<td><myjsp:select name='status' added="class='s-select'" id='status' listname='statuslist' value='${result.status}' readonly='' showonly=''/>*</td>
					<td>等级：</td>
					<td><myjsp:select name='level' added="class='s-select'" id='level' listname='levellist' value='${result.level}' readonly=' ' showonly=''/></td>
				</tr>
				<tr>
					<td>客户：</td>
					<td>
						<select name="crm" id="crm" class="s-select">
							<option value="">请选择客户</option>
						</select><span class="addCrm" onclick="showAddCrm()">+</span>
					</td>
					<td>受理坐席：</td>
					<td>
						<select name="toAgent" id="toAgent" class="s-select">
							<option value="">请选择受理坐席</option>
						</select>
					</td>
				</tr>			
				<tr>
					<td>主题：</td>
					<td colspan="3"><html:text property='subject' name='result' readonly='' styleClass='s-input' style="width:800px" maxlength='100'/></td>
				</tr>
				<tr>
					<td>工单内容：</td>
					<td colspan="3">
						<html:textarea property='context' name='result' styleId='context' rows='4' cols='50' />
					</td>
				</tr>
				<tr>
					<td align="right"><input name="submit" type="submit" class="search-2" value="保存"/></td>
					<td colspan="3"><input name="fanhui" type="button" class="search-2" value="返回" onclick="window.location='../common/Common.do?method=goback&&action=goback&pgUri=${pgUri}'"/></td>					 
				</tr>				
			</table>
		</html:form>
	</div>

</div>
</body>
</html>
