<%@ page contentType="text/html; charset=UTF-8" language="java" errorPage=""%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>菜单列表</title>
	
	    <link rel="stylesheet" href="../themes/skyblue/skyblueLeft.css"type="text/css" title="styles1" />
	    
		<script type="text/javascript" src="../themes/js/prototype.lite.js"></script>
		<script type="text/javascript" src="../themes/js/moo.fx.js"></script>
		<script type="text/javascript" src="../themes/js/moo.fx.pack.js"></script>
		<script src="../themes/js/moo.dom.js" type="text/javascript"></script>
		<script type="text/javascript">
			function init(){
				var stretchers = document.getElementsByClassName('stretcher2'); //div that stretches
				var toggles = document.getElementsByClassName('display'); //h3s where I click on
				//accordion effect
				var myAccordion = new fx.Accordion(
					toggles, stretchers, {opacity: false, duration: 300}
				);
				//myAccordion.showThisHideOpen(stretchers[0]);
				//hash functions
				var found = false;
				toggles.each(function(h3, i){
					var div = Element.find(h3, 'nextSibling'); //element.find is located in prototype.lite
					if (window.location.href.indexOf(h3.title) > 0) {
						myAccordion.showThisHideOpen(div);
						found = true;
					}
				});
				if (!found) myAccordion.showThisHideOpen(stretchers[0]);
			}
		</script>
	</head>

	<body>
    <div >
      <div id="meunimg-bg">
        <div id="meunbar" >
          <div id="dhtmlxTree">
				<%@ include file="menu.jsp"%>
          </div>
        </div>
      </div>
    </div>
<script type="text/javascript">
		Element.cleanWhitespace('content');
		init();
</script>	
<script type="text/javascript" src="../themes/js/jquery.js"></script>
								
</body>
</html>
