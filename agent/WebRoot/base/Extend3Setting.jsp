<%@ page contentType="text/html; charset=UTF-8" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>扩展配置</title>
	<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />    
	
	
	<!--请在下面增加js-->
   	<script type="text/javascript" src="../js/mycommon.jquery.js"></script>
	<script type="text/javascript" src="../plugins/datepicker/WdatePicker.js"></script>
	<script type="text/javascript" src='../plugins/jquery-x.x.js'> </script>	
</head>

<body onload="show()">
	<html:form action="/base/TSettingDpAct?method=doModifyOrgSetting" styleId="CommonForm">
		<html:hidden property="tag"/>
		<html:hidden property="level"/>
		<html:hidden property="subject"/>
		<div id="main">
			<div id="tab-top">
				<div id="lift"></div>
				<div id="pt">扩展坐席</div>
				<div id="right"></div>
			</div>
		
			<div id="fh-flie-2"> </div>
			<div id="main-tablist">
				<table width='100%' border='0' cellpadding='0' cellspacing='0' id='showData'>	
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt' width='250px'>ACCESS_KEY：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='accessKey' id='accessKey' value='${result.accessKey}' add="style='width:150px;height:20px'" line='1'/>*
							</td>
						</tr>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>接口回调地址：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='callUrl' id='callUrl' value='${result.callUrl}' add="style='width:400px;height:30px'" line='1'/>
							</td>
						</tr>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>回调动作：</td>
							<td class='zw-txt' align='left'>
								<myjsp:scheckbox name='callMethod' id='crmGet' value='${result.callMethod}' rvalue='C1'/><span title="访客进入系统时，如果带有tid参数，则调用回调地址获取CRM信息">CRM提取</span>
							</td>
						</tr>						
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>温馨提示<br/>我们回调的方式如下：</td>
							<td class='zw-txt' align='left'>
								String timestamp = String.valueOf(System.currentTimeMillis());<br/>
								String token = MD5(tid+timestamp+accessKey);//token是三个参数的MD5<br/>
								HttpPost request = new HttpPost(callUrl);<br/>
								request.setParams(httpClientParams);<br/>
								Map<String, String> params = new HashMap<String, String>();<br/>
								params.put("tid", tid);<br/>
								params.put("timestamp", timestamp);<br/>
								params.put("token", token);<br/>
								String json = JSONObject.fromObject(params).toString();<br/>
								StringEntity entity = new StringEntity(json, "UTF-8");<br/>
								entity.setContentType("application/json");<br/>
								request.setEntity(entity);//JSON格式传递了tid/timestamp/token参数<br/>
							</td>
						</tr>
				</table>
				
				<div align="center"> 
					<input type="submit" name="aa" value="确认保存" class='search-2'>&nbsp<input type="button" name="bb" value="返回" class='search-2' onclick="history.back();">
				</div>
			</div>
		</div>
	</html:form>
</body>
</html>
