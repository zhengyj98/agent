<%@ page contentType="text/html; charset=UTF-8" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>提示语</title>
	<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />    
	
	
	<!--请在下面增加js-->
   	<script type="text/javascript" src="../js/mycommon.jquery.js"></script>
	<script type="text/javascript" src="../plugins/datepicker/WdatePicker.js"></script>
	<script type="text/javascript" src='../plugins/jquery-x.x.js'> </script>	

</head>

<body>
	<html:form action="/base/TSettingDpAct?method=doModifyOrgSetting" styleId="CommonForm">
		<html:hidden property="tag"/>
		<html:hidden property="level"/>
		<html:hidden property="subject"/>
		<div id="main">
			<div id="tab-top">
				<div id="lift"></div>
				<div id="pt">提示语</div>
				<div id="right"></div>
			</div>
		
			<div id="fh-flie-2"> </div>
			<div id="main-tablist">
				<table width='100%' border='0' cellpadding='0' cellspacing='0' id='showData'>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>机器人不懂回答：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='selfUnknowTip' id='selfUnknowTip' value='${result.selfUnknowTip}' add="style='width:400px'" line='2'/>
							</td>
						</tr>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>自动转人工提示：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='selfAutoToManualTip' id='selfAutoToManualTip' value='${result.selfAutoToManualTip}' add="style='width:400px'" line='2'/>
							</td>
						</tr>						
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>坐席会话协助：</td>
							<td class='zw-txt' align='left'>
								<myjsp:select added='class="s-select"' name='agentTalkingPolicy' id='agentTalkingPolicy' listname='agentTalkingPolicyList' value='${result.agentTalkingPolicy}'/>
							</td>
						</tr>						
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>排队时回复：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='visitorQueueTip' id='visitorQueueTip' value='${result.visitorQueueTip}' add="style='width:400px'" line='3'/>
							</td>
						</tr>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>访客结束会话：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='visitorEndSessionTip' id='visitorEndSessionTip' value='${result.visitorEndSessionTip}' add="style='width:400px'" line='3'/>
							</td>
						</tr>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>无坐席提示语：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='noAgentOnlineTip' id='noAgentOnlineTip' value='${result.noAgentOnlineTip}' add="style='width:400px'" line='3'/>
							</td>
						</tr>
						<tr class='out' onMouseOver='this.className="over"' onMouseOut='this.className="out"'>
							<td class='zw-txt' width='250px'>自助引导：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='enterFirstTip' id='enterFirstTip' value='${result.enterFirstTip}' add="style='width:400px'" line='2'/>
							</td>
						</tr>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>选择组的提示：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='beforeGroupList' id='beforeGroupList' value='${result.beforeGroupList}' add="style='width:400px'" line='3'/>[可以改为推广词]
							</td>
						</tr>	
						<tr class='out' onMouseOver='this.className="over"' onMouseOut='this.className="out"'>
							<td class='zw-txt'>弹框引导：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='guidePopTip' id='guidePopTip' value='${result.guidePopTip}' add="style='width:400px'" line='2'/>
							</td>
						</tr>
						<tr class='out' onMouseOver='this.className="over"' onMouseOut='this.className="out"'>
							<td class='zw-txt'>下班拒接会话：</td>
							<td class='zw-txt' align="left">
								<myjsp:sinput name='offdutyRejectTip' id='offdutyRejectTip' value='${result.offdutyRejectTip}' add="style='width:400px'" line='2'/>		
							</td>
						</tr>	
						<tr class='out' onMouseOver='this.className="over"' onMouseOut='this.className="out"'>
							<td class='zw-txt'>下班接入会话：</td>
							<td class='zw-txt' align="left">
								<myjsp:sinput name='offdutyReceiptTip' id='offdutyReceiptTip' value='${result.offdutyReceiptTip}' add="style='width:400px'" line='2'/>		
							</td>
						</tr>	
						<tr class='out' onMouseOver='this.className="over"' onMouseOut='this.className="out"'>
							<td class='zw-txt'>上班接入会话：</td>
							<td class='zw-txt' align="left">
								<myjsp:sinput name='ondutyReceiptTip' id='ondutyReceiptTip' value='${result.ondutyReceiptTip}' add="style='width:400px'" line='2'/>		
							</td>
						</tr>	
						<tr class='out' onMouseOver='this.className="over"' onMouseOut='this.className="out"'>
							<td class='zw-txt'>坐席结束会话：</td>
							<td class='zw-txt' align="left">
								<myjsp:sinput name='agentEndSessionTip' id='agentEndSessionTip' value='${result.agentEndSessionTip}' add="style='width:400px'" line='2'/>		
							</td>
						</tr>		
						<tr class='out' onMouseOver='this.className="over"' onMouseOut='this.className="out"'>
							<td class='zw-txt'>超时结束会话：</td>
							<td class='zw-txt' align="left">
								<myjsp:sinput name='overtimeEndSessionTip' id='overtimeEndSessionTip' value='${result.overtimeEndSessionTip}' add="style='width:400px'" line='2'/>		
							</td>
						</tr>	
						<tr class='out' onMouseOver='this.className="over"' onMouseOut='this.className="out"'>
							<td class='zw-txt'>坐席离线：</td>
							<td class='zw-txt' align="left">
								<myjsp:sinput name='agentLeftTip' id='agentLeftTip' value='${result.agentLeftTip}' add="style='width:400px'" line='2'/>		
							</td>
						</tr>				
						<tr class='out' onMouseOver='this.className="over"' onMouseOut='this.className="out"'>
							<td class='zw-txt'>静默提示：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='silentNoticeTip' id='silentNoticeTip' value='${result.silentNoticeTip}' add="style='width:400px'" line='2'/>
							</td>
						</tr>
						<tr class='out' onMouseOver='this.className="over"' onMouseOut='this.className="out"'>
							<td class='zw-txt'>静默结束：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='silentEndTip' id='silentEndTip' value='${result.silentEndTip}' add="style='width:400px'" line='2'/>
							</td>
						</tr>		
						<tr class='out' onMouseOver='this.className="over"' onMouseOut='this.className="out"'>
							<td class='zw-txt'>评价感谢：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='satisfyThanksTip' id='satisfyThanksTip' value='${result.satisfyThanksTip}' add="style='width:400px'" line='2'/>
							</td>
						</tr>
						<tr class='out' onMouseOver='this.className="over"' onMouseOut='this.className="out"'>
							<td class='zw-txt'>评价邀请：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='satisfyInviteTip' id='satisfyInviteTip' value='${result.satisfyInviteTip}' add="style='width:400px'" line='2'/>
							</td>
						</tr>						
				</table>
				
				<div align="center"> 
					<input type="submit" name="aa" value="确认保存" class='search-2'>&nbsp<input type="button" name="bb" value="返回" class='search-2' onclick="history.back();">
				</div>
			</div>
		</div>
	</html:form>
</body>
</html>
