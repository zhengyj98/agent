<%@ page contentType="text/html; charset=UTF-8" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>坐席配置</title>
	<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />    
	
	
	<!--请在下面增加js-->
   	<script type="text/javascript" src="../js/mycommon.jquery.js"></script>
	<script type="text/javascript" src="../plugins/datepicker/WdatePicker.js"></script>
	<script type="text/javascript" src='../plugins/jquery-x.x.js'> </script>	

	<script>
		function show() {
			var v = $("#chatMaxType").val();
			if(v == 1) {
				$("#chatMaxLimitTr").show();
			} else {
				$("#chatMaxLimitTr").hide();
			}
		}
	</script>
</head>

<body onload="show()">
	<html:form action="/base/TSettingDpAct?method=doModifyOrgSetting" styleId="CommonForm">
		<html:hidden property="tag"/>
		<html:hidden property="level"/>
		<html:hidden property="subject"/>
		<div id="main">
			<div id="tab-top">
				<div id="lift"></div>
				<div id="pt">坐席配置</div>
				<div id="right"></div>
			</div>
		
			<div id="fh-flie-2"> </div>
			<div id="main-tablist">
				<table width='100%' border='0' cellpadding='0' cellspacing='0' id='showData'>	
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>坐席会话协助：</td>
							<td class='zw-txt' align='left'>
								<myjsp:select added='class="s-select"' name='agentTalkingPolicy' id='agentTalkingPolicy' listname='agentTalkingPolicyList' value='${result.agentTalkingPolicy}'/>
							</td>
						</tr>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>坐席退出或超时：</td>
							<td class='zw-txt' align='left'>
								<myjsp:select added='class="s-select"' name='agentLogout' id='agentLogout' listname='agentLogoutList' value='${result.agentLogout}'/>
							</td>
						</tr>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>登陆默认状态：</td>
							<td class='zw-txt' align='left'>
								<myjsp:select name='loginDefaultStatus' id='loginDefaultStatus' listname='loginDefaultStatusList' value='${result.loginDefaultStatus}' added='class="s-select" onclick="show()"'/>
							</td>
						</tr>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt' width='250px'>最大会话数控制：</td>
							<td class='zw-txt' align='left'>
								<myjsp:select name='chatMaxType' id='chatMaxType' listname='chatMaxTypeList' value='${result.chatMaxType}' added='class="s-select" onclick="show()"'/>
							</td>
						</tr>			
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"' id="chatMaxLimitTr">
							<td class='zw-txt'>统一最大数：</td>
							<td class='zw-txt' align='left'>
								<myjsp:sinput name='chatMaxLimit' id='chatMaxLimit' value='${result.chatMaxLimit}' add="style='width:30px;height:20px'" line='1'/>个会话，统一设置后坐席将不能进行自定义聊天最大数
							</td>
						</tr>
						<tr class='out-70' onMouseOver='this.className="over-70"' onMouseOut='this.className="out-70"'>
							<td class='zw-txt'>会话分配策略：</td>
							<td class='zw-txt' align='left'>
								<myjsp:select added='class="s-select"' name='sessionDispatchPolicy' id='sessionDispatchPolicy' listname='sessionDispatchPolicyList' value='${result.sessionDispatchPolicy}' readonly='' showonly='' />				
							</td>
						</tr>							
				</table>
				
				<div align="center"> 
					<input type="submit" name="aa" value="确认保存" class='search-2'>&nbsp<input type="button" name="bb" value="返回" class='search-2' onclick="history.back();">
				</div>
			</div>
		</div>
	</html:form>
</body>
</html>
