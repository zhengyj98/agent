<%@ page contentType="text/html; charset=UTF-8" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>新增渠道</title>
	<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />    
	
	
	<!--请在下面增加js-->
   	<script type="text/javascript" src="../js/mycommon.js"></script>
	<script type="text/javascript" src="../plugins/datepicker/WdatePicker.js"></script>
	<script type="text/javascript" src='../plugins/jquery-x.x.js'> </script>
	<script type="text/javascript" src="../js/jquery.validate.js"></script>
	<script type="text/javascript" src="../js/jquery.mine.js"></script>
	<script type="text/javascript" src="../js/TChannel.js"></script>	
</head>

<body>
<div id="main">
	<div id="tab-top">
		<div id="ptk1">
			<div id="lift"></div>
			<div id="pt">新增渠道</div>
			<div id="right"></div>
		</div>
	</div>
	<div id="table">
		<div id="ptk">
			<div id="tabtop-l"></div>
			<div id="tabtop-z">输入信息</div>
			<div id="tabtop-r1"></div>
		</div>
	</div>
	<div id="main-tab">
		<html:form action="/base/TChannelDpAct?method=doAdd" styleId="TChannelForm" >
			<p align="center">${result}</p>		
			<table width="911" align="center" class="table-slyle-hs" >		
				<tr>
					<td>渠道号：</td>
					<td><html:text property='no' style='width:265px' maxlength='40' styleClass='s-input'/>*</td>
				</tr>				
				<tr>
					<td>渠道名：</td>
					<td><html:text property='name' style='width:265px' maxlength='40' styleClass='s-input'/>*</td>
				</tr>
				<tr>
					<td>类型：</td>
					<td><myjsp:select name='type' id='type' listname='typelist' value='' readonly='' showonly='' added='class="s-select" onchange="javascript:change()"'/>*</td>
				</tr>
				<tr style="display: none" id="tr_appid">
					<td>appid：</td>
					<td><html:text property='wcAppid' style='width:265px' maxlength='40' styleClass='s-input'/></td>
				</tr>
				<tr style="display: none" id="tr_appsecret">
					<td>appsecret：</td>
					<td><html:text property='wcSecret' style='width:265px' maxlength='40' styleClass='s-input'/></td>
				</tr>
				<tr id="tr_webUrl">
					<td>weburl：</td>
					<td><html:text property='webUrl' style='width:265px' maxlength='40' styleClass='s-input'/></td>
				</tr>
				<tr id="tr_qq">
					<td>QQ：</td>
					<td><html:text property='qq' style='width:265px' maxlength='40' styleClass='s-input'/></td>
				</tr>
				<tr>
					<td>备注：</td>
					<td><html:textarea property='memo' styleId='memo' onkeyup="areaTextMaxLength('memo', 'memoDiv', 100)" rows='3' cols='50'/>*<memoDiv></memoDiv></td>
				</tr>

				<tr>
				<td align="right"><input type="submit" class="search-2" value="新增"/></td>
				<td><input type="button" class="search-2" value="返回" onclick="window.location='../common/Common.do?method=goback&&action=goback&pgUri=${pgUri}'"/></td>
				</tr>
			</table>
		</html:form>
	</div>
<script>
function change() {
	if($("#type").val() == "WEB") {
		$("#tr_appid").hide();
		$("#tr_appsecret").hide();
		$("#tr_qq").hide();
		$("#tr_webUrl").show();
	} else if($("#type").val() == "WECHAT") {
		$("#tr_appid").show();
		$("#tr_appsecret").show();
		$("#tr_qq").hide();
		$("#tr_webUrl").hide();
	} else if($("#type").val() == "QQ") {
		$("#tr_appid").hide();
		$("#tr_appsecret").hide();
		$("#tr_qq").show();
		$("#tr_webUrl").hide();
	}
}
</script>
</div>
</body>
</html>
