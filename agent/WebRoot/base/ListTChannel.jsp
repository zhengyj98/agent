<%@ page contentType="text/html; charset=UTF-8" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>渠道管理</title>
	<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />    
	
	
	<!--请在下面增加js-->
   	<script type="text/javascript" src="../js/mycommon.jquery.js"></script>
	<script type="text/javascript" src="../plugins/datepicker/WdatePicker.js"></script>
	<script type="text/javascript" src='../plugins/jquery-x.x.js'> </script>	
	<%
	String host = request.getLocalAddr();
	int port = request.getServerPort();
	String path = request.getContextPath();
	%>
</head>

<body>
	<html:form action="/base/TChannelDpAct?method=list" styleId="TChannelForm">
		<div id="main">
			<div id="tab-top">
				<div id="lift"></div>
				<div id="pt">渠道管理</div>
				<div id="right"></div>
			</div>
		
			<div id="table">
				<div id="ptk"> 
					<div id="tabtop-l"> </div>
					<div id="tabtop-z">当前查询结果</div>
					<li><input class='search-2' type='button' value='新增渠道' onClick='window.location="../base/TChannelDpAct.do?method=add"'></li>
					<li></li>
					<li></li>					
				</div>					
			</div>
		
			<div id="fh-flie-2"> </div>
			<div id="main-tablist">
				<table width='100%' border='0' cellpadding='0' cellspacing='0' id='showData'>
					<tr id='minpt-tab'>
						<td>渠道号</td>
						<td>渠道名</td>
						<td>备注</td>
						<td>类型</td>
						<td>操作</td>
					</tr>

					<c:forEach var="aRecord" items="${result}">
						<tr class='out' onMouseOver='this.className="over"' onMouseOut='this.className="out"'>
							<td class='zw-txt'>${aRecord.no}</td>
							<td class='zw-txt'>${aRecord.name}</td>
							<td class='zw-txt'>${aRecord.memo}</td>
							<td class='zw-txt'><myjsp:select name='type' width='0' id='type' listname='typelist' first='----' value='${aRecord.type}' readonly='' showonly='true'/></td>
							<td class='zw-txt'>
								<a href='../base/TChannelDpAct.do?method=modify&id=${aRecord.id} '>修改信息/配置路由</a>
								<c:if test="${aRecord.type=='WEB'}">
									&nbsp<a href='javascript:$("#code_${aRecord.no}").toggle()'>获取代码</a>
								</c:if>
								<br/>								
								&nbsp<a href='../base/TSettingDpAct.do?method=list&tag=TIP&level=2&subject=${aRecord.no}'>特定提示语</a>
								&nbsp<a href='../base/TSettingDpAct.do?method=list&tag=SELF&level=2&subject=${aRecord.no}'>特定自助</a>
								&nbsp<a href='../base/TSettingDpAct.do?method=list&tag=UI&level=2&subject=${aRecord.no}'>特定UI</a>
							</td>
						</tr>
						<c:if test="${aRecord.type=='WEB'}">
							<tr class='out' onMouseOver='this.className="over"' onMouseOut='this.className="out"' id='code_${aRecord.no}' style='display:none'>
								<td class='zw-txt' colspan='6' align='left'>
									请在您的网页底部嵌入以下代码：
									<p/>&lt;script type="text/javascript" src="http://www.jianyuekefu.com:8080/js/online/visitor-access.js?pid=${attribute.pid}&channel=${aRecord.no}"&gt;&lt;/script&gt;
									<p/>&lt;script type="text/javascript" src="http://www.jianyuekefu.com:8080/plugins/layer/layer.js"&gt;&lt;/script&gt;
								<td>
							</tr>
						</c:if>
					</c:forEach>
				</table>
				
				<div id="info-pz"> 
					${link}
				</div>
			</div>
		</div>
	</html:form>
</body>
</html>
