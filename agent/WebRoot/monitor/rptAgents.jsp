<%@ page contentType="text/html; charset=UTF-8" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>坐席工作量统计</title>
	<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" /> 
	
	
	<!--请在下面增加js-->
   	<script type="text/javascript" src="../js/mycommon.jquery.js"></script>
	<script type="text/javascript" src='../plugins/jquery-x.x.js'> </script>	

</head>

<body>
		<div id="main">
			<div id="tab-top">
				<div id="lift"></div>
				<div id="pt">坐席工作量统计</div>
				<div id="right"></div>
			</div>

			<div id="table">
				<div id="ptk">
					<div id="tabtop-l"></div>
						<div id="tabtop-z"><a href="/monitor/ReporterDpAct.do?method=agents&duration=0">今天</a></div>			
						<div id="tabtop-r1"></div>		
						<div id="tabtop-z"><a href="/monitor/ReporterDpAct.do?method=agents&duration=1">昨天</a></div>	
						<div id="tabtop-r1"></div>				
						<div id="tabtop-z"><a href="/monitor/ReporterDpAct.do?method=agents&duration=7">近一周</a></div>			
						<div id="tabtop-r1"></div>			
						<div id="tabtop-z"><a href="/monitor/ReporterDpAct.do?method=agents&duration=30">近30天</a></div>			
						<div id="tabtop-r1"></div>		
						<div id="">PS：坐席工作量以坐席接入会话为准,且统计已经结束会话</div>		
				</div>
			</div>
			
			<div id="fh-flie-2"> </div>
			<div id="main-tablist">
				<table width='100%' border='0' cellpadding='0' cellspacing='0' id='showData'>
					<tr id='minpt-tab'>
						<td>坐席</td>
						<td>会话数</td>
						<td>消息数</td>
						<td>评价首次响应时长</td>
						<td>最长首次响应时长</td>
						<td>平均会话时长</td>
						<td>最长会话时长</td>
					</tr>

					<c:forEach var="aRecord" items="${result}">
						<tr class='out' onMouseOver='this.className="over"' onMouseOut='this.className="out"'>
							<td class='zw-txt'>${aRecord[0]}</td>
							<td class='zw-txt'>${aRecord[1]}</td>
							<td class='zw-txt'>${aRecord[2]}</td>
							<td class='zw-txt'>${aRecord[3]/aRecord[1]}</td>
							<td class='zw-txt'>${aRecord[4]}</td>
							<td class='zw-txt'>${aRecord[5]/aRecord[1]}</td>
							<td class='zw-txt'>${aRecord[6]}</td>
						</tr>
					</c:forEach>
				</table>
				
				<div id="info-pz"> 
					${link}
				</div>
			</div>
		</div>
</body>
</html>
