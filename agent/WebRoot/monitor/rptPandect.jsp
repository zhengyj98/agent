<%@ page contentType="text/html; charset=UTF-8" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>总览</title>
	<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />
	
	<!--请在下面增加js-->
   	<script type="text/javascript" src="../js/mycommon.jquery.js"></script>
	<script type="text/javascript" src='../plugins/jquery-x.x.js'> </script>	
	<script type="text/javascript" src='../plugins/highcharts/highcharts.js'> </script>	

	<style>
	.curve {
    	width: 100%;
	    height: 60%;
	    text-align: center;
	    margin-top: 20px;
	}
	</style>
	<script type="text/javascript">
		function createCategories(duration) {
			var categories = []; 
			if(duration == 0 || duration == 1){
				for(var i = 1; v <= 24; v++) {
					categories.push(i);
				}
			} else if(duration == 7){
				for(var i = 1; v <= 7; v++) {
					categories.push(i);
				}
			} else if(duration == 30){
			}
			return categories;
		}
		
		var categories = [];
		var crmCount = [];
		var messageCount = [];
		var sessionCount = [];
		
		<c:forEach var="aRecord" items="${result}">categories.push("${aRecord.day}");</c:forEach>
		<c:forEach var="aRecord" items="${result}">crmCount.push(${aRecord.crmCount});</c:forEach>
		<c:forEach var="aRecord" items="${result}">messageCount.push(${aRecord.messageCount});</c:forEach>
		<c:forEach var="aRecord" items="${result}">sessionCount.push(${aRecord.sessionCount});</c:forEach>
		
		$(function() {
			var series = [];
			series.push({name: '访客数', data : crmCount});
			series.push({name: '会话数', data : sessionCount});
			drawCurve("rptMessage", categories, series, "访客数/会话数");

			var series2 = [];
			series2.push({name: '消息数', data : messageCount});
			drawCurve("rptSession", categories, series2, "消息数");
			
				
			function drawCurve(div, categories, series, text0) {
				$("#" + div).highcharts({
					chart : {
						reflow : true
					},
			        title: {
			            text: '',
			        },
			        subtitle: {
			            text: '',
			        },
			        colors: ['#058DC7', '#50B432'],
			        xAxis: {
			        	tickmarkPlacement: 'on',
			            categories: categories
			        },
			        yAxis: {
			        	min : 0,
						allowDecimals : false,
			            title: {
			                text: text0
			            }
			        },
			        tooltip: {
			        	shared: true
			        },
			        legend: {
			            layout: 'vertical',
			            align: 'right',
			            verticalAlign: 'middle',
			            borderWidth: 0
			        },
			        series: series
			    });
			}			
		});
	</script>
</head>

<body>
		<div id="main">
			<div id="tab-top">
				<div id="lift"></div>
				<div id="pt">总览</div>
				<div id="right"></div>
			</div>

			<div id="table">
				<div id="ptk">
					<div id="tabtop-l"></div>
						<div id="tabtop-z"><a href="/monitor/ReporterDpAct.do?method=pandect&duration=0">今天</a></div>			
						<div id="tabtop-r1"></div>		
						<div id="tabtop-z"><a href="/monitor/ReporterDpAct.do?method=pandect&duration=1">昨天</a></div>	
						<div id="tabtop-r1"></div>				
						<div id="tabtop-z"><a href="/monitor/ReporterDpAct.do?method=pandect&duration=7">近一周</a></div>			
						<div id="tabtop-r1"></div>			
						<div id="tabtop-z"><a href="/monitor/ReporterDpAct.do?method=pandect&duration=30">近30天</a></div>			
						<div id="tabtop-r1"></div>		
						<div id="">PS：总览数据以访客请求进入为准,且统计已结束会话</div>		
				</div>
			</div>
			
			<div id="fh-flie-2"> </div>
			<div id="main-tablist">
				<div class="curve">
					<div id="rptMessage"></div>
				</div>
				<div class="curve">
					<div id="rptSession"></div>
				</div>	
			</div>			
				
			<div id="main-tablist">
				<table width='100%' border='0' cellpadding='0' cellspacing='0' id='showData'>
					<tr id='minpt-tab'>
						<td>时间</td>
						<td>访客数</td>
						<td>会话数</td>
						<td>消息数</td>
					</tr>

					<c:forEach var="aRecord" items="${result}">
						<c:if test="${aRecord.crmCount > 0 || aRecord.sessionCount > 0 || aRecord.messageCount > 0}">
							<tr class='out' onMouseOver='this.className="over"' onMouseOut='this.className="out"'>
								<td class='zw-txt' width='40%'>${aRecord.day}</td>
								<td class='zw-txt' width='20%'>${aRecord.crmCount}</td>
								<td class='zw-txt' width='20%'>${aRecord.sessionCount}</td>
								<td class='zw-txt' width='20%'>${aRecord.messageCount}</td>
							</tr>
						</c:if>
					</c:forEach>
				</table>
			</div>
		</div>
</body>
</html>
