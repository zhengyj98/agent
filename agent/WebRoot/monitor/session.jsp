<%@ page contentType="text/html; charset=UTF-8" language="java" errorPage=""%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>
<%
String filterAgent = request.getParameter("filterAgent");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>会话监控</title>
		<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />
		<!--请在下面增加js-->
		<script type="text/javascript" src='../plugins/jquery-x.x.js'> </script>
		<script type="text/javascript" src='../js/mycommon.jquery.js'> </script>
		<script type="text/javascript" src="../plugins/layer/layer.js"></script>
		<script type="text/javascript" src='../js/online/all-tip.js'> </script>
		<script type="text/javascript" src='../js/online/constant.js'> </script>
		<script type="text/javascript" src="../plugins/layer/layer.js"></script>
		<script type="text/javascript" src='../js/online/agent-common.js'> </script>
		<script>
			var counting = 0;
			function readSessions(){
				commit('../monitor/MonitorDpAct.do?method=sessions', {filterAgent : "<%=filterAgent%>"}, function(resp){
					if(resp) {
						for(var i = 0; i< resp.length ;i++) {
							var one = resp[i];
							var td = "";
							td = td + "<td>" + one.session.id.substring(15) + "</td>";
							td = td + "<td>" + (vl(one.crm) ? (one.crm.name + "[" + CRM_LEVEL[one.crm.level] + "]") : "") + "</td>";
							td = td + "<td>" + one.requestTime + "[" + one.duration +  "]</td>";
							td = td + "<td>" + SESSION_STEP_TEXT[one.session.step] + "</td>";
							td = td + "<td>" + (vl(one.session.agent) ? (one.session.agent + "[" + one.session.agentName + "]") : "") + "</td>";
							td = td + "<td>" + (vl(one.agentTime) ? (one.agentTime +  "[" + one.agentDuration + "]") : "") + "</td>";
							td = td + "<td>" + (vl(one.firstAgentResponseTime) ? (one.firstAgentResponseTime +  "[" + one.firstAgentResponseDuration + "]") : "") + "</td>";
							td = td + "<td>" + one.session.messageCount + "</td>";
							td = td + "<td>" + one.session.systemMessageCount + "</td>";
							td = td + "<td>" + one.session.agentMessageCount + "</td>";
								
							var session = $("#tr_" + one.session.id);
							if(session.length > 0) {//已经存在则更新
								session.html(td);
							} else {//不存在则增加tr
								//var tr = "<tr class='out' onMouseOver='this.className=\"over\"' onMouseOut='this.className=\"out\"' id='tr_" + one.session.id + "'>";
								var onMouseOver = 'this.className="over"';
								var onMouseOut = 'this.className="out"';
								var onClick = 'showLogs("' + one.session.id + '")';
								var tr = "<tr class='out' onMouseOver='" + onMouseOver + "' onMouseOut='" + onMouseOut + "' id='tr_" + one.session.id + "' onClick='" + onClick + "'>";
								tr = tr + td;
								tr = tr + "	</tr>";
								$("#sessions-table").append(tr);
							}
						}
						  $("tr").each(function(){
						    if(this.id.indexOf("tr_") >= 0) {
						    	var exist = false;
						    	for(var i = 0; i< resp.length ;i++) {
									var sid = "tr_" + resp[i].session.id;
									if(sid == this.id) {
										exist = true;
									}
								}
								if(exist == false) {//如果已经不存在了，就删除
									$(this).remove();
								}
						    }
						  });
					}
					counting++;
					$("#counting").html(counting);
					setTimeout(readSessions, 5000);			
				});
			}
			
			readSessions();
		</script>
	</head>

	<body>
		<div id="main">
			<div id="tab-top">
				<div id="lift"></div>
				<div id="pt">
					会话监控[<span id='counting'></span>]
				</div>
				<div id="right"></div>
			</div>

			<div id="table">
				<div id="ptk">
					<div id="tabtop-l">
					</div>
				</div>
			</div>

			<div id="fh-flie-2">
			</div>
			<div id="main-tablist">
				<table width='100%' border='0' cellpadding='0' cellspacing='0' id='sessions-table'>
					<tr id='minpt-tab'>
						<td width="12%" class="zw-txt">
							会话ID
						</td>
						<td width="10%" class="zw-txt">
							访客[等级]
						</td>
						<td width="12%" class="zw-txt">
							请求[时长]
						</td>
						<td width="5%" class="zw-txt">
							会话状态
						</td>
						<td width="12%" class="zw-txt">
							接待坐席
						</td>
						<td width="12%" class="zw-txt">
							接待时间[时长]
						</td>
						<td width="12%" class="zw-txt">
							坐席首次回复[时长]
						</td>
						<td width="7%" class="zw-txt">
							访客消息数
						</td>
						<td width="7%" class="zw-txt">
							系统消息数
						</td>
						<td width="7%" class="zw-txt">
							坐席消息数
						</td>
					</tr>
				</table>

				<div id="info-pz">
					${link}
				</div>
			</div>
		</div>
	</body>
</html>
