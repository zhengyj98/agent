<%@ page contentType="text/html; charset=gbk" language="java" import="java.sql.*" errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gbk" />
<title>权限修改</title>
<!--普通调用本目录中的js-->	
<script language="javascript" src="./js/tree/dhtmlXCommon.js"></script>
<script language="javascript" src="./js/tree/dhtmlXTree.js"></script>

<script language="javascript" src="./js/authCommon.js"></script>
<script language="javascript" src="./js/authorityModify.js"></script>
<script language="javascript" src="./js/url.js"></script>

<!--调用上一级中的js-->
<script language="javascript" src="../js/mycommon.js"></script>	
<script language="JavaScript" src="../js/ajaxCommon.js" type="text/javascript" ></script>


<!--界面颜色变化-->
<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />
<script type="text/javascript" src="../themes/js/jquery.js"></script>
	
</head>

<body>
<div id="main">
  	<div id="tab-top">
  		<div id="ptk1">
	  		<div id="lift"></div>
	  		<div id="pt">权限修改</div>
	  		<div id="right"></div>
  		</div>
 	</div>
<html:form action="/auth/AuthorityDpAct?method=doModify" onsubmit="return onsubmitCheck();" >
	<div id="table">
   		<div id="ptk">
   			<div id="tabtop-l"></div>
    		<div id="tabtop-z">权限修改</div>
    		<div id="tabtop-r1"></div>
    		<li>&nbsp;&nbsp;&nbsp;在左边选中一个结点, 再在右边修改其内容, 然后提交</li>
    	</div>
  	</div>  	
  	<div id="main-tablist">			
	
	<table width="100%">
      <tr>
        <td valign="top" width="30%"><div id="treeboxbox_tree2" style="width:350; height:218;background-color:#f5f5f5;border :1px solid Silver;; overflow:auto;"></div></td>
        <td style="padding-left:25" valign="top"> 
		  id：<input name="id" type="text" id="id" value="id" alt="请输入结点名称" size="50" style="border:0" readonly="true"/><br />
		  名称<input name="newName" type="text" id="newName" value="名称" alt="请输入结点名称" size="50"/><br />
		  描述<input name="newDesc" type="text" id="newDesc" value="描述" alt="请输入结点描述" size="50"/><br />
		  标识<input name="newIden" type="text" id="newIden" value="标识" alt="请输入结点标识符" size="50"/><br />		         
          <a href="javascript:void(0);" onClick="modifyTreeNode();">修改此子结点名称</a>&nbsp;&nbsp;<a href="javascript:void(0);" onClick="return ajaxGo('/auth/AuthorityDpAct?method=showHoldingRole');">展示此权限项对应的角色</a> : 
          <br />		
		  <logic:equal property="auth.auth.modify.value" name="root" value="true" scope="session">
			  <html:submit styleClass="input1" property="add" value="确定修改"/>			  
		  </logic:equal>
		  <html:button styleClass="ny-2-1" property="cancel" value="返回" onclick="javascript:history.back();"/>		  
          <br /><br />	
		  <div id="urlContainer" style="display:none"></div>			 
		  <br /> 
		  <input type="text" id="urlId" name="newUrl" size="54" onkeyup="listUrl(this,'./TAuthUrlDpAct.do?method=list2');" autocomplete="off" />
		  <div id="new_list" class="new_list" style="display:none"></div>
		  <logic:equal property="auth.auth.modify.value" name="root" value="true" scope="session">
			  <input type="button" class="search" name="add" value="关联" onclick="addUrl()" />
		  </logic:equal>
		</td>
	  </tr>
    </table>
    
    </div>
	<div id="forSubmitNewNodes"></div>
	
</html:form>
	<script>
			tree2=new dhtmlXTreeObject("treeboxbox_tree2","100%","100%",0);
			tree2.setImagePath("./imgs/");
			tree2.enableCheckBoxes(1);
			tree2.loadXML("authTree.xml");
			tree2.enableThreeStateCheckboxes(true);
	        tree2.setOnClickHandler(doOnClick);
	        function doOnClick(nodeId){
				var name = tree2.getItemText(nodeId);
				var desc = tree2.getUserData(nodeId,"desc");
				var iden = tree2.getUserData(nodeId,"iden");
				var urls = tree2.getUserData(nodeId,"urls");
				var urlids = tree2.getUserData(nodeId,"urlids");
				
				$$('id').value=nodeId;
				$$('newName').value=name;
				$$('newDesc').value=desc;
				$$('newIden').value=iden;				
			
				var urlContainer = $$('urlContainer');
				
				if(urls && urls.length >0){
					urlContainer.innerHTML = '关联的URL有[点击可以删除]：<br/>';				
					var urlArray = urls.split(",");
					var urlIdArray = urlids.split(",");					
					for(var i=0; i<urlArray.length; i++) {
						var str = "<div onclick=\"javascript:delUrl("+ urlIdArray[i]+",'"+ urlArray[i] + "', this);\">" + urlArray[i] + "</div>";
						urlContainer.innerHTML += str;
					}
					urlContainer.style.display='';
				} else {
					urlContainer.style.display='none';
				}
            }
	</script>
</div>
</body>
</html>
