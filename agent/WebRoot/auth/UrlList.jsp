<%@ page contentType="text/html; charset=gbk" language="java" import="java.sql.*" errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gbk" />
<title>URL列表</title>
<script language="javascript" src="../js/mycommon.js"></script>
<script language="javascript" src="./js/url2.js"></script>

<!--界面颜色变化-->
<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />
<script type="text/javascript" src="../themes/js/jquery.js"></script>

</head>

<body>
<div id="main">
  	<div id="tab-top">
  		<div id="ptk1">
	  		<div id="lift"></div>
	  		<div id="pt">URL管理</div>
	  		<div id="right"></div>
  		</div>
 	</div>
<html:form action="/auth/TAuthUrlDpAct.do?method=list">
<html:hidden property="id"/> 
	<div id="table">
   		<div id="ptk">
   			<div id="tabtop-l"></div>
    		<div id="tabtop-z">查询条件</div>
    		<div id="tabtop-r1"></div>
    	</div>
  	</div>  	
  	<div id="main-tab">
	    <div id="info">	    
			<ul id="fh-flie-bdh">
	       		<li>
	       			URL：<html:text property="actionUrl" size="80"/>
	       		</li>
	       		<li>
	       			类型：<html:select property="urlType">
			                <html:option value="all">--无限制--</html:option>
			                <html:option value="1">strut action类</html:option>
					        <html:option value="2">普通jsp</html:option>
					        <html:option value="3">特别url</html:option>
			              </html:select>
	       		</li>
	       		<li>
	       			<html:submit styleClass="search" property="search" value=" 查询 "/>
	       		</li>
	       	</ul>
		</div>
	</div>
	<div id="table">
   		<div id="ptk">
   			<div id="tabtop-l"></div>
    		<div id="tabtop-z">查询结果</div>
    		<div id="tabtop-r1"></div>
			<logic:equal property="auth.url.delete.value" name="root" value="true" scope="session">	
	    		<li><html:button styleClass="tabny-l" value="删除选中"property="deleteChecked" onclick="return deleteSelected('../auth/TAuthUrlDpAct.do?method=doDelete');"/></li>
			</logic:equal>
			<logic:equal property="auth.url.add.value" name="root" value="true" scope="session">					
    			<li><html:button  styleClass="tabny-l-1" value="添 加" property="addRecord" onclick="window.location='../auth/TAuthUrlDpAct.do?method=add'"/></li>
			</logic:equal>
			<logic:equal property="auth.url.reflect.value" name="root" value="true" scope="session">	
	    		<li><html:button  styleClass="tabny-l-1" value="反 射" property="addRecord" onclick="onClickReflect();"/></li>
			</logic:equal>
    	</div>
    </div>
    
    <div id="fh-flie-2"></div>	
	<div id="main-tablist">
  	<table width="100%" align="center" cellpadding="0" cellspacing="0" >
    <!---------------表格列表----------------------------------------->  	
	<tr id="minpt-tab">  
	    <td width="8%"><input type="checkbox" name="seleceAll" onclick="fSelectAll(this.name);" /></td>
	    <td>ActionUrl</td>
	    <td>类型</td>
	    <td width="15%">修改</td>
  	</tr>
	<logic:present name="result" scope="request">
		<logic:notEmpty name="result">
			<logic:iterate id="aRecord" name="result" scope="request">		  
			  <tr class="out" onmouseover="this.className='over'" onmouseout="this.className='out'">
				<td align="center"><input type="checkbox" name="ids" value="${aRecord.id}" /></td>
				<td align="center">${aRecord.actionUrl}</td>
				<td align="center"><html:select property="urlType" value="${aRecord.urlType}" disabled="true">
	                <html:option value="1">strut action</html:option>
			        <html:option value="2">普通jsp</html:option>
			        <html:option value="3">特别url</html:option>
	              </html:select></td>
			  <td>
			  	<logic:equal property="auth.url.modify.value" name="root" value="true" scope="session">			    
					<a href="javascript:setValues('id', '${aRecord.id}');intoAction('../auth/TAuthUrlDpAct.do?method=modify')" title="修改"><img src="../themes/images/ico/cog.gif" width="16" height="16" class="icoimg" />修改</a>&nbsp;
				</logic:equal>
				<logic:equal property="auth.url.association.value" name="root" value="true" scope="session">
				    <a href="javascript:setValues('id', '${aRecord.id}');doDisplay()" title="关联权限项"><img src="../themes/images/ico/accept.gif" width="16" height="16" class="icoimg" />关联(${aRecord.itemNum})</a>
				</logic:equal>
			  </td>
			  </tr>
			</logic:iterate>
		</logic:notEmpty>
	</logic:present>
</table>
  <div id="info-pz">
    ${link}
  </div>
</div>
</html:form>
</div>
</body>
</html>
<%@ include file="Dialog.jsp" %>
