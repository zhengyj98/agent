<%@ page contentType="text/html; charset=GBK" language="java" import="java.sql.*" errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK" />
<title>权限添加</title>
<script language="javascript" src="./js/tree/dhtmlXCommon.js"></script>
<script language="javascript" src="./js/tree/dhtmlXTree.js"></script>
<script language="javascript" src="./js/authCommon.js"></script>
<script language="javascript" src="./js/authorityAdd.js"></script>
<script language="javascript" src="../js/mycommon.js"></script>	

<!--界面颜色变化-->
<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />
<script type="text/javascript" src="../themes/js/jquery.js"></script>
	
</head>

<body>
<div id="main">
  	<div id="tab-top">
  		<div id="ptk1">
	  		<div id="lift"></div>
	  		<div id="pt">权限添加</div>
	  		<div id="right"></div>
  		</div>
 	</div>
<html:form action="/auth/AuthorityDpAct?method=doAdd" onsubmit="return onsubmitCheck();" >
	<div id="table">
   		<div id="ptk">
   			<div id="tabtop-l"></div>
    		<div id="tabtop-z">权限添加</div>
    		<div id="tabtop-r1"></div>
    		<li>&nbsp;&nbsp;&nbsp;在左边选中一个结点, 再在右边写入其内容, 然后提交</li>
    	</div>
  	</div>  	
		
	<div id="main-tablist">
	<table width="100%">
      <tr>
        <td valign="top" width="30%"><div id="treeboxbox_tree2" style="width:350; height:218;background-color:#f5f5f5;border :1px solid Silver;; overflow:auto;"></div></td>
        <td  style="padding-left:25" valign="top"> 
		  id：<input name="id" type="text" id="id" value="id" alt="请输入结点名称" size="50" style="border:0" readonly="true"/><br />		  
		  名称<input name="newName" type="text" id="newNameId" value="名称" alt="请输入结点名称" size="50"/><br />
		  描述<input name="newDesc" type="text" id="newDescId" value="描述" alt="请输入结点描述" size="50"/><br />
		  标识<input name="newIden" type="text" id="newIdenId" value="标识" alt="请输入结点标识符" size="50"/><br />
          
          <br />
		  <a href="javascript:void(0);" onClick="addTreeNode();">添加输入的新结点</a> : 		  
		  <br/>
		  <a href="javascript:void(0);" onClick="deleteTreeNode(tree2.getSelectedItemId());">删除选中的新结点</a><br>
          <br /> 
		         
	    </td>
      </tr>	  
	  <tr>
	  <td id="stateBar"></td>
	  <td>
	  <logic:equal property="auth.auth.add.value" name="root" value="true" scope="session">
		  <html:submit styleClass="input1" property="add" value="确定提交"/>
	  </logic:equal>
	  <html:button styleClass="ny-2-1" property="cancel" value="返回" onclick="javascript:history.back();"/>	  
	  </td>
	  </tr>
    </table>
	
	</div>
	
	<div id="forSubmitNewNodes"></div>
	
		
</html:form>
	<script>
			tree2=new dhtmlXTreeObject("treeboxbox_tree2","100%","100%",0);
			tree2.setImagePath("./imgs/");
			tree2.loadXML("authTree.xml");
			tree2.setOnClickHandler(doOnClick);
	        function doOnClick(nodeId){
			    var name = tree2.getItemText(nodeId);
				var desc = tree2.getUserData(nodeId,"desc");
				var iden = tree2.getUserData(nodeId,"iden");
				$$('id').value=nodeId;
				$$('newName').value=name;
				$$('newDesc').value=desc;
				$$('newIden').value=iden;				
            }
	</script>
</div>
</body>
</html>
