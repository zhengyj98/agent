<%@ page contentType="text/html; charset=gbk" language="java" import="java.sql.*" errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gbk" />
<title>url增加</title>
<script language="javascript" src="../js/mycommon.js"></script>

<!--界面颜色变化-->
<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />
<script type="text/javascript" src="../themes/js/jquery.js"></script>
	
</head>

<body>
<div id="main">
  	<div id="tab-top">
  		<div id="ptk1">
	  		<div id="lift"></div>
	  		<div id="pt">URL增加</div>
	  		<div id="right"></div>
  		</div>
 	</div>
<html:form action="/auth/TAuthUrlDpAct?method=doAdd" method="post" onsubmit="return checkNull('actionUrl','url')">
<html:hidden property="id" />
	<div id="table">
   		<div id="ptk">
   			<div id="tabtop-l"></div>
    		<div id="tabtop-z">URL增加</div>
    		<div id="tabtop-r1"></div>
    	</div>
  	</div>  	
  	<div id="main-tab">
	    <div id="info">		    
			<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
			  <tr>
				<td width="14%" class="label">url：</td>
				<td width="86%"><html:text property="actionUrl" size="80"/></td>
			  </tr>
			  <tr>
				<td class="label">类型：</td>
				<td><html:select property="urlType">
				        <html:option value="1">strut action类</html:option>
						<html:option value="2">普通jsp</html:option>
						<html:option value="3">特别url</html:option>
				    </html:select>
				</td>
			  </tr>
			</table>
			
			<div id="fh-flie-2"></div>
			<div id="main-tabk2">
				<ul>
					<logic:equal property="auth.url.adm.value" name="root" value="true" scope="session">
						<li><html:submit styleClass="input1" value="增&nbsp;&nbsp;加" property="add"/></li>
					</logic:equal>
			        <li><html:button styleClass="ny-2-1" value="返&nbsp;&nbsp;回" property="cancel" onclick="history.back();"/></li>
				</ul>
			</div>
			
		</div>
	</div>
</html:form>
</div>
</body>
</html>
