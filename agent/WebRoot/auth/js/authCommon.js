//将权限节点数组提交，必须给出一个div(containerId)，用于存放内容
function makeSubmitField(newArr,containerId){
	if(!newArr || newArr.length <=0) {
		return;
	}
	if(!containerId || !document.getElementById(containerId)) {
		alert('参数格式不符合要求,找不到容器');
		return;
	}
	var container = document.getElementById(containerId);
	for(var i=0; i<container.childNodes.length; i++) {
		container.removeChild(container.childNodes[i]);
	}
	var _table = document.createElement("table");
	for(var i=0; i<newArr.length; i++) {
		//每一个被修改的节点就是一个数组，即将生成4个input输入框
		createFieldLine(_table, newArr[i][0], newArr[i][1], newArr[i][2], newArr[i][3], newArr[i][4]);
	}
	container.appendChild(_table) ;
}
//传入参数，作为input存在
function createFieldLine(theTable){
	if(!theTable || arguments.length <=1 || arguments.length % 5 != 1 ){
		alert("参数格式不符合要求");
		return ;
	}
	for(var i=1; i<arguments.length; i+=5) {
	    currRow = theTable.insertRow();
		
		cellc=currRow.insertCell();
        cellcContext='<input type="hidden" name="newIds" value="'+arguments[i]+'" style="width:200px"/>';  			
        cellc.innerHTML=cellcContext;
        
		cellc=currRow.insertCell();
        cellcContext='<input type="hidden" name="newNames" value="'+arguments[i+1]+'" style="width:200px"/>';  			
        cellc.innerHTML=cellcContext;
        
		cellc=currRow.insertCell();
        cellcContext='<input type="hidden" name="newDescs" value="'+arguments[i+2]+'" style="width:200px"/>';  			
        cellc.innerHTML=cellcContext;
        
		cellc=currRow.insertCell();
        cellcContext='<input type="hidden" name="newIdens" value="'+arguments[i+3]+'" style="width:200px"/>';  			
        cellc.innerHTML=cellcContext;

		cellc=currRow.insertCell();
        cellcContext='<input type="hidden" name="newFathers" value="'+arguments[i+4]+'" style="width:200px"/>';  			
        cellc.innerHTML=cellcContext;
	}   
}



