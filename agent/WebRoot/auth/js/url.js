//suggest出模糊匹配的list
function listUrl(inputNode, actionUrl){
	if(!inputNode || inputNode.value.length <=4){
		$$("new_list").style.display ="none";
		return;
	}
	var paraString = "";
	if(inputNode && inputNode.value.length >0)
	    paraString = "&input="+inputNode.value;
	var http_request = createHttpRequestObjUsingPrototype();
	if (!http_request) {
		alert('创建HttpRequest对象失败！');
		return false;
	}
	//创建请求结果处理程序
	http_request.onreadystatechange = function(){
		if(4 == http_request.readyState){
			if(200 == http_request.status){
				handleSearchSuggest(http_request.responseText); //处理返回结果
			}else{
				alert('网络失败或者服务器错误!');
			}
		}
	}
	http_request.open('GET',actionUrl+paraString,false);
	http_request.setRequestHeader("If-Modified-Since","0"); 
	http_request.send(null);
}

function setSearch(value) {
	$$("new_list").style.display ="none";
	$$('urlId').value = value;
}

function handleSearchSuggest(respondResult) 
{	var strArray = respondResult.split("\n");
	var suggest = "";
	for(var i=0; i<strArray.length; i++) {
		suggest +="<li><a href='#' onclick='setSearch(this.innerHTML);'>" + strArray[i] + "</a></li>";
	}
	$$("new_list").innerHTML = suggest;
	$$("new_list").style.display='';
} 


/**
* 以下是操作权限项所关联的url数据。
* addUrl调用askForAddUrl调用后台doBind，handleAskForAddUrl处理后台返回。
* delUrl调用askForDelUrl调用后台doUnBind，handleAskForDelUrl处理后台返回。
**/
function addUrl(){
	var itemId = tree2.getSelectedItemId();
	if(!itemId.match(/I_/)){
		alert('URl暂时只支持分配到权限项结点');
		return ;
	}
	var newUrl = $$("newUrl");
	if(!newUrl && newUrl.value.length <=0){
		alert('url不能为空');
		return ;
	}
	//向服务器添加
	try{
	    askForAddUrl(itemId, newUrl, "./TAuthUrlDpAct.do?method=doBind");
	}catch(e){
		alert(e.message);
	}
}
function delUrl(urlId, urlString, theObj){
	var itemId = tree2.getSelectedItemId();

    if(confirm("确定删除 "+urlString+" 吗")){
		//向服务器添加
		try{
			askForDelUrl(itemId, urlId, urlString, "./TAuthUrlDpAct.do?method=doUnBind", theObj);
		}catch(e){
			alert(e.message);
		}
	    
	}
	doOnClick(itemId);
}

function askForAddUrl(itemId, inputNode, actionUrl){
	if(!itemId || itemId.length <=0){
		alert('请指定权限项结点ID');
		return;
	}
	if(!inputNode || inputNode.value.length <=0){
		alert('请指定要添加的URL');
		return;
	}
	
	var paraString = "";
	paraString = "&itemId="+itemId+"&input="+inputNode.value;
	var http_request = createHttpRequestObjUsingPrototype();
	if (!http_request) {
		alert('创建HttpRequest对象失败！');
		return false;
	}
	//创建请求结果处理程序
	http_request.onreadystatechange = function(){
		if(4 == http_request.readyState){
			if(200 == http_request.status){
				handleAskForAddUrl(http_request.responseText, itemId); //处理返回结果
			}else{
				alert('网络失败或者服务器错误!');
			}
		}
	}
	http_request.open('GET',actionUrl+paraString,false);
	http_request.setRequestHeader("If-Modified-Since","0"); 
	http_request.send(null);
}
function handleAskForAddUrl(respondResult, itemId) 
{
    if(!respondResult || respondResult.length <= 0){
    	throw new Error("处理异常,添加失败");
    }
    if(respondResult.indexOf("SUCCESS") >= 0){		
	    //本地显示
		var newUrl = $$("newUrl");		
	    var oldUrlsInUserData = tree2.getUserData(itemId,"urls");
		var oldUrlIdsInUserData = tree2.getUserData(itemId,"urlids");
	    if(oldUrlsInUserData && oldUrlsInUserData.length > 0){
		    tree2.setUserData(itemId,"urls",oldUrlsInUserData + urlSplitString + newUrl.value);
			tree2.setUserData(itemId,"urlids",oldUrlIdsInUserData + urlSplitString + "-1");
    	}else{
		    tree2.setUserData(itemId,"urls",newUrl.value);
			tree2.setUserData(itemId,"urlids","-1");
	    }		
		doOnClick(itemId);
    	alert('添加成功');		
    }else{
    	alert(respondResult);
    }
} 


function askForDelUrl(itemId, urlId, urlString, actionUrl, theObj){

	if(!itemId || itemId.length <=0){
		alert('请指定权限项结点ID');
		return;
	}
	var paraString = "&itemId="+itemId+"&urlId="+urlId;
	var http_request = createHttpRequestObjUsingPrototype();
	if (!http_request) {
		alert('创建HttpRequest对象失败！');
		return false;
	}
	//创建请求结果处理程序
	http_request.onreadystatechange = function(){
		if(4 == http_request.readyState){
			if(200 == http_request.status){
				handleAskForDelUrl(http_request.responseText, itemId, urlString, theObj); //处理返回结果
			}else{
				alert('网络失败或者服务器错误!');
			}
		}
	}
	http_request.open('GET',actionUrl+paraString,false);
	http_request.setRequestHeader("If-Modified-Since","0"); 
	http_request.send(null);
}
function handleAskForDelUrl(respondResult, itemId, urlString, theObj) 
{
    if(!respondResult || respondResult.length <= 0){
    	throw new Error("处理异常,删除失败");
    }
	//alert(theObj + "1");
    if(respondResult.indexOf("SUCCESS") >= 0){
		//alert(theObj + "2");
	    //本地显示
		var urls = tree2.getUserData(itemId, "urls");
		//alert(theObj + "3");
		if(urls && urls.length > 0){
			var newUrls = "";
			var urlArray = urls.split(urlSplitString);
			for(var i=0; i<urlArray.length; i++) {
				if(urlArray[i] != urlString){
					if(newUrls.length >0)
						newUrls += urlSplitString;
					newUrls += urlArray[i];
				}
			}
			tree2.setUserData(itemId,"urls", newUrls);
		}	
		doOnClick(itemId);
    }else{
    	alert('删除失败'+respondResult);
		return false;
    }
} 
