function onsubmitCheck(){
	var idsStr = tree2.getAllChecked();
	if(idsStr == "" || !idsStr.match("I_")) {
		alert("没有任何权限的角色是无效的，请选择权限项");
		return false;
	} else {
		if(checkNull('roleName','角色名称','roleDesc','角色描述','roleType','角色类型') == false){
			return false;
		} else {
			$$("ids").value = idsStr;
			if(confirm("确定要提交操作吗?")){
				$$("dialogWindow").style.display = '';
				return true;
			}
		}
	}
	return false;
}	
