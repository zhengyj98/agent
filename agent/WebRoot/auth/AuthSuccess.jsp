<%@ page contentType="text/html; charset=gbk" language="java" import="java.sql.*" errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gbk" />
<title>权限操作成功</title>


<!--界面颜色变化-->
<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />
<script type="text/javascript" src="../themes/js/jquery.js"></script>

	
</head>
<body>
 <div id="main">
  
  <div id="tab-top">
  <div id="ptk1">
  <div id="lift"></div>
  <div id="pt">操作状态</div>
  <div id="right"></div></div>
  </div>
  
    <div id="table">
   <div id="ptk"> <div id="tabtop-l"> </div>
    <div id="tabtop-z">操作执行状态</div>
    <div id="tabtop-r1"></div></div>
  </div>
  
  <div id="main-tab">
   <div  id="center">
   	
   <div  id="edit-ok-l">
   <li id="edit-ok-txt"><img src="../themes/images/images/logo-hs.gif" alt="" width="113" height="52" /></li>
      </div>
      
       <div  id="edit-ok-r">
   <li id="edit-ok-txt"><img src="../themes/images/images/114-hs.gif" alt="" width="110" height="20" /></li>
   <li id="edit-ok-txt">您的操作成功</li>
   <li id="edit-ok-txt">
   	<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
      	<td height="24" colspan="2">${result}	</td>
	    </tr>
      <tr>
        <td width="73" height="24">
			<html:link action="/common/Common.do?method=goback&&action=goback&pgUri=${pgUri}">返 回 </html:link>
		</td>
        <td width="327">
			<logic:equal property="auth.auth.display.value" name="root" value="true" scope="session">
				<html:link action="/auth/AuthorityDpAct?method=display">查看权限状态</html:link>
			</logic:equal>
		</td>
      </tr>	  
     </table>
   </li>
   </ul>
      </div>
       
    </div>
  </div>
    

</div>
  </body>
  
  
  


</html>
