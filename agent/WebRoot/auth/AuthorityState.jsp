<%@ page contentType="text/html; charset=gbk" language="java" import="java.sql.*" errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gbk" />
<title>权限操作成功</title>


<!--界面颜色变化-->
<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />
<script type="text/javascript" src="../themes/js/jquery.js"></script>

	
</head>
<body>
<div id="main">
  
  <div id="tab-top">
  <div id="ptk1">
  <div id="lift"></div>
  <div id="pt">权限状态</div>
  <div id="right"></div></div>
  </div>
  
    <div id="table">
   <div id="ptk"> <div id="tabtop-l"> </div>
    <div id="tabtop-z">权限状态</div>
    <div id="tabtop-r1"></div></div>
  </div>
  
  <div id="main-tab">
  <logic:equal property="auth.auth.reInit.value" name="root" value="true" scope="session">
  	<html:button property="reInit" value="点击可更新状态属性， 当你增加、删除、修改了角色时候才需要点击" onclick="window.location='../auth/AuthorityDpAct.do?method=reInit'"/>
	<br>	<br>
  </logic:equal>
  <br>
  ${result}
  </div>
    

</div>
</body>
</html>
