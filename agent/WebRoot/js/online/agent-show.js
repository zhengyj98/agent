
function showSetting(setting) {
	$("#serviceStatus").val(setting.loginDefaultStatus);
	if(setting.loginDefaultStatus == SERVICE_STATUS.ONLINE) {
		$("#currentServiceStatus").removeClass("offline-icons");
	}
	$("#maxChat").val(setting.chatMaxLimit);
	if (setting.chatMaxType == CHAT_MAX_TYPE.MANAGER_DEFINED) {
		$("#maxChat").attr("disabled", true);
	}
}
function showLeftDiv(v) {
	$(".tab-left-div").hide();
	$("#tab-left-div-" + v).show();
}
function showRightDiv(v, url) {
	$(".tab-right-div").hide();
	$("#tab-right-div-" + v).show();
	if(v == '5'){//显示工单列表
		showWorders();
	} else if(v == 'x'){
		if(currentVisitorId) {
			var visitor = talkingVisitor[currentVisitorId];
			url = url.replace("{{crm.id}}", visitor.object.crm.id);
			url = url.replace("{{crm.tid}}", visitor.object.crm.tid);
			url = url.replace("{{crm.name}}", visitor.object.crm.name);
			url = url.replace("{{crm.phone}}", visitor.object.crm.phone);
			url = url.replace("{{session.id}}", visitor.object.session.id);
			
			url = url.replace("{{agent.name}}", agentSelfInfo.name);
			url = url.replace("{{agent.phone}}", agentSelfInfo.phone);
			url = url.replace("{{agent.username}}", agentSelfInfo.username);
			
			var timestamp = new Date().getTime();
			var token = md5(timestamp + setting.accessKey);
			var b = "timestamp=" + timestamp + "&token=" + token;
			url = url + (url.indexOf("?") > 0 ? "&" : "?") + b;
		}
		$("#innerFrame").attr("src", url);
	}
}
function toggle(id) {
	$("#Span-" + id).toggleClass("fold");
	$("#" + id).toggle();
}
function initShow() {
	$("#crm-avatar").attr("src", SETTING_FILE_PATH + "visitor.png");
	$("#msgWrapper,  .tab-right-div, .tab-left-div").niceScroll({cursorborder:"", cursorcolor:"#bebebe", boxzoom:true, cursorwidth:"8px", dblclickzoom:false, autohidemode:false, cursorborderradius:"4px"});
}
function showAgent() {
}
function showGroupsAndMembers(allGroup) {
	//<div class="second-level">
	//   <span id="Span-talking" class="click-icon fold" onclick="toggle('talking')"></span><span onclick="toggle('talking')" class="hand">A组</span><span id="talkingCount">2</span>
	//   <div id="talking" style="display:none">
	//      <ul>
	//        <li id="user_kodmj19004434" class="talking-li">
	//          <b id="img_kodmj19004434" class="web-offline"></b>
	//          <span class="session-visitor-name">郑余杰有限公司</span>
	//          <span class="session-visitor-name session-visitor-channel">网页#uytu</span>
	//        </li>
	//      </ul> 
	//    </div>
	//</div>
	for (var one in allGroup) { // 遍历Array
		var group = allGroup[one].group;
		var members = allGroup[one].member;
		var groupDIV = "G_" + group.id + "_DIV";
		var groupUL = "G_" + group.id + "_UL";
		//不存在组，则创建
		if ($("#" + groupDIV).length === 0) {
			var secondLevel = "";
			secondLevel = secondLevel + "<div class=\"second-level\">";
			secondLevel = secondLevel + "   <span id=\"Span-" + groupDIV + "\" class=\"click-icon fold\" onclick=\"toggle('" + groupDIV + "')\"></span><span onclick=\"toggle('" + groupDIV + "')\" class=\"hand\">" + group.name + "</span><span>[" + members.length + "]</span>";
			secondLevel = secondLevel + "   <div id=\"" + groupDIV + "\" style=\"display:none\"> ";
			secondLevel = secondLevel + "      <ul id=\"" + groupUL + "\">";
			secondLevel = secondLevel + "      </ul>";
			secondLevel = secondLevel + "   </div>";
			secondLevel = secondLevel + "</div>";
			$("#myTeam").append(secondLevel);
		}
		for (var v = 0; v < members.length; v++) {
			var member = members[v];
			showTeamMember(member, groupUL);
		}
	}
}
function showTeamMember(member, groupUL) {
	var memberLi = "";
	memberLi = memberLi + "<li id=\"M_" + member.username + "_LI\" class=\"talking-li\">";
	memberLi = memberLi + "\t<b class=\"web-offline b_" + member.username + "\"></b>";
	memberLi = memberLi + "\t<span class=\"session-visitor-name\">" + member.name + "[" + member.nickName + "]</span>";
	memberLi = memberLi + "\t<span class=\"session-visitor-name team-member-channel\">" + member.phone + "</span>";
	memberLi = memberLi + "\t<span class=\"session-visitor-name team-member-chat c_" + member.username + "\">--</span>";
	memberLi = memberLi + "</li>";
	$("#" + groupUL).append(memberLi);
}
function showLiAsGroup(liId) {
	$("#" + liId + " > .msg-group-chat").show();
}
function showLi(biz, start) {
	var crm = biz.object.crm;
	var group = biz.object.group;
	var channel = biz.object.channel;
	var session = biz.object.session;
	var status = "";
	if (start == "Q" || start == "W") {//排队或者等待协助
		status = "online";
	} else {
		status = "offline";
	}
	var liId = start + "_" + crm.id + "_LI";
	if("L" == start || "M" == start) {//留言
		liId = start + "_" + crm.id + "_" + biz.sid + "_LI";
	}
	if ($("#" + liId).length == 0) {
		var memberLi = "";
		memberLi = memberLi + "<li id=\"" + liId + "\" class=\"talking-li\">";
		memberLi = memberLi + "\t<b class=\"web-" + status + "\"></b>";
		memberLi = memberLi + "\t<span class='msg-group-chat' style='display:none'>&nbsp;</span>";
		memberLi = memberLi + "\t<span class=\"session-visitor-name\" id=\"" + liId + "_name" + "\">" + crm.name + "[" + CRM_LEVEL[crm.level] + "]</span>";
		memberLi = memberLi + "\t<span class=\"session-visitor-name session-visitor-channel\">" + (group ? group.name : "--") + "/" + channel + "</span>";
		memberLi = memberLi + "\t<i class=\"badge2 badge2_flow\">N</i>";
		memberLi = memberLi + "</li>";
	}
	if (start == "Q") {
		$("#queueingUL").append(memberLi);
	} else if (start == "T") {
		$("#talkingUL").append(memberLi);
	} else if (start == "H") {
		$("#historyUL").append(memberLi);
	} else if (start == "M") {//未处理
		$("#noHandledUL").append(memberLi);
	} else if (start == "L") {//已处理
		$("#handledUL").append(memberLi);
	} else if (start == "L") {//已处理
		$("#handledUL").append(memberLi);
	} else if (start == "W") {//带协助
		$("#waitingUL").append(memberLi);
	}
	if(session && session.type == 'G') {//显示群聊
		showLiAsGroup(liId);
	}
}
function removeWaitingVisitor(crm) {
	removeLi(crm, "W");
}
function removeQueueVisitor(crm) {
	removeLi(crm, "Q");
}
function removeTalkingVisitor(crm) {
	removeLi(crm, "T");
}
function removeLi(crm, start) {
	getLi(crm.id, start).remove();
}
function getLi(cid, start) {
	return $("#" + start + "_" + cid + "_LI");
}
function showTalkingUnreadCount(cid) {
	if (talkingVisitor[cid].isNew == true) {
		var badge2 = getLi(cid, "T").find(".badge2");
		badge2.html("N");
		badge2.show();
	} else {
		if (talkingVisitor[cid].unreadCount == 0) {
			getLi(cid, "T").find(".badge2").hide();
		} else {
			var badge2 = getLi(cid, "T").find(".badge2");
			badge2.html(talkingVisitor[cid].unreadCount);
			badge2.show();
		}
	}
}
function showMyVisitorUnreadCount() {
	var count = 0;
	for (var one in talkingVisitor) {
		//存在消息则按照消息计算
		if (vl(talkingVisitor[one].unreadCount) && talkingVisitor[one].unreadCount > 0) {
			count = count + talkingVisitor[one].unreadCount;
		} else {
			if (talkingVisitor[one].isNew == true) {//新用户进入则按照一个消息计算
				count = count + 1;
			}
		}
	}
	if (count == 0) {
		$("#myVisitorUnreadCount").hide();
	} else {
		$("#myVisitorUnreadCount").html(count);
		$("#myVisitorUnreadCount").show();
	}
}
function showSessionUnreadCount() {
	//在会话标签，则不需要提示了
	if ($("#tab-left-div-0").is(":visible")) {
		$("#sessionUnreadCount").hide();
		return;
	}
	var count = 0;
	for (var one in talkingVisitor) {
		//存在消息则按照消息计算
		if (vl(talkingVisitor[one].unreadCount) && talkingVisitor[one].unreadCount > 0) {
			count = count + talkingVisitor[one].unreadCount;
		} else {
			if (talkingVisitor[one].isNew == true) {//新用户进入则按照一个消息计算
				count = count + 1;
			}
		}
	}
	if (count == 0) {
		$("#sessionUnreadCount").hide();
	} else {
		$("#sessionUnreadCount").html(count);
		$("#sessionUnreadCount").show();
	}
}
function showQueueVisitor(biz) {
	showLi(biz, "Q");
}
function showTalkingVisitor(biz) {
	showLi(biz, "T");
}
function showWaitingVisitor(biz) {
	showLi(biz, "W");
}
function showHistoryVisitor(biz) {
	showLi(biz, "H");
}

function showLwNoHandled(biz) {
	showLi(biz, "M");
}

function showLwHandled(biz) {
	showLi(biz, "L");
}

function changeTheVisitorStatus(visitor) {
	var crm = visitor.object.crm;
	var group = visitor.object.group;
	var channel = visitor.object.channel;
	var visitorStatus = visitor.visitorStatus;
	if (visitorStatus == MEMBER_STATUS.ONLINE) {
		$("#T_" + crm.id + "_LI b").attr("class", "web-online");
	} else {
		$("#T_" + crm.id + "_LI b").attr("class", "web-offline");
	}
	showLayer(crm.name + AGENT_TEXT[visitorStatus + "_TIP"]);
	//如果是当前正在聊天的访客才需要提醒以及变更显示
	if (currentVisitorId == visitor.cid) {
		showButtons(talkingVisitor[visitor.cid], "T");
	}
}
function showButtons(theVisitor, start, lid, liId) {
	if (start == "") {
		$("#btnSend").hide();
		$("#btnEnd").hide();
		$("#btnReceive").hide();
		return;
	} else if("L" == start || "M" == start) {//留言信息
		if("L" == start) {//历史消息不传递这个参数，将不出现‘处理’的按钮
			liId = "";
		}
		blockInput("<a href='javascript:getLw(\"" + liId + "\", \"" + lid + " \")'>" + TEXT.LW_DETAIL + "</a>");
		return ;
	} else if("W" == start) {//等待接入的访客
		var  waiting = waitingVisitor[theVisitor.cid];
		var text = "[" + waiting.object.agent.name + "]";
		if(waiting.object.type == MESSAGE_EVENT.AG_TRANSFER2_AGENT) {
			text = text + AGENT_TEXT.WAITING_TRANSFER;
		} else {
			text = text + AGENT_TEXT.WAITING_HANDLE;
		}
		text = text + "  <a href='javascript:acceptInvite(\"" + theVisitor.cid + "\")'>" + AGENT_TEXT.ACCEPT + "</a>";	
		text = text + "  <a href='javascript:rejectInvite(\"" + theVisitor.cid + "\")'>" + AGENT_TEXT.REJECT + "</a>";			
		blockInput(text);
		return ;
	}
	
	unBlockInput();
	if (theVisitor.visitorStatus == MEMBER_STATUS.ONLINE) {
		$("#btnSend").show();
		$("#btnEnd").show();
		$("#btnReceive").hide();
	} else if (theVisitor.visitorStatus == MEMBER_STATUS.OFFLINE) {
		$("#btnSend").show();
		$("#btnEnd").show();
		$("#btnReceive").hide();
	} else if (theVisitor.visitorStatus == MEMBER_STATUS.END) {
		$("#btnSend").hide();
		$("#btnEnd").show();
		$("#btnReceive").hide();
	} else if (theVisitor.visitorStatus == MEMBER_STATUS.QUEUE) {
		$("#btnSend").hide();
		$("#btnEnd").hide();
		$("#btnReceive").show();
	} else if (theVisitor.visitorStatus == MEMBER_STATUS.HISTORY) {
		$("#btnSend").hide();
		$("#btnEnd").hide();
		$("#btnReceive").hide();
		blockInput(AGENT_TEXT.HISTORY);
	} else {
		$("#btnSend").hide();
		$("#btnEnd").show();
		$("#btnReceive").hide();
	}
}
function showTalkingDiv() {
	$("#talking").show();
}


function showWorders() {
	layer.open({
		type : 2,
		closeBtn : 1,
		title : "工单列表",
		content : "../biz/TWorderDpAct.do?method=list",
		move : false,
		maxmin : true,
		area : [ '90%', '90%' ],
		shadeClose : true,
		cancel: function(){ 
		    //右上角关闭回调
		    layer.closeAll();
		}
	});
	return false;
}

function showLws() {
	layer.open({
		type : 2,
		closeBtn : 1,
		title : "留言列表",
		content : "../biz/TLwDpAct.do?method=list",
		move : false,
		maxmin : true,
		area : [ '90%', '90%' ],
		shadeClose : true,
		cancel: function(){ 
		    //右上角关闭回调
		    layer.closeAll();
		}
	});
	return false;
}


function showAgsessions() {
	layer.open({
		type : 2,
		closeBtn : 1,
		title : "坐席会话列表",
		content : "../biz/TAgentSessionDpAct.do?method=list",
		move : false,
		maxmin : true,
		area : [ '90%', '90%' ],
		shadeClose : true,
		cancel: function(){ 
		    //右上角关闭回调
		    layer.closeAll();
		}
	});
	return false;
}

function toInviteAgents(type) {
	var title = "gchat" == type ? "加坐席群聊" : "转接会话";
	if(!currentVisitorId) {
		showLayer(AGENT_TEXT.DO_SEND_ONLY_TALKING);
		return false;
	}
	layer.open({
		type : 2,
		closeBtn : 0,
		title : title,
		content : "../online/invite.html?type=" + type + "&sid=" + talkingVisitor[currentVisitorId].object.session.id,
		move : false,
		area : [ "450px", "410px" ],
		shadeClose : true,
		cancel: function(){ 
		    layer.closeAll();
		}
	});
}