
function createSatisfyDiv() {
	var v = "";
	v = v + '<div class="satisfy" id="satisfy">';
	v = v + '  <p>' + setting.satisfyInviteTip + '</p>';
	if(setting.satisfyLimit >=1) {
		v = v + '  <div class="satisfy-item hand" id="satisfyDiv1" onclick="satisfyChoose(1)">';
		v = v + '    <span>' + TEXT.SATISFY_LEVEL_1 + '</span>';
		v = v + '  </div>';
	}
	if(setting.satisfyLimit >=2) {
		v = v + '  <div class="satisfy-item hand" id="satisfyDiv2" onclick="satisfyChoose(2)">';
		v = v + '    <span>' + TEXT.SATISFY_LEVEL_2 + '</span>';
		v = v + '  </div>';
	}
	if(setting.satisfyLimit >=3) {
		v = v + '  <div class="satisfy-item hand" id="satisfyDiv3" onclick="satisfyChoose(3)">';
		v = v + '    <span>' + TEXT.SATISFY_LEVEL_3 + '</span>';
		v = v + '  </div>';
	}
	if(setting.satisfyLimit >=4) {
		v = v + '  <div class="satisfy-item hand" id="satisfyDiv4" onclick="satisfyChoose(4)"> ';
		v = v + '    <span>' + TEXT.SATISFY_LEVEL_4 + '</span>';
		v = v + '  </div>';
	}
	if(setting.satisfyLimit >=5) {
		v = v + '  <div class="satisfy-item hand" id="satisfyDiv5" onclick="satisfyChoose(5)"> ';
		v = v + '    <span>' + TEXT.SATISFY_LEVEL_5 + '</span>';
		v = v + '  </div>';
	}
	
	if(setting.satisfyInput ==1) {
		v = v + '  <div class="satisfy-item">';
		v = v + '    <span>我还想说：<textarea type="text" name="satisfy-text" class="satisfy_input" id="satisfy_input"></textarea></span>';
		v = v + '  </div>';
	}
	
	v = v + '  <div class="satisfy-item">';
	v = v + '  <input class="button light-btn" type="button" onclick="satisfySubmit()" value="' + TEXT.CONFIRM + '"/>';
	v = v + '  <input class="button" type="button" onclick="closeLayerAndMaybeWindow()" value="' + TEXT.CANCEL + '"/>';
	v = v + '  </div>';
	v = v + '</div>';
	return v;
}
function showSatisfy() {
	var satisfyHtml = createSatisfyDiv();
	layer.open({type:1, title:TEXT.SATISFY_TOP, closeBtn:false, shadeClose:false, shade:0.3, move:false, area:["380px", 'auto'], content:satisfyHtml});
}
function satisfyChoose(num) {
	$("#satisfyDiv" + num).addClass("active").siblings().removeClass("active");
}
function satisfySubmit() {
	var score = 0;
	for (var i = 1; i <= 5; i++) {
		if ($("#satisfyDiv" + i).hasClass("active")) {
			score = i;
			break;
		}
	}
	if (score == 0) {
		showLayer(TEXT.SELECT_SATISFY_ITEM);
		return;
	}
	var data = {event:MESSAGE_EVENT.DO_SATISFY, value:score, value2:$('#satisfy_input').val(), sid:sessions[pid].id, pid:pid};
	send(data, function (result) {
		layer.closeAll();
		showLayer(setting.satisfyThanksTip);
		if(closingWindow == true) {
			realCloseWindow(500);
		}
	});
}
function closeLayerAndMaybeWindow() {
	layer.closeAll();
	if(closingWindow == true) {
		realCloseWindow();
	}
}

