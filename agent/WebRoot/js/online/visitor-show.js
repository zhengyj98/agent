function resetNiceScroll() {
	$("#msgWrapper,#tab-content-div-0").niceScroll({
		cursorborder: "",
		cursorcolor: "#bebebe",
		boxzoom: true,
		cursorwidth: "8px",
		dblclickzoom: false,
		autohidemode: false,
		cursorborderradius: "4px"
	});
}

function hideHeader() {
	$("#header").hide();
	$("#msgWrapper").css("top", $('#msgWrapper').offset().top- 60);
	$("#body-right").css("top", $('#body-right').offset().top - 60);
	$("#alerting").css("top", $('#alerting').offset().top - 60);
}	

function initShow(mode) {
	$("#clickNickname").html(TEXT.clickNickname);
	$("#clickLeaveword").html(TEXT.clickLeaveword);
	//小窗口模式：隐藏[常见问题DIV]
	if(windowShowType == 'S') {
		$("#chat-panel").css("right", "0px");
		$("#body-right").hide();
		$("#faqBtn").show();
	}
	if(windowOpenType == "P") {//弹框模式，就没有最小化的按钮
		$("#minChat").hide();
	} else {
		$("#minChat").show();
	}
	if(jianyueMobileClient()) {
		$("#cutBtn").hide();
		$("#textarea").hide();
		$("#forPc").remove();
		$("#talking_buttons").remove();
		$("#footer").css("height", "90px");
		$("#msgWrapper").css("bottom", "90px");
	} else {
		$("#forMobile").remove();
		$("#endBtn").remove();
	}
	resetNiceScroll();
}	

function showSetting(setting) {
	$("#agent-name").html(setting.orgName);
	$("#support-holder").html('<a href="' + setting.supportUrl + '" target="_blank">' + setting.supportHolder + '</a>' + '<img id="support-holder-guide" src="../themes/online/how.png" class="showGuide" onMouseOver="vTitleTip(\'support-holder-guide\')"/>');
	$("#agent-avatar").attr("src", SETTING_FILE_PATH + setting.orgHeader);
	$("#menu0").html(setting.firstMenuTag + '<img id="first-menu-guide" src="../themes/online/how.png" class="showGuide" onMouseOver="vMenu0Tip(\'first-menu-guide\')"/>');
	showRightButtonsPannel(setting);
	if (vl(setting.callUrl) && setting.callMethod.indexOf("orderList") >=0) {
		$("#menu1").show();
	}
	if (vl(setting.callUrl) && setting.callMethod.indexOf("viewList") >=0) {
		$("#menu2").show();
	}
}


function showSession(session) {
	showSessionStep(session.step);
}
function showSessionStep(step) {
	if(step == CLIENT_SESSION_STEP.LOGIN) {
		$("#btnSend").hide();
		$("#btnManual").hide();
		$("#btnEnd").hide();
		blockInput(TEXT.SELECT_GROUP_TITLE);
		startInterval(INTERVAL_WHEN_END);
	} else if(step == CLIENT_SESSION_STEP.SELF) {
		$("#btnSend").show();
		$("#btnManual").show();
		$("#btnEnd").show();
		$("#groupChooseDiv").hide();
		$("#agent-signature").html(setting.selfName);
		unBlockInput();
		startInterval();
	} else if(step == CLIENT_SESSION_STEP.QUEUE) {
		$("#btnSend").show();
		$("#btnManual").hide();
		$("#btnEnd").show();
		$("#groupChooseDiv").hide();
		if(setting.queueTalkingPolicy == 0) {
			var tip = TEXT.QUEUING;
			tip = tip + "<span onclick='javascript:cancelQueue()' class='clickHttp'>" + TEXT.CLICK_TO_CANCEL_QUEUE + "</span>";
			tip = tip + "<span onclick='javascript:toggleLw()' class='clickHttp'>" + TEXT.CLICK_TO_LEAVEWORD + "</span>";
			blockInput(tip);
		} else {
			unBlockInput();
		}
		startInterval();
	} else if(step == CLIENT_SESSION_STEP.AGENT) {
		$("#btnSend").show();
		$("#btnManual").hide();
		$("#btnEnd").show();
		$("#groupChooseDiv").hide();
		unBlockInput();
		startInterval();
	} else if(isEnd(step)) {//处于结束
		$("#btnSend").hide();
		$("#btnManual").hide();
		$("#btnEnd").hide();
		var tip = TEXT[step];
		tip = tip + "<span onclick='javascript:window.location.reload()' class='clickHttp'>" + TEXT.CLICK_TO_CONTINUE + "</span>";
		tip = tip + "<span onclick='javascript:showSatisfy()' class='clickHttp'>" + TEXT.CLICK_TO_SATISFY + "</span>";
		tip = tip + "<span onclick='javascript:toggleLw()' class='clickHttp'>" + TEXT.CLICK_TO_LEAVEWORD + "</span>";
		blockInput(tip);
		startInterval(INTERVAL_WHEN_END);
	} else if(step == CLIENT_SESSION_STEP.LIST_GROUP) {
		$("#btnManual").hide();
		$("#btnEnd").show();
		//blockInput(TEXT.SELECT_GROUP_TITLE);
		startInterval(INTERVAL_WHEN_END);
	}
}

function showGroupList(groupList) {	
	$("#groupChooseDiv").remove();
	var v = '';
	v = v + '<div id="groupChooseDiv">';
	if(vl(groupList.title)) {
		v = v + '	<h4>' + groupList.title + '</h4>	';
	} else {
		v = v + '	<h4>' + TEXT.SELECT_GROUP_TITLE + '</h4>	';
	}
	for(var i = 0;i < groupList.list.length; i++) {
		var one = groupList.list[i];
		v = v + '	<ul class="groupChooseUl">';
		v = v + '		<li><img src="../themes/online/group.png"><span style="cursor: pointer;" onclick="enterGroup(\'' + one.id + '\')"><a>' + one.name + '</a></span></li>';
		v = v + '	</ul>';
	}
	v = v + '</div>';
	$("#msgHolder").append(v);
}

function showQuestionList(questionList) {
	var v = '';
	v = v + '<div id="questionChooseDiv">';
	v = v + '	<h4>' + TEXT.SELECT_QUESTION + '</h4>	';
	for(var i = 0;i < questionList.length; i++) {
		var one = questionList[i];
		v = v + '	<ul class="questionChooseUl">';
		v = v + '		<li>&nbsp;<span style="cursor: pointer;" onclick="selectQuestion(\'' + one.id + '\', \'' + one.question + '\')"><a>' + (i + 1) + ' ' + one.question + '</a></span></li>';
		v = v + '	</ul>';
	}
	v = v + '</div>';
	$("#msgHolder").append(v);
}

function showContentDiv(v) {
	$("#tab-content-div-0").hide();
	$("#tab-content-div-1").hide();
	$("#tab-content-div-2").hide();
	$("#tab-content-div-" + v).show();
	if(v == 1 || v == 2) {
		var type = v == 1 ? "orderList" : "viewList";
		$("#orderFrame").attr("src", "../visitor/VisitorDpAct.do?method=orderList&type=" + type + "&pid=" + pid);
	}
}


function showRightButtonsPannel(setting) {
	if(vl(setting.buttonTag)) {
		$("#right-buttons-pannel").show();
		$("#right-buttons-tag").html(setting.buttonTag + '<img id="button-tag-guide" src="../themes/online/how.png" class="showGuide" onMouseOver="vButtonTip(\'button-tag-guide\')"/>');
	} else {
		$("#right-buttons-pannel").hide();
	}
}

function showButtonList(list) {
	for(var i = 0;i < list.length; i++) {
		var one = list[i];
		var v = '<div class="one-button"><a href="'+one.href+'" target="_blank"><img src="' + SETTING_FILE_PATH + one.img + '" alt="" title="' + one.title + '" style="border-radius:15px;"></a><br>' + one.name+ '</div>';
		$("#button-list").append(v);
	}
}

function showFaqList(messages) {
	for (var v = 0; v < messages.length; v++) {
		var message = messages[v];
		var one = "<ul><li class=\"quick-li\" onclick=\"selectQuestion('" + message.id + "', '" + message.question + "')\"><strong>" + (v + 1) + "</strong> " + message.question + "</li></ul>";
		$("#tab-content-div-0").append(one);
	}
}

function showFaqList2() {
	$(".quick-li").css("width", "95%");
	layer.open({
		type : 1,
		closeBtn : 1,
		title : TEXT.FAQ_TITLE,
		content : $("#tab-content-div-0").html(),
		move : false,
		area : [ '100%', '100%' ],
		offset : [ '60px', '0' ],
		shadeClose : true,
		cancel: function(){ 
		    //右上角关闭回调
		    layer.closeAll();
		}
	});
}

function toggleLw(){	
	$(".liuyan-body").toggle();
	if(crm.type > 0){//匿名用户的name是一个无业务意义的数字，不显示
		$("#LW_NAME").val(crm.name);
	} else {
		$("#LW_NAME").val("");
	}
	$("#LW_PHONE").val(crm.phone);
	$("#LW_EMAIL").val(crm.email);
	$("#LW_QQ").val(crm.qq);
	$("#LW_MEMO").val("");
}