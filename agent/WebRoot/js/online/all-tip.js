function vButtonTip(){
	layer.tips('如果您想修改这部分的内容，请到坐席台：<br/>左边菜单--UI管理--面板按钮', '#right-buttons-tag', {
		time:8000
	});
}
function vMenu0Tip(div){
	layer.tips('如果您想修改这部分的内容，请到坐席台：<br/>左边菜单--自助聊天--自助问答', '#' + div, {
		time:8000
	});
}
function vTitleTip(div){
	layer.tips('如果您想修改颜色主题、企业LOGO，请到坐席台：<br/>左边菜单--UI管理--UI配置', '#' + div, {
		tips: [1, '#0FA6D8'], time:8000
	});
}
function vGuideTip(div){
	layer.tips('您通过体验方式进入，所以系统会呈现问号做引导提示，正常嵌入代码使用客服系统不会呈现这些问号', '#' + div, {
		time:8000
	});
}

function vBtnMualTip(div) {
	layer.tips('转人工坐席之后访客进入排队，如果有坐席在线，则直接进入服务。<br/>[1]提供坐席服务：顶部按钮--进入坐席--进入在线客服<br/>[2]更多配置：左边菜单--参数配置--接入配置', '#' + div, {
		tips: [1], time:8000
	});
}

function showFunctionTip(div) {
	var msg = "";
	if(div == "T1") {
		msg = "当前在线客服数，是指：进入了客服服务的坐席，管理员登陆之后没有进入客服服务则不计算在内";
	} else if(div == "T2") { 
		msg = "服务中客服，是指：进入客服服务的坐席，把自己的状态设置为【服务状态】，可以正常接入新访客"; 
	} else if(div == "T3") { 
		msg = "暂停中客服数，是指：进入客服服务的坐席，把自己的状态设置为【暂停状态】，新访客将不再分配给该类坐席"; 
	} else if(div == "T4") { 
		msg = "当前会话数，是指：所有坐席手中的会话，包括访客在线"; 
	} else if(div == "T5") { 
		msg = "自助聊天中，是指：访客正在与机器人聊天"; 
	} else if(div == "T6") { 
		msg = "排队中，是指：访客请求人工服务，但未被坐席接入"; 
	} else if(div == "T7") { 
		msg = "有效聊天中，是指：与坐席正在聊天，并且访客在线<br/>坐席聊天中，是指：与坐席正在聊天，但访客已经不在线"; 
	} else if(div == "T8") { 
		msg = "其他状态，是指：登陆但未进入聊天或者聊天结束或者排队超时等"; 
	} else if(div == "A1") { 
		msg = "今日会话数，是指：今日访客请求（打开）会话窗口的次数，一个访客打开多次累计次数，无论会话是否与坐席进行过沟通聊天"; 
	} else if(div == "A2") { 
		msg = "今日访客数，是指：今日访客请求（打开）会话窗口的访客数量"; 
	} else if(div == "A3") { 
		msg = "今日服务坐席数，是指：今日分配到会话的坐席数量"; 
	} else if(div == "A4") { 
		msg = "今日坐席接待数，是指：坐席接待的会话数量"; 
	}  else if(div == "A5") { 
		msg = "今日坐席评价接待数，是指：所有坐席接待的会话数量除以坐席的数量"; 
	} 
	if(msg) {
		layer.tips(msg, '#' + div, {time:8000});
	}
}


function showDefinedTip(div, defined) {
	if(defined) {
		layer.tips(defined, '#' + div, {time:8000});
	}
}