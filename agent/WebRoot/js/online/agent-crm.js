

$.fn.serializeObject = function(){
	var o = {};
	var a = this.serializeArray();
	$.each(a, function() {
		if (o[this.name] !== undefined) {
			if (!o[this.name].push) {
				o[this.name] = [o[this.name]];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
};

function getCrm(cid) {
	get("crmInfo", {cid:cid}, true, function (resp) {
		showCrm(resp);
	});
}

function showCrm(crm) {
	//所有字段清空显示
	$(".crmfield").val("");
	for(var f in crm) {
		//重新赋值显示
		$("#crmForm #" + f).val(eval("crm." + f));
		//如果是省份，则需要读取下面的城市
		if(f == "province") {
			var p = eval("crm.province");
			loadCitryOfProvince(p);
			$("#crmForm #city").val(eval("crm.city"));
		}
	}
	$("#crm-name").html(crm.name + "[" + CRM_LEVEL[crm.level] + "]");
}

function loadCitryOfProvince(p){
	if(p == ""){
		$("#city").html("");
		return;
	}
	//加载这个省份下面的城市列表
	var citys = dsy.getCityOfProvince(p);
	var v= "";
	$.each(citys, function(indx, obj){
		v = v + '<option value="' + obj + '">' + obj + '</option>';
	});
	$("#city").html(v);
}

function modifyCrm() {
	 var data = $('#crmForm').serializeObject();
	 data.event="modifyCrm";
	 send(data, function (resp) {
		if (resp.code == 1) {
			showLayer(TEXT.DO_ERROR);
		} else {
			crm = resp;
			$("#T_" + crm.id + "_LI_name").html(crm.name + "[" + CRM_LEVEL[crm.level] + "]");
			$("#agent-name").html(crm.name + "[" + CRM_LEVEL[crm.level] + "]");
			showLayer(TEXT.DO_SUCCESS);
		}
	});
	 return false;
}

function getCrmField() {
	get("crmField", {}, true, function (resp) {
		crmField = resp;
		for(var i=0; i < resp.length; i++) {
			var one = resp[i];
			var v = '';
			if(one.ftype != "H") {
				v = v + '<div class="form-group">';
				v = v + '<label class="form-label" title="' + one.fshowName + '">';	
				v = v + '	' + one.fshowName + ":";
				v = v + '</label>';	
			}
			
			if(one.ftype == "H") {
				v = v + '<input type="hidden" name="' + one.fname + '" id="' + one.fname + '" class="crmfield" />';
			} else if(one.ftype == "I") {
				v = v + '&nbsp;<input type="text" name="' + one.fname + '" id="' + one.fname + '" class="form-input crmfield" />';
			} else if(one.ftype == "T") {
				v = v + '&nbsp;<textarea name="content"  class="form-textarea crmfield" id="' + one.fname + '"></textarea>';
			} else if(one.ftype == "S") {
				v = v + '&nbsp;<select name="' + one.fname + '" id="' + one.fname + '" class="form-select crmfield">';
				if(one.fname == "province"){
					v = v + getProvinceOptions();				
				} else {
					for(var m=0;m<one.options.length;m++) {
						v = v + '<option value="' + one.options[m].optionValue + '">' + one.options[m].optionText + '</option>';
					}
				}
				v = v + '</select>';
			} else if(one.ftype == "C") {
				v = v + '&nbsp;<input name="' + one.fname + '" id="' + one.fname + '" type="checkbox" value="" class="crmfield"/>';
				v = v + '&nbsp;<input name="' + one.fname + '" id="' + one.fname + '" type="radio" value="" class="crmfield"/>';
			} else if(one.ftype == "R") {
				v = v + '&nbsp;<input name="' + one.fname + '" id="' + one.fname + '" type="radio" value="" class="crmfield"/>';
				v = v + '&nbsp;<input name="' + one.fname + '" id="' + one.fname + '" type="radio" value="" class="crmfield"/>';
			}
			if (one.frequied == 1) {
				v = v + "*";
			}
			
			if(one.ftype != "H") {
				v = v + '</div>';
			}
			$("#tab-right-div-0").append(v);
			
			
			if (one.fmodify == 0) {
				$("#" + one.fname).attr("disabled", "true");
			}
		}
	});
}

function getCrmHistory(cid) {
	$("#tab-right-div-1").html("");
	var v = "";
	v = v + '<div class="form-group">';
	v = v + '<label class="form-line" style="width:100%">';	
	v = v + '	<strong><span class="form-line-span-long">时间</span>';
	v = v + '	<span class="form-line-span-short">渠道</span>';
	v = v + '	<span class="form-line-span-long">接待坐席</span>'
	v = v + '	<span class="form-line-span-80">结束原因</span></strong>';
	v = v + '</label><br/>';	
	v = v + '</div>';
	$("#tab-right-div-1").append(v);
	get("crmHistory", {cid : cid}, true, function (resp) {
		for(var i=0; i < resp.length; i++) {
			var one = resp[i];
			var session = one[0];
			var agsession = one[1];
			var v = '';
			v = v + '<div class="form-group">';
			v = v + '<label class="form-line" onclick="getLogBySid(\'' + session.id + '\')" style="width:100%">';	
			v = v + '	<span class="form-line-span-long">' + getLocalTime(session.requestTime /1000) + '</span>';
			v = v + '	<span class="form-line-span-short">' + session.channel + '</span>';
			v = v + '	<span class="form-line-span-long">' + agsession.agentNickname + '</span>'
			v = v + '	<span class="form-line-span-80">' + END_SESSION_REASON[session.endType] + '</span>';
			v = v + '</label><br/>';				
			v = v + '</div>';
			$("#tab-right-div-1").append(v);
		}
	});
}

function getCrmTrace(cid) {
	$("#tab-right-div-3").html("");
	var v = "";
	v = v + '<div class="form-group">';
	v = v + '<label class="form-line" style="width:100%">';	
	v = v + '	<strong><span class="form-line-span-long">时间</span>';
	v = v + '	<span>访问</span></strong>';
	v = v + '</label><br/>';	
	v = v + '</div>';
	$("#tab-right-div-3").append(v);
	get("crmTrace", {cid : cid}, true, function (resp) {
		for(var i=0; i < resp.length; i++) {
			var trace = resp[i];
			var v = '';
			v = v + '<div class="form-group">';
			v = v + '<label class="form-line" style="width:100%">';	
			v = v + '	<span class="form-line-span-long">' + getLocalTime(trace.createTime /1000) + '</span>';
			//v = v + '	<span class="form-line-span-short">' + trace.requestAgent + '</span>';
			if(trace.requestAgent == "Y") {
				v = v + '	<span class="form-line-strong"><a href="' + trace.url + '" target="_blank">' + trace.title.substring(0,15) + '</a></span>';
			} else {
				v = v + '	<span><a href="' + trace.url + '" target="_blank">' + trace.title.substring(0,18) + '</a></span>';
			}
			v = v + '</label><br/>';				
			v = v + '</div>';
			$("#tab-right-div-3").append(v);
		}
	});
}

function getCrmSession(cid) {
	$("#tab-right-div-4").html("");
	get("crmSession", {cid : cid}, true, function (resp) {
		if(resp == null || resp.code == 1) {
			showLayer(AGENT_TEXT.CRM_SESSION_ERROR);
			return;
		}
		var session = resp.session;
		var trace = resp.trace;
		var count = resp.count;
		var todayCount = resp.todayCount;
		var v = '';
		//点击留言的访客，是没有显示会话的
		if(session) {
			v = v + '<div class="form-group">';
			v = v + '<label class="form-line" style="width:100%">';	
			v = v + '	<span>' + session.id + '</span>';
			v = v + '</label><br/>';				
			v = v + '</div>';
			v = v + '<div class="form-group">';
			v = v + '<label class="form-line" style="width:100%">';	
			v = v + '	<span class="form-line-span-long">请求:</span>';
			v = v + '	<span>' + getLocalTime(session.requestTime /1000) + '</span>';
			v = v + '</label><br/>';				
			v = v + '</div>';
			v = v + '<div class="form-group">';
			v = v + '<label class="form-line" style="width:100%">';	
			v = v + '	<span class="form-line-span-long">进入排队:</span>';
			v = v + '	<span>' + getLocalTime(session.queueTime /1000) + '</span>';
			v = v + '</label><br/>';				
			v = v + '</div>';
			v = v + '<div class="form-group">';
			v = v + '<label class="form-line" style="width:100%">';	
			v = v + '	<span class="form-line-span-long">坐席接入:</span>';
			v = v + '	<span>' + getLocalTime(session.agentTime /1000) + '</span>';
			v = v + '</label><br/>';				
			v = v + '</div>';
			v = v + '<div class="form-group">';
			v = v + '<label class="form-line" style="width:100%">';	
			v = v + '	<span class="form-line-span-long">IP:</span>';
			v = v + '	<span>' + session.ip + '</span>';
			v = v + '</label><br/>';				
			v = v + '</div>';
			v = v + '<div class="form-group">';
			v = v + '<label class="form-line" style="width:100%">';	
			v = v + '	<span class="form-line-span-long">归属地:</span>';
			v = v + '	<span>' + session.province + session.city + '</span>';
			v = v + '</label><br/>';				
			v = v + '</div>';
		}
		if(trace) {
			v = v + '<div class="form-group">';
			v = v + '<label class="form-line" style="width:100%">';	
			v = v + '	<span class="form-line-span-long">接入页面:</span>';
			v = v + '	<span>' + trace.title + '</span>';
			v = v + '</label><br/>';				
			v = v + '</div>';
		}
		
		v = v + '<div class="form-group">';
		v = v + '<label class="form-line" style="width:100%">';	
		v = v + '	<span class="form-line-span-long">来访次数:</span>';
		v = v + '	<span>' + count + '</span>';
		v = v + '</label><br/>';				
		v = v + '</div>';
		v = v + '<div class="form-group">';
		v = v + '<label class="form-line" style="width:100%">';	
		v = v + '	<span class="form-line-span-long">今日次数:</span>';
		v = v + '	<span>' + todayCount + '</span>';
		v = v + '</label><br/>';				
		v = v + '</div>';
		$("#tab-right-div-4").append(v);
	});
}


function getProvinceOptions() {
	var v = "";
	v = v + '<option value="">---</option>';
	v = v + '<option value="安徽">安徽</option>';
	v = v + '<option value="北京市">北京市</option>';
	v = v + '<option value="福建">福建</option>';
	v = v + '<option value="甘肃">甘肃</option>';
	v = v + '<option value="广东">广东</option>';
	v = v + '<option value="广西">广西</option>';
	v = v + '<option value="贵州">贵州</option>';
	v = v + '<option value="海南">海南</option>';
	v = v + '<option value="河北">河北</option>';
	v = v + '<option value="河南">河南</option>';
	v = v + '<option value="黑龙江">黑龙江</option>';
	v = v + '<option value=" 湖北">湖北</option>';
	v = v + '<option value="湖南">湖南</option>';
	v = v + '<option value="吉林">吉林</option>';
	v = v + '<option value="江苏">江苏</option>';
	v = v + '<option value="江西">江西</option>';
	v = v + '<option value="辽宁">辽宁</option>';
	v = v + '<option value="内蒙古">内蒙古</option>';
	v = v + '<option value="宁夏">宁夏</option>';
	v = v + '<option value="青海">青海</option>';
	v = v + '<option value="山东">山东</option>';
	v = v + '<option value="山西">山西</option>';
	v = v + '<option value="陕西">陕西</option>';
	v = v + '<option value="上海">上海</option>';
	v = v + '<option value="四川">四川</option>';
	v = v + '<option value="天津">天津</option>';
	v = v + '<option value="西藏">西藏</option>';
	v = v + '<option value="新疆">新疆</option>';
	v = v + '<option value="云南">云南</option>';
	v = v + '<option value="浙江">浙江</option>';
	v = v + '<option value="重庆">重庆</option>';
	v = v + '<option value="台湾">台湾</option>';
	v = v + '<option value="香港">香港</option>';
	v = v + '<option value="澳门">澳门</option>';	
	return v;
}


function showCrms() {
	layer.open({
		type : 2,
		closeBtn : 1,
		title : "客户列表",
		content : "../crm/TCrmDpAct.do?method=list",
		move : false,
		maxmin : true,
		area : [ '90%', '90%' ],
		shadeClose : true,
		cancel: function(){ 
		    //右上角关闭回调
		    layer.closeAll();
		}
	});
	return false;
}