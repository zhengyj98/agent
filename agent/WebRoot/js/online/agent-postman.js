
var interval = null;
function startInterval(time) {
	if (!time) {
		time = 3000;
	}
	if (interval) {
		clearTimeout(interval);
	}
	interval = setInterval(function () {
		get();
	}, time);
}
function myClearInterval() {
	clearInterval(interval);
}
var loadedNiceScroll = false;
var success = 0;
var error = 0;
function get(event, data, async0, callback) {
	var callurl = "../agent/AgentDpAct.do?method=processor";
	//指定类型获取
	if (event && event != "") {
		callurl = callurl + "&event=" + event;
	}
	//默认情况都是异步获取
	var async = true;
	if (async0 && async0 != "") {
		async = async0;
	}
	if (!data) {
		data = {};
	}
	if (!data.id) {
		var time = new Date().getTime();
		data.id = "chat-" + agentSelfInfo.id + "-" + time;
	}
	$.ajax({type:"POST", dataType:"json", async:async, url:callurl, data:data, success:function (resp) {
		if (callback) {
			callback(resp);
			return;
		}
		
		//以下信息处理是针对BizMessage进行的
		if (resp.code == 1) {
			showLayer(TEXT.REFRESH_TO_LOAD);
			return;
		}
		for (var v = 0; v < resp.length; v++) {
			var message = resp[v];
			console.log(message);
			if (message.event == MESSAGE_EVENT.NOTIFY) {
				handlerNotify(message);
			} else {
				if (message.event == MESSAGE_EVENT.CHAT) {
					if (!vl(talkingVisitor[message.cid].unreadCount)) {
						talkingVisitor[message.cid].unreadCount = 0;
					}
					talkingVisitor[message.cid].unreadCount++;
					showTalkingUnreadCount(message.cid);
					//如果是当聊天的访客，那么就直接显示
					if (message.cid == currentVisitorId) {
						showChatMessge(message);
						toBottom();
					}
					showMyVisitorUnreadCount();
					showSessionUnreadCount();
					flashTitle();
					showNotify(TEXT.AGENT_HAVE_NEW_MESSAGE);
				}
			}
		}
		if ($("#msgWrapper")[0].scrollTop > 150 && loadedNiceScroll == false) {
			$("#msgWrapper").getNiceScroll().resize();
			loadedNiceScroll = true;
		}
		success++;
	}, error:function (data) {
		error++;
	}});
}
function send(data, callback) {
	get("", data, true, callback);
}

function getCount(uid, callback) {
	var time = new Date().getTime();
	var id = "chat-" + uid + "-" + time;
	get("count", {id : id}, false, function (resp) {
		if(resp.code==0){
			callback(resp);
		} else {
		}
	});
}