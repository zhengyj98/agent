
$(document).ready(function () {
	loadTinymce("context");
	$("#crm").select2({
		language: "zh-CN",
		ajax: {
		    url: '../crm/TCrmDpAct.do?method=list',
		    delay: 350,
		    dataType: 'json',
		    data: function (term, type) {
				return {
					term: term.term,
					from: "select2"
				};
		    },
		    processResults: function (data) {
		      return {
		        results: data
		      };
		    }
		  }
	});
	$("#toAgent").select2({
		language: "zh-CN",
		ajax: {
		    url: '../user/TUserDpAct.do?method=list',
		    delay: 350,
		    dataType: 'json',
		    data: function (term, type) {
				return {
					term: term.term,
					from: "select2"
				};
		    },
		    processResults: function (data) {
		      return {
		        results: data
		      };
		    }
		  }
	});
});