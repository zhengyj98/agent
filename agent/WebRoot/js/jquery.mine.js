$.validator.addMethod(
	"byteRangeLength", 
	function(value, element, param) {
		var length = value.length;
		for(var i = 0; i < value.length; i++){
			if(value.charCodeAt(i) > 127){
				length++;
			}
		}
		return this.optional(element) || ( length >= param[0] && length <= param[1] );    
	}, 
	//$.validator.format("{0}长度要求在{1}-{2}个字节之间")
	$.validator.format("{0}-{1}位之间")
);

$.validator.addMethod(
	"byteEqaulLength", 
	function(value, element, param) {
		var length = value.length;
		for(var i = 0; i < value.length; i++){
			if(value.charCodeAt(i) > 127){
				length++;
			}
		}
		return this.optional(element) || ( length == param[1]);    
	}, 
	//$.validator.format("{0}长度要求在{1}-{2}个字节之间")
	$.validator.format("要求{1}位")
);

$.validator.addMethod(
	"byteLessLength", 
	function(value, element, param) {
		var length = value.length;
		for(var i = 0; i < value.length; i++){
			if(value.charCodeAt(i) > 127){
				length++;
			}
		}
		return this.optional(element) || ( length < param[1]);    
	}, 
	//$.validator.format("{0}长度要求在{1}-{2}个字节之间")
	$.validator.format("要求少于{1}位")
);


// 本地语言   
jQuery.extend(jQuery.validator.messages, {   
        required: "<span style=\"color:#FF0000\">必须填写</span>",   
        remote: "请修正该字段",   
        email: "请输入正确格式的电子邮件",   
        url: "请输入合法的网址",   
        date: "请输入合法的日期",   
        dateISO: "请输入合法的日期 (ISO).",   
        number: "请输入合法的数字",   
        digits: "只能输入整数",   
        creditcard: "请输入合法的信用卡号",   
        equalTo: "请再次输入相同的值",   
        accept: "请输入拥有合法后缀名的字符串",   
        maxlength: jQuery.validator.format("长度最多是 {0} 的字符串"),   
        minlength: jQuery.validator.format("长度最少是 {0} 的字符串"),   
        rangelength: jQuery.validator.format("长度介于 {0} 和 {1} 之间的字符串"),   
        range: jQuery.validator.format("介于 {0} 和 {1} 之间的值"),   
        max: jQuery.validator.format("最大为 {0} 的值"),   
        min: jQuery.validator.format("最小为 {0} 的值")   
});