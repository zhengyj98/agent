<%@ page contentType="text/html; charset=utf-8" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>新增扩展模块</title>
	<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />    
	
	<!--请在下面增加js-->
   	<script type="text/javascript" src="../js/mycommon.js"></script>
	<script type="text/javascript" src='../plugins/jquery-x.x.js'> </script>
	<script type="text/javascript" src="../js/jquery.validate.js"></script>
	<script type="text/javascript" src="../js/jquery.mine.js"></script>
	<script type="text/javascript" src="../js/TExtend.js"></script>	
</head>

<body>
<div id="main">
	<div id="tab-top">
		<div id="ptk1">
			<div id="lift"></div>
			<div id="pt">新增扩展模块</div>
			<div id="right"></div>
		</div>
	</div>
	<div id="table">
		<div id="ptk">
			<div id="tabtop-l"></div>
			<div id="tabtop-z">输入信息</div>
			<div id="tabtop-r1"></div>
		</div>
	</div>
	<div id="main-tab">
		<html:form action="/extend/TExtendDpAct?method=doAdd" styleId="TExtendForm" >
			<p align="center">${result}</p>		
			<table width="911" align="center" class="table-slyle-hs" >		
				<tr>
					<td>模块名：</td>
					<td><html:text property='name' styleClass='s-input' maxlength='40'/>*</td>
				</tr>
				<tr>
					<td>位置：</td>
					<td><myjsp:select name='type' added='class="s-select"' id='type' listname='typelist' value='' readonly='' showonly='' />*如果选择在线模块，请控制界面在350px宽度</td>
				</tr>
				<tr>
					<td>连接：</td>
					<td>
					<html:textarea property='url' styleId="url" styleClass='s-input' style="height:40px; width:400px"/>*
					<br/>您可以在url后面增加这些参数以便进行更多处理：
					<select id="param" style="width:250px;height:30px">
						<option value="">选择参数然后插入到后缀</option>
						<option value="{{crm.id}}">简约ID：{{crm.id}}</option>
						<option value="{{crm.tid}}">TID：{{crm.tid}}</option>
						<option value="{{crm.name}}">用户名称：{{crm.name}}</option>
						<option value="{{crm.phone}}">用户电话号码:{{crm.phone}}</option>
						<option value="{{session.id}}">会话id:{{session.id}}</option>
						<option value="{{agent.id}}">坐席id:{{agent.id}}</option>
						<option value="{{agent.name}}">坐席名称:{{agent.name}}</option>
					</select><span onclick="addParam()" style="cursor: pointer">插入参数</span>
					<br/>url还自动携带两个系统参数以便进行鉴权：(1)timestamp是一个时间戳(2)token是MD5(timestamp+accessKey)
					</td>
				</tr>
	
				<tr>
					<td align="right"><input type="submit" class="search-2" value="新增"/></td>
					<td><input type="button" class="search-2" value="返回" onclick="window.location='../common/Common.do?method=goback&&action=goback&pgUri=${pgUri}'"/></td>
				</tr>
			</table>
		</html:form>
	</div>

</div>
</body>
</html>
