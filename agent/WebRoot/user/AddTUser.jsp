<%@ page contentType="text/html; charset=UTF-8" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>新增账号</title>
	<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />
	
	
	<!--请在下面增加js-->
   	<script type="text/javascript" src="../js/mycommon.js"></script>
	<script type="text/javascript" src='../plugins/jquery-x.x.js'> </script>
	<script type="text/javascript" src="../js/jquery.validate.js"></script>
	<script type="text/javascript" src="../js/jquery.mine.js"></script>
	<script type="text/javascript" src="../js/TUser.js"></script>	
</head>

<body>
<div id="main">
	<div id="tab-top">
		<div id="ptk1">
			<div id="lift"></div>
			<div id="pt">新增账号</div>
			<div id="right"></div>
		</div>
	</div>
	<div id="table">
		<div id="ptk">
			<div id="tabtop-l"></div>
			<div id="tabtop-z">输入信息</div>
			<div id="tabtop-r1"></div>
		</div>
	</div>
	<div id="main-tab">
		<html:form action="/user/TUserDpAct?method=doAdd" styleId="TUserForm" >
			<p align="center">${result}</p>		
			<table width="911" align="center" class="table-slyle-hs" >		
<tr>
<td>工号：</td>
<td><html:text property='no' styleClass='s-input' maxlength='5'/>*</td>
</tr>
<tr>
<td>类型：</td>
<td><myjsp:select name='type' added='class="s-select"' id='type' listname='typelist' value='' readonly='' showonly='' />*</td>
</tr>
<tr>
<td>电话号码：</td>
<td><html:text property='phone' styleClass='s-input' maxlength='30' onblur="synchUsername(this.value)"/>[建议采用电话号码作为用户名]*</td>
</tr>
<tr>
<td>用户名：</td>
<td><html:text property='username' styleId="username" styleClass='s-input' maxlength='30'/>*</td>
</tr>
<tr>
<td>密码：</td>
<td><html:text property='password' styleClass='s-input' maxlength='30'/>*</td>
</tr>
<tr>
<td>状态：</td>
<td><myjsp:select name='status' added='class="s-select"' id='status' listname='statuslist' value='' readonly='' showonly='' />*</td>
</tr>
<tr>
<td>真实姓名：</td>
<td><html:text property='name' styleClass='s-input' maxlength='30'/>*</td>
</tr>
<tr>
<td>昵称：</td>
<td><html:text property='nickName' styleClass='s-input' maxlength='30'/></td>
</tr>
<tr>
<td>EMAIL：</td>
<td><html:text property='email' styleClass='s-input' maxlength='30'/></td>
</tr>
<td>等级：</td>
<td><html:text property='level' styleClass='s-input' maxlength='30' value="1"/></td>
</tr>

				<tr>
				<td align="right"><input type="submit" class="search-2" value="新增"/></td>
				<td><input type="button" class="search-2" value="返回" onclick="window.location='../common/Common.do?method=goback&&action=goback&pgUri=${pgUri}'"/></td>
				</tr>
			</table>
		</html:form>
	</div>

</div>
</body>
</html>
