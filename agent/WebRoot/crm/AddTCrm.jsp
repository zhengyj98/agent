<%@ page contentType="text/html; charset=UTF-8" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>新增客户</title>
	<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />    
	
	<!--请在下面增加js-->
   	<script type="text/javascript" src="../js/mycommon.js"></script>
	<script type="text/javascript" src="../plugins/datepicker/WdatePicker.js"></script>
	<script type="text/javascript" src='../plugins/jquery-x.x.js'> </script>
	<script type="text/javascript" src="../js/jquery.validate.js"></script>
	<script type="text/javascript" src="../js/jquery.mine.js"></script>
	<script type="text/javascript" src="../js/TCrm.js"></script>	
</head>

<body>
<div id="main">
	<div id="tab-top">
		<div id="ptk1">
			<div id="lift"></div>
			<div id="pt">新增客户</div>
			<div id="right"></div>
		</div>
	</div>
	<div id="table">
		<div id="ptk">
			<div id="tabtop-l"></div>
			<div id="tabtop-z">输入信息</div>
			<div id="tabtop-r1"></div>
		</div>
	</div>
	<div id="main-tab">
		<html:form action="/crm/TCrmDpAct?method=doAdd" styleId="TCrmForm" >
			<p align="center">${result}</p>		
			<table width="911" align="center" class="table-slyle-hs" >		
<tr>
<td>姓名：</td>
<td><html:text property='name' styleId='name' styleClass="s-input"/><div id='nameDiv'/></td>
</tr>
<tr>
<td>电话：</td>
<td><html:text property='phone' styleId='phone' styleClass="s-input"/><div id='phoneDiv'/></td>
</tr>
<tr>
<td>邮箱：</td>
<td><html:text property='email' styleId='email' styleClass="s-input"/><div id='emailDiv'/></td>
</tr>
<tr>
<td>QQ：</td>
<td><html:text property='qq' styleId='qq' styleClass="s-input"/><div id='qqDiv'/></td>
</tr>
<tr>
<td>等级：</td>
<td><myjsp:select name='level' added='class="s-select"' id='level' listname='levellist' value='' readonly='' showonly='' /></td>
</tr>
<tr>
<td>状态：</td>
<td><myjsp:select name='status' added='class="s-select"' id='status' listname='statuslist' value='' readonly='' showonly='' /></td>
</tr>
<tr>
<td>性别：</td>
<td><myjsp:select name='sex' added='class="s-select"' id='sex' listname='sexlist' value='' readonly='' showonly='' /></td>
</tr>
<tr>
<td>企业：</td>
<td><html:text property='company' styleId='company' styleClass="s-input"/></td>
</tr>
<tr>
<td>省份：</td>
<td><html:text property='province' styleId='province' styleClass="s-input"/></td>
</tr>
<tr>
<td>城市：</td>
<td><html:text property='city' styleId='city' styleClass="s-input"/></td>
</tr>
<tr>
<td>备注：</td>
<td><html:textarea property='memo' readonly='' styleId='memo' onkeyup="areaTextMaxLength('memo', 'memoDiv', 50)" rows='3' cols='50'/><div id='memoDiv'/></td>
</tr>
<tr>
<td>TID：</td>
<td><html:text property='tid' styleId='tid' styleClass="s-input"/></td>
</tr>

				<tr>
				<td align="right"><input type="submit" class="search-2" value="新增"/></td>
				<td><input type="button" class="search-2" value="返回" onclick="window.location='../common/Common.do?method=goback&&action=goback&pgUri=${pgUri}'"/></td>
				</tr>
			</table>
		</html:form>
	</div>

</div>
</body>
</html>
