<%@ page language="java" contentType="text/html;charset=UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>我的坐席端</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">
		<link rel="stylesheet" type="text/css" href="../themes/online/big.css" />
		<link rel="stylesheet" type="text/css" href="../themes/online/agent.css" />
		<link rel="stylesheet" type="text/css" href="../plugins/wechat-emotion/jquery-wechatEmotion-2.1.0.css" />
		<link rel="shortcut icon" type="image/x-icon" href="../file/load.do?method=get&bizType=SETTING&fileName=${orgHeader}" />

		<script type="text/javascript" src="../js/mycommon.jquery.js"></script>
		<script type="text/javascript" src="../js/my.upload.js"></script>
		<script type="text/javascript" src="../js/jquery.validate.js"></script>

		<script type="text/javascript" src="../plugins/jquery-x.x.js"></script>
		<script type="text/javascript" src="../plugins/jquery.form.js"></script>
		<script type="text/javascript" src="../plugins/jquery.blockUI.js"></script>
		<script type="text/javascript" src="../plugins/jquery.nicescroll.min.js"></script>
		<script type="text/javascript" src="../plugins/worldAddress.js"></script>
		<script type="text/javascript" src="../plugins/layer/layer.js"></script>
		<script type="text/javascript" src="../plugins/keditor/kindeditor-min.js"></script>
		<script type="text/javascript" src="../plugins/keditor/lang/zh_CN.js"></script>
		<script type="text/javascript" src="../plugins/keditor/keditor-extend.js"></script>
		<script type="text/javascript" src="../plugins/wechat-emotion/jquery-wechatEmotion-2.1.0.js"></script>
		<script type="text/javascript" src="../plugins/md5.min.js"></script>
		<script type="text/javascript" src="../plugins/recorder/bundle.js"></script>
		<script type="text/javascript" src="../plugins/recorder/recorder-extend.js"></script>

		<script type="text/javascript" src="../js/online/constant.js"></script>
		<script type="text/javascript" src="../js/online/agent-db.js"></script>
		<script type="text/javascript" src="../js/online/agent-request.js"></script>
		<script type="text/javascript" src="../js/online/agent-postman.js"></script>
		<script type="text/javascript" src="../js/online/agent-logic.js"></script>
		<script type="text/javascript" src="../js/online/all-show.js"></script>
		<script type="text/javascript" src="../js/online/agent-show.js"></script>
		<script type="text/javascript" src="../js/online/agent-crm.js"></script>
		<script type="text/javascript" src="../js/online/agent-invite.js"></script>
		<script type="text/javascript" src="../js/online/agent-qq.js"></script>
	
		<script>
		$(function(){
			var onlyShow = getQueryString("onlyShow");
			if(onlyShow) {
				agentSelfInfo = {id : "xx"};
				$("#msgWrapper").css("bottom", "0px");
				$("#msgWrapper").css("top", "0px");
				$("#chat-panel").css("left", "0px");
				$("#chat-panel").css("right", "0px");
				$("#body-left").hide();
				$("#body-right").hide();
				$("#footer").hide();
				$("#support-holder").hide();
				$("#header").hide();
				
				var cid = getQueryString("cid");
				var sid = getQueryString("sid");
				if(cid) {
					getLog(cid);
				} else if(sid) {
					getLogBySid(sid);
				}
			} else {
				agentSelfInfo = {id : "${attribute.username}"};
				getCrmField();
				getAgent();
				getSetting();
				startInterval(3000);
				initEvents();
				getGroupsAndMembers();
				getTalkingVisitor();
				getWaitingList();
				getHistoryVisitor();
				getLwNoHandled();
				getLwHandled();
				initShow();
			}
		});
		

		function tryit() {
			var src = "../visitor/VisitorDpAct.do?method=logon&pid=${attribute.pid}&channel=10001&windowOpenType=P&windowShowType=B";
			var h = 600;
			var w = 880;
			var iTop = (window.screen.availHeight  - h) / 2; 
			var iLeft = (window.screen.availWidth  - w) / 2; 
			window.open(src, "newwindow", "height=" + h + ", width=" + (w + 10) + ", toolbar=no, menubar=no, ,top="+ iTop + ",left=" + iLeft +",scrollbars=no, resizable=no, Resizable=no, location=no, status=no");
		}
		function tryit2() {
			var tcrm = {name : '${attribute.name}', phone : '${attribute.phone}'};
			var src = "../visitor/VisitorDpAct.do?method=logon&pid=${attribute.pid}&channel=10001&windowOpenType=P&windowShowType=B&tid=${attribute.username}&timestamp=${trytimestamp}&ttoken=${trytoken}&tcrm="+encodeURIComponent(encodeURIComponent(JSON.stringify(tcrm)));
			var h = 600;
			var w = 880;
			var iTop = (window.screen.availHeight  - h) / 2; 
			var iLeft = (window.screen.availWidth  - w) / 2; 
			window.open(src, "newwindow", "height=" + h + ", width=" + (w + 10) + ", toolbar=no, menubar=no, ,top="+ iTop + ",left=" + iLeft +",scrollbars=no, resizable=no, Resizable=no, location=no, status=no");
		}
	</script>	
	</head>

	<body>
		<div id="pageChat">
			<div id="body-left">
				<div>
					<div class="tab-div">
						<ul id="menu-tab-wrapper">
							<div id="menu0" class="one-tab one-function-tab" onclick='showLeftDiv(0)'>
								会话
								<i id="sessionUnreadCount" class="badge2">1</i>
							</div>
							<div id="menu1" class="one-tab one-function-tab" onclick='showLeftDiv(1)'>
								设置
								<div id="currentServiceStatus" class="click-online-icons offline-icons"></div>
							</div>
							<div id="menu2" class="one-tab one-function-tab" onclick='showLeftDiv(2)'>
								团队
							</div>
							<div id="menu3" class="one-tab one-function-tab" onclick='showLeftDiv(3)'>
								QQ
							</div>
						</ul>
					</div>

					<div id="tab-left-div-0" class="session-box tab-left-div" style="display: ">
						<div class="first-level">
							<span id="Span-myVisitor" class="click-icon" onclick="toggle('myVisitor')"></span><span onclick="toggle('myVisitor')" class="hand">我的访客</span>
							<i id="myVisitorUnreadCount" class="badge2">4</i>
							<div id="myVisitor">
								<div class="second-level">
									<span id="Span-talking" class="click-icon" onclick="toggle('talking')"></span><span onclick="toggle('talking')" class="hand">对话中</span><span id="talkingCount">0</span>
									<div id="talking">
										<ul id="talkingUL">
										</ul>
									</div>
								</div>
								<div class="second-level">
									<span id="Span-history" class="click-icon fold" onclick="toggle('history')"></span><span onclick="toggle('history')" class="hand" title="最近10天内访问的50条记录">最近访问</span><span id="historyCount">0</span>&nbsp;
									<span onclick="showAgsessions()" class="hand">[更多]</span>
									<div id="history" style="display: none">
										<ul id="historyUL">
										</ul>
									</div>
								</div>
								<div class="second-level">
									<span id="Span-waiting" class="click-icon" onclick="toggle('waiting')"></span><span onclick="toggle('waiting')" class="hand">待协助</span></span>
									<div id="waiting">
										<ul id="waitingUL">
										</ul>
									</div>
								</div>
							</div>
						</div>

						<div class="first-level">
							<span id="Span-allVisitor" class="click-icon" onclick="toggle('allVisitor')"></span><span onclick="toggle('allVisitor')" class="hand">全部访客</span>
							<div id="allVisitor">
								<div class="second-level">
									<span id="Span-queueing" class="click-icon" onclick="toggle('queueing')"></span><span onclick="toggle('queueing')" class="hand">排队中</span><span id="queueingCount"></span>
									<div id="queueing">
										<ul id="queueingUL">
										</ul>
									</div>
								</div>
							</div>
						</div>

						<div class="first-level">
							<span id="Span-allLw" class="click-icon" onclick="toggle('allLw')"></span><span onclick="toggle('allLw')" class="hand">最近留言</span>&nbsp;
							<span onclick="showLws()" class="hand">[更多]</span>
							<div id="allLw">
								<div class="second-level">
									<span id="Span-noHandled" class="click-icon" onclick="toggle('noHandled')"></span><span onclick="toggle('noHandled')" class="hand">未处理</span><span id="noHandledCount"></span>
									<div id="noHandled"">
										<ul id="noHandledUL">
										</ul>
									</div>
								</div>
								<div class="second-level">
									<span id="Span-handled" class="click-icon fold" onclick="toggle('handled')"></span><span onclick="toggle('handled')" class="hand" title="最近10天内留言的50条记录">已处理</span><span id="handledCount"></span>
									<div id="handled" style="display: none">
										<ul id="handledUL">
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>


				<div id="tab-left-div-1" class="tab-left-div" style="display: none;">
					<div class="line-full hand" onclick="httpLogoutClick()">
						退出在线客服服务
					</div>
					<div class="line-full hand" onclick="tryit()">
						点击我体验匿名访客
					</div>
					<div class="line-full hand" onclick="tryit2()">
						点击我体验实名访客
					</div>
					<div class="line-full">
						<div class="line-harf">
							设置状态：
						</div>
						<div class="line-harf">
							<myjsp:select width='100' name='serviceStatus' id='serviceStatus' listname='loginDefaultStatusList' value='' added='onchange="changeStatus()"' />
						</div>
					</div>
					<div class="line-full">
						<div class="line-harf">
							最大接待：
						</div>
						<div class="line-harf">
							<myjsp:select width='100' name='maxChat' id='maxChat' listname='maxChatList' value='' added='onchange="changeMaxChat()"' />
						</div>
					</div>
				</div>

				<div id="tab-left-div-2" class="session-box tab-left-div" style="display: none">
					<div class="first-level">
						<span id="Span-myTeam" class="click-icon" onclick="toggle('myTeam')"></span><span onclick="toggle('myTeam')" class="hand">我的团队</span>
						<div id="myTeam">
							<!-- 创建一个组，组里面可以多个成员 -->
						</div>
					</div>
				</div>

				<div id="tab-left-div-3" class="session-box tab-left-div" style="display: none">
					<div class="line-full hand" onclick="showQrcode()" style="float: none">
						QQ登陆
					</div>
					<!-- 
						<div class="first-level">
							<span id="Span-QQ" class="click-icon" onclick="toggle('QQ')"></span><span onclick="toggle('QQ')" class="hand">QQ</span>
							<div id="QQ">
							</div>
						</div>
					 -->
				</div>
			</div>
		</div>


		<div id="chat-panel">
			<div id="header">
				<div class="header-left">
					<img id="crm-avatar" src="" alt="">
					<div id="agent-info">
						<div id="crm-name">
							访客名称
						</div>
						<div id="crm-more">

						</div>
					</div>
				</div>
				<div style="float: right; margin: 20px; font-size: 12px; cursor: pointer" onclick='toggleName()'>
					昵称
				</div>
			</div>

			<div id="alerting">
				<div id="alertInside">
					this is a tip
				</div>
			</div>
			<div id="msgWrapper">
				<div id="msgHolder">
				</div>
			</div>


			<div id="audioRecording" class="talkingBottom">
				<div>
					<p>
						<span style="font-size: 12px;">正在录音...(已录音<span id="audioTime">0</span>/60秒)</span>
						<button class="a_talking_button light-btn" id="cancelRecord">
							取消
						</button>
						<button class="a_talking_button light-btn" id="sendRecord">
							发送
						</button>
						<button class="a_talking_button light-btn" id="stopRecord">
							停止
						</button>
					</p>
				</div>
			</div>
			<div id="inviting" class="talkingBottom">
				<div>
					<p>
						<span style="font-size: 12px;">正在要邀请坐席...</span>
						<button class="a_talking_button light-btn" id="cancelInvite">
							取消
						</button>
					</p>
				</div>
			</div>
			<div id="footer">
				<div class="functions">
					<span id="emojiBtn"><i class="chat-icon icon-emoji" title="表情"></i> </span>
					<form method="post" enctype="multipart/form-data" class="hideForm" style="display: block; opacity: 10;" id="sendFileForm">
						<a class="chat-icon icon-file" title="发送文件" alt="发送文件"> <input id="sendFile" name="file" type="file" class="hideFile" value="" onchange="uploadFile()"> </a>
					</form>
					<span id="cutBtn"><i class="chat-icon icon-cut" title="截图"></i> </span>
					<span id="satisfyBtn"><i class="chat-icon icon-satisfy" title="满意度评价"></i> </span>
					<span id="audioBtn" class="icon-audio" title="发送语音"></span>
					<span id="worderBtn" class="icon-worder" title="产生工单"></span>
					<span id="gchatBtn" class="icon-gchat" title="群聊"></span>
					<span id="transferBtn"><i class="chat-icon icon-transfer" title="转接"></i> </span>
					<span id="mapBtn"><i class="chat-icon icon-map" title="地图"></i> </span>
				</div>
				<div>
					<textarea id="textarea" name="content" style="width: 100%; height: 60px; visibility: hidden;"></textarea>
				</div>
				<div id="talking_buttons">
					<div id="feature-right">
						<input id="btnEnd" type="button" value="结束会话" class="a_talking_button light-btn" onclick="endSession()" style="display: none">
						<input id="btnReceive" type="button" value="接入会话" class="a_talking_button light-btn" onclick="receiveSession()" style="display: none">
						<input id="btnSend" type="button" value="发送消息" class="a_talking_button light-btn" onclick="sendChatMessage()" style="display: none">
					</div>
				</div>
			</div>
		</div>


		<div id="body-right" class="agent-body-right">
			<div>
				<div class="tab-div">
					<ul id="menu-tab-wrapper">
						<div id="menu0" class="one-tab on-extend-tab" onclick='showRightDiv(0)'>
							访客
						</div>
						<div id="menu1" class="one-tab on-extend-tab" onclick="showRightDiv(1)">
							历史
						</div>
						<div id="menu2" class="one-tab on-extend-tab" onclick="showRightDiv(2)">
							快捷
						</div>
						<div id="menu3" class="one-tab on-extend-tab" onclick="showRightDiv(3)">
							轨迹
						</div>
						<div id="menu4" class="one-tab on-extend-tab" onclick="showRightDiv(4)">
							来访
						</div>
						<div id="menu5" class="one-tab on-extend-tab" onclick="showRightDiv(5)">
							工单
						</div>

						<c:forEach var="aRecord" items="${extendList}">
							<div id="menu${aRecord.id}" class="one-tab on-extend-tab" onclick="showRightDiv('x', '${aRecord.url}')" title="${aRecord.name}">
								${aRecord.name}
							</div>
						</c:forEach>
					</ul>
				</div>
				<form action="" id="crmForm">
					<div id="tab-right-div-0" class="tab-right-div">
						<div class="form-group">
							<button name="crmSubmit" onclick="return modifyCrm();" class="a_talking_button light-btn">
								保存信息
							</button>
							<button name="crmSubmit" onclick="return showCrms()" class="a_talking_button light-btn">
								客户列表
							</button>
						</div>
					</div>
				</form>
				<div id="tab-right-div-1" class="tab-right-div" style="display: none">
				</div>
				<div id="tab-right-div-2" class="tab-right-div" style="display: none">
					<iframe src="../self/TQuestionDpAct.do?method=list2" class="full-screen" frameborder="0" id="faqFrame" name="faqFrame"></iframe>
				</div>
				<div id="tab-right-div-3" class="tab-right-div" style="display: none">
				</div>
				<div id="tab-right-div-4" class="tab-right-div" style="display: none">
				</div>
				<div id="tab-right-div-5" class="tab-right-div" style="display: none">
				</div>
				<div id="tab-right-div-x" class="tab-right-div" style="display: none">
					<iframe src="" class="full-screen" frameborder="0" id="innerFrame"></iframe>
				</div>
			</div>
		</div>


		<div id="support-holder">
			<a href="" target="_blank"></a>
		</div>
	</body>
</html>

