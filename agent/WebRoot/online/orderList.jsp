<%@ page contentType="text/html; charset=UTF-8" language="java"  errorPage="" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean"%>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.cellcom.com.cn/myjsp" prefix="myjsp"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>订单列表</title>
	<link rel="stylesheet" href="../themes/skyblue/skyblueMain.css" type="text/css" title="styles1" />    
	
	
	<!--请在下面增加js-->
   	<script type="text/javascript" src="../js/mycommon.jquery.js"></script>
	<script type="text/javascript" src='../plugins/jquery-x.x.js'> </script>
	<script type="text/javascript" src="../js/online/constant.js"></script>	
	<script>
		function sendThis(id) {
			parent.doSend($("#" + id).html(), "", MULIT_TYPE.ORDER);
		}
	</script>
</head>

<body>
	<html:form action="/visitor/VisitorDpAct?method=orderList" styleId="VisitorForm">
			<div id="main-tablist" style="left:0px">
				<table width='98%' border='0' cellpadding='0' cellspacing='0' style="margin-top:35px">

					<c:forEach var="aRecord" items="${result}">
						<div id="order_${aRecord.orderNo}" style="display: none">${aRecord.jsonString}</div>
						<tr title="订单号:${aRecord.orderNo}">
							<td align="center" valign="top" width="55px" height="60px" rowspan="2"><img width="35px" height="35px" src="${aRecord.imgUrl}"></td>
							<td>
							<strong>${aRecord.title}</strong>[￥${aRecord.price}]<br/>${aRecord.desc}
							</td>
						</tr>
						<tr>
							<td>
								${aRecord.time}&nbsp;<a href='javascript:sendThis("order_${aRecord.orderNo}");'>发送</a><br/>
							</td>
						</tr>
					</c:forEach>
				</table>
				
				<!-- <div id="info-pz"> 
					${link}
				</div> -->
			</div>
	</html:form>
</body>
</html>
